<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="topbar">
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="full">
            <button type="button" id="sidebarCollapse" class="sidebar_toggle"><i class="fa fa-bars"></i></button>
            <div class="logo_section">
                <!-- <a href="index.html"></a> -->
            </div>
            <div class="right_topbar">
                <div class="icon_info">
                    <ul class="user_profile_dd">
                        <li>
                            <a class="dropdown-toggle" data-toggle="dropdown"><?= $this->Html->image($userImage, ['class' => 'img-responsive rounded-circle', 'style' => 'height: 30px; width: 30px;']) ?><span class="name_user"><?= $userName ?></span></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?=$this->Url->build(['prefix' => 'Teacher', 'controller' => 'Users', 'action' => 'profile', base64_encode($this->request->getAttribute('identity')->id)])?>">My Profile</a>
                                <?= $this->Html->link('<span>Log Out</span> <i class="fa fa-sign-out"></i>', ['prefix' => false, 'controller' => 'Auth', 'action' => 'logout'], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                <!-- <a class="dropdown-item" href="#"><span>Log Out</span> <i class="fa fa-sign-out"></i></a> -->
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
