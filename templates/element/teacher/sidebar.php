<?php

/**
 * @var \App\View\AppView $this
 */
?>
<nav id="sidebar">
    <div class="sidebar_blog_1">
        <div class="sidebar-header">
            <div class="logo_section">
                <a href="javascript:void(0)">
                    <?= $this->Html->image($userImage, ['class' => 'logo_icon img-responsive', 'style' => 'height: 45px; width: 45px;']) ?>
                </a>
            </div>
        </div>
        <div class="sidebar_user_info">
            <div class="icon_setting"></div>
            <div class="user_profle_side">
                <!-- <div class="user_img"><img class="img-responsive" src="images/layout_img/user_img.jpg" alt="#" /></div> -->
                <div class="user_img"><?= $this->Html->image($userImage, ['class' => 'img-responsive', 'style' => 'height: 70px; width: 70px;']) ?></div>
                <div class="user_info">
                    <h6><?= $userName ?></h6>
                    <p><span class="online_animation"></span> Online</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar_blog_2">
        <h4>General</h4>
        <ul class="list-unstyled components">
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Dashboard', 'action' => 'index']) ?>">
                    <i class="fa fa-dashboard yellow_color"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#evaluation" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-adjust blue1_color"></i>
                    <span>Evaluation</span>
                </a>
                <ul class="collapse list-unstyled" id="evaluation">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Evaluations', 'action' => 'index']) ?>">
                            ><span>Evaluations List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'EvaluationMonths', 'action' => 'index']) ?>">
                            ><span>Evaluations Months List</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#domains" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-adjust red_color"></i>
                    <span>Domains</span>
                </a>
                <ul class="collapse list-unstyled" id="domains">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Domains', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Domains', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#domainTypes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-file green_color"></i>
                    <span>Domain Types</span>
                </a>
                <ul class="collapse list-unstyled" id="domainTypes">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'DomainTypes', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'DomainTypes', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#parents" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-user purple_color"></i>
                    <span>Parent/Student</span>
                </a>
                <ul class="collapse list-unstyled" id="parents">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Parents', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Parents', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Parents', 'action' => 'viewGrades']) ?>">
                            ><span>View Student Grades</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Assessments', 'action' => 'index']) ?>">
                    <i class="fa fa-list-ol green_color"></i>
                    <span>Students Assessment</span>
                </a>
            </li>
            <li>
                <a href="#subjects" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-list-alt orange_color"></i>
                    <span>Subjects</span>
                </a>
                <ul class="collapse list-unstyled" id="subjects">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Subjects', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Subjects', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#lesson_plans" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-list-alt orange_color"></i>
                    <span>Lesson Plan</span>
                </a>
                <ul class="collapse list-unstyled" id="lesson_plans">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'LessonPlans', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'LessonPlans', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#modules" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-file orange_color2"></i>
                    <span>Modules</span>
                </a>
                <ul class="collapse list-unstyled" id="modules">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Modules', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Modules', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#videos" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-play red_color"></i>
                    <span>Videos</span>
                </a>
                <ul class="collapse list-unstyled" id="videos">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Videos', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Videos', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#audios" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-volume-up purple_color"></i>
                    <span>Audios</span>
                </a>
                <ul class="collapse list-unstyled" id="audios">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Audios', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Audios', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#quizzes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-pencil-square blue1_color"></i>
                    <span>Quizzes</span>
                </a>
                <ul class="collapse list-unstyled" id="quizzes">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Quizzes', 'action' => 'index']) ?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Teacher', 'controller' => 'Quizzes', 'action' => 'add']) ?>">
                            ><span>Register</span>
                        </a>
                    </li> -->
                </ul>
            </li>
            <li>
                <a href="#websites" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-globe blue2_color"></i>
                    <span>Website</span>
                </a>
                <ul class="collapse list-unstyled" id="websites">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Admin', 'controller' => 'Carousels', 'action' => 'index']) ?>">
                            <i class="fa fa-image"></i><span>Carousel</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Admin', 'controller' => 'Abouts', 'action' => 'index']) ?>">
                            <i class="fa fa-info"></i><span>About</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'Admin', 'controller' => 'Contacts', 'action' => 'index']) ?>">
                            <i class="fa fa-map-signs"></i><span>Contacts</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</nav>