<?php
/**
 * @var \App\View\AppView $this
 */
?>
<footer class="tm-footer row tm-mt-small">
    <div class="col-12 font-weight-light">
        <p class="text-center text-white mb-0 px-4 small">
            Copyright &copy; <b><?=date('Y')?></b> All rights reserved.
        </p>
    </div>
</footer>
