<?php
/**
 * @var \App\View\AppView $this
 */
?>

<nav class="navbar navbar-expand-xl">
    <div class="container h-100">
        <a class="navbar-brand" href="javascript:void(0);">
            <h1 class="tm-site-title mb-0">Kindy LMS</h1>
        </a>
        <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars tm-nav-icon"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto h-100">
                <li class="nav-item <?=(strtolower($controller) == strtolower('Quizzes'))? 'active': null;?>">
                    <a class="nav-link" href="<?=$this->Url->build(['prefix' =>'Student', 'controller' => 'Quizzes', 'action' => 'index'])?>">
                        <i class="fa fa-pencil-alt"></i>
                        Quizzes
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('Assessments'))? 'active': null;?>">
                    <a class="nav-link" href="<?=$this->Url->build(['prefix' =>'Student', 'controller' => 'Assessments', 'action' => 'index'])?>">
                        <i class="fa fa-file"></i>
                        Results
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('Modules'))? 'active': null;?>">
                    <a class="nav-link" href="<?=$this->Url->build(['prefix' =>'Student', 'controller' => 'Modules', 'action' => 'index'])?>">
                        <i class="fa fa-book"></i>
                        Modules
                    </a>
                </li>

                <li class="nav-item <?=(strtolower($controller) == strtolower('Videos'))? 'active': null;?>">
                    <a class="nav-link" href="<?=$this->Url->build(['prefix' =>'Student', 'controller' => 'Videos', 'action' => 'index'])?>">
                        <i class="fa fa-video"></i>
                        Videos
                    </a>
                </li>

                <li class="nav-item <?=(strtolower($controller) == strtolower('Audios'))? 'active': null;?>">
                    <a class="nav-link" href="<?=$this->Url->build(['prefix' =>'Student', 'controller' => 'Audios', 'action' => 'index'])?>">
                        <i class="fa fa-file-audio"></i>
                        Audios
                    </a>
                </li>

                <li class="nav-item <?=(strtolower($controller) == strtolower('Games'))? 'active': null;?>">
                    <a class="nav-link" href="<?=$this->Url->build(['prefix' =>'Student', 'controller' => 'Games', 'action' => 'index'])?>">
                        <i class="fa fa-gamepad"></i>
                        Games
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li>
                    <a class="nav-link d-block" href="<?=$this->Url->build(['prefix' =>false, 'controller' => 'Auth', 'action' => 'logout'])?>">
                        <?=$this->request->getAttribute('identity')->student->first_name.' '.strtoupper($this->request->getAttribute('identity')->student->middle_name[0]).' '.$this->request->getAttribute('identity')->student->last_name?>, <b>Logout</b>
                    </a>
                </li>
            </ul>
        </div>
    </div>

</nav>
