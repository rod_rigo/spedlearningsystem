<?php

/**
 * @var \App\View\AppView $this
 */
?>
<nav id="sidebar">
    <div class="sidebar_blog_1">
        <div class="sidebar-header">
            <div class="logo_section">
                <a href="javascript:void(0)">
                    <?= $this->Html->image($userImage, ['class' => 'logo_icon img-responsive', 'style' => 'height: 45px; width: 45px;']) ?>
                </a>
            </div>
        </div>
        <div class="sidebar_user_info">
            <div class="icon_setting"></div>
            <div class="user_profle_side">
                <!-- <div class="user_img"><img class="img-responsive" src="images/layout_img/user_img.jpg" alt="#" /></div> -->
                <div class="user_img"><?= $this->Html->image($userImage, ['class' => 'img-responsive', 'style' => 'height: 70px; width: 70px;']) ?></div>
                <div class="user_info">
                    <h6><?= $this->request->getAttribute('identity')->parent->first_name.' '.$this->request->getAttribute('identity')->parent->last_name ?></h6>
                    <p><span class="online_animation"></span> Online</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar_blog_2">
        <h4>General</h4>
        <ul class="list-unstyled components">
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'Parent', 'controller' => 'Assessments', 'action' => 'index']) ?>">
                    <i class="fa fa-list-ol"></i>
                    <span>Assessments</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'Parent', 'controller' => 'Subjects', 'action' => 'index']) ?>">
                    <i class="fa fa-list-alt"></i>
                    <span>Subjects</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'Parent', 'controller' => 'Quizzes', 'action' => 'index']) ?>">
                    <i class="fa fa-pencil-square"></i>
                    <span>Quizzes</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'Parent', 'controller' => 'Parents', 'action' => 'view-grades']) ?>">
                    <i class="fa fa-file"></i>
                    <span>Grades</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
