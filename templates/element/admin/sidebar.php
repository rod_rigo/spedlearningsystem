<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav id="sidebar">
    <div class="sidebar_blog_1">
        <div class="sidebar-header">
            <div class="logo_section">
                <a href="javascript:void(0)">
                    <img class="logo_icon img-responsive" src="" alt="#" />
                </a>
            </div>
        </div>
        <div class="sidebar_user_info">
            <div class="icon_setting"></div>
            <div class="user_profle_side">
                <div class="user_img"><img class="img-responsive" src="images/layout_img/user_img.jpg" alt="#" /></div>
                <div class="user_info">
                    <h6>John David</h6>
                    <p><span class="online_animation"></span> Online</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar_blog_2">
        <h4>General</h4>
        <ul class="list-unstyled components">
            <li>
                <a href="#users" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                </a>
                <ul class="collapse list-unstyled" id="users">
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'add'])?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#teachers" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Teachers</span>
                </a>
                <ul class="collapse list-unstyled" id="teachers">
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'index'])?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'add'])?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#parents" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Parents</span>
                </a>
                <ul class="collapse list-unstyled" id="parents">
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Parents', 'action' => 'index'])?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Parents', 'action' => 'add'])?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#students" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Students</span>
                </a>
                <ul class="collapse list-unstyled" id="students">
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Students', 'action' => 'index'])?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Students', 'action' => 'add'])?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#childrens" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Guardians</span>
                </a>
                <ul class="collapse list-unstyled" id="childrens">
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Childrens', 'action' => 'index'])?>">
                            ><span>List</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Childrens', 'action' => 'add'])?>">
                            ><span>Register</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#websites" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-globe"></i>
                    <span>Website</span>
                </a>
                <ul class="collapse list-unstyled" id="websites">
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Carousels', 'action' => 'index'])?>">
                            <i class="fa fa-image"></i><span>Carousel</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Abouts', 'action' => 'index'])?>">
                            <i class="fa fa-info"></i><span>About</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Contacts', 'action' => 'index'])?>">
                            <i class="fa fa-map-signs"></i><span>Contacts</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</nav>
