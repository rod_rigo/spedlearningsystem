<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quiz[]|\Cake\Collection\CollectionInterface $quizzes
 */
?>

<div class="row h-100 d-flex justify-content-center align-items-center"  data-anijs="if: scroll ,on: window, do: tada animated, to: .ani">


    <div class="col-sm-12 col-md-4 col-lg-3 tm-block-col">
        <div class="tm-bg-white tm-block card" style="background: lightgrey;">
            <div class="card-body">
                <?=$this->Html->image('math.png',[
                    'class' => 'w-100 h-100'
                ])?>
            </div>
            <div class="card-footer">
                <a href="<?=$this->Url->build([
                    'prefix' => 'Student',
                    'controller' => 'Games',
                    'action' => 'math'
                ])?>" class="btn btn-small btn-block btn-blue quiz" >
                    Play <i class="fa fa-gamepad"></i>
                </a>
            </div>
        </div>
    </div>

</div>

<script>
    'use strict';
    $(document).ready(function () {

        var cards = $('.card');
        var animations = [
            'animate__bounce',
            'animate__pulse',
            'animate__rubberBand',
            'animate__headShake',
            'animate__swing',
            'animate__tada',
            'animate__wobble',
            'animate__jello',
            'animate__heartBeat',
        ];

        $('p, label, h1, h2, h3, h4, h5, h5, li, small, button').hover( function () {
            window.speechSynthesis.cancel();
            var text = $(this).text();
            speech.text = text;
            // Start Speaking
            window.speechSynthesis.speak(speech);
        });

        cards.addClass('animate__animated animate__swing animate__slow');

        setTimeout(function () {
            cards.removeClass('animate__animated animate__swing animate__slow');
        }, 3000);

        setTimeout(function () {
            cards.each(function (e) {
                var random = Math.floor(Math.random() * (parseInt(animations.length) + 1));
                $(this).toggleClass('animate__animated '+(animations[(random)])+' animate__delay-5s animate__slow animate__infinite');
            });
        }, 5000);


    });
</script>