<?php
/**
 * @var \App\View\AppView
 */
?>

<style>
    #container{
        Height: 100%;
        width: 100%;
        background-color: #9DD2EA;
        margin: 100px auto;
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0px 4px 0px 0px #009de4;
        -webkit-box-shadow: 0px 4px 0px 0px #009de4;
        -moz-box-shadow: 0px 4px 0px 0px #009de4;
        /*[horizontal offset, vertical offset, blur radius, spead]*/
        position: relative;
    }

    #score {
        background-color: #f1ff92;
        color: #888e5f;
        padding: 11px;
        position: absolute;
        left: 500px;
        box-shadow: 0px 4px #9da853;
        -webkit-box-shadow: 0px 4px #9da853;
        -moz-box-shadow: 0px 4px #9da853;
        height: 2.5em;
        margin-bottom: 1em;
    }

    #correct {
        position: absolute;
        left: 45%;
        top: 30%;
        background-color: #42e252;
        color: white;
        padding: 11px;
        display: none;
    }
    #wrong {
        position: absolute;
        left: 45%;
        top: 30%;
        background-color: #de401a;
        color: white;
        padding: 11px;
        display: none;
    }

    #question{
        width: 80%;
        height: 3em !important;
        margin: 50px auto 10px auto;
        background-color: #9da0ea;
        box-shadow: 0px 4px #535aa8;
        -webkit-box-shadow: 0px 4px #535aa8;
        -moz-box-shadow: 0px 4px #535aa8;
        font-size: 100px;
        text-align: center;
        font-family: cursive, sans-serif;
        color:black;
    }
    #instruction {
        width: 80%;
        height: 50px;
        background-color: #b481d9;
        margin: 10px auto;
        text-align: center;
        line-height: 45px;
        box-shadow: 0px 4px #815386;
        -moz-box-shadow: 0px 4px #815386;
        -webkit-box-shadow: 0px 4px #815386;
    }
    #choices {
        width: 450px;
        height: 100px;
        margin: 5px auto;
    }
    .box {
        margin-right: 36px;
        width: 85px;
        height: 85px;
        background-color: white;
        float: left;
        border-radius: 3px;
        cursor: pointer;
        box-shadow: 0px 4px rgba(0,0,0, 0.2);
        -moz-box-shadow: 0px 4px rgba(0,0,0, 0.2);
        -webkit-box-shadow: 0px 4px rgba(0,0,0, 0.2);
        text-align: center;
        line-height: 80px;
        position: relative;
        transition: all 0.2s;
        -webkit-transition: all 0.2s;
        -moz-transition: all 0.2s;
    }
    .box:hover, #startreset:hover{
        background-color: #9C89f6;
        color: white;
        box-shadow: 0px 4px #6b54d3;
        -webkit-box-shadow: 0px 4px #6b54d3;
        -moz-box-shadow: 0px 4px #6b54d3;
    }
    .box:active, #startreset:active{
        box-shadow: 0px 0px #6b54d3;
        -moz-box-shadow: 0px 0px #6b54d3;
        -webkit-box-shadow: 0px 0px #6b54d3;
        top: 4px;
    }

    #box4{
        margin-right: 0;
    }
    #startreset {
        width: 8em;
        padding: 10px;
        background-color: rgba(255,255,255,0.5);
        margin: 0 auto;
        border-radius: 3px;
        cursor: pointer;
        box-shadow: 0px 4px rgba(0,0,0, 0.2);
        -moz-box-shadow: 0px 4px rgba(0,0,0, 0.2);
        -webkit-box-shadow: 0px 4px rgba(0,0,0, 0.2);
        text-align: center;
        position: relative;
        transition: all 0.2s;
        -webkit-transition: all 0.2s;
        -moz-transition: all 0.2s;
    }

    #timeremaining {
        color: white;
        width: 160px;
        padding: 10px;
        position: absolute;
        right: 0;
        bottom: 0;
        background-color: rgba( 94, 223, 87, 0.693 );
        border-radius: 5px;
        box-shadow: 0px 4px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: 0px 4px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0px 4px rgba(0, 0, 0, 0.2);
        /*	visibility: hidden;*/
        display: none;
        height: 5em;
    }
    #timeremainingvalue {
        /*	font-family: fantasy;*/
        /*	font-weight: 500;*/
        /*	font-size: 17px;*/
    }
    #gameOver {
        height: 200px;
        width: 500px;
        background: linear-gradient(#e8f64a, #F3CA6B, #F3706C);
        -moz-background: linear-gradient(#e8f64a, #F3CA6B, #F3706C);
        -webkit-background: linear-gradient(#e8f64a, #F3CA6B, #F3706C);
        color: white;
        font-size: 2.3em;
        text-align: center;
        text-transform: uppercase;
        position: absolute;
        top: 100px;
        left: 30%;
        z-index: 2;
        display: none;
    }
</style>

<div class="row h-100 d-flex justify-content-center align-items-center"  data-anijs="if: scroll ,on: window, do: tada animated, to: .ani">
    <div id="container">
        <div id="score">
            score: <span id="scorevalue" style="font-weight: 900">0</span>
        </div>
        <div id="correct">
            Correct!
        </div>
        <div id="wrong">
            Try again!
        </div>
        <div id="question">

        </div>
        <div id="instruction">
            Click on the correct answer.
        </div>
        <div id="choices">

            <div id="box1" class="box"> </div>
            <div id="box2" class="box">	</div>
            <div id="box3" class="box">	</div>
            <div id="box4" class="box">	</div>
        </div>
        <div id="startreset">
            Start Game
        </div>
        <div id="timeremaining">
            Time remaining:   <span id="timeremainingvalue" style="font-family: sans seriff; font-weight: bold; font-size 32px; color: black;">  60</span>
        </div>
        <div id="gameOver">

        </div>
    </div>
</div>



<script>

    var playing = false;
    var score;
    var action;
    var timeremaining;
    var correctAnswer;

    document.getElementById("startreset").onclick = function(){
        if (playing == true){
            location.reload();
        }
        else {
            playing = true;
            score = 0;

            document.getElementById("scorevalue").innerHTML = score;
            //show count
            show("timeremaining");
            timeremaining = 60;

            document.getElementById("timeremainingvalue").innerHTML = timeremaining;

            //hide game over
            hide("gameOver");

            //change start to reset
            document.getElementById("startreset").innerHTML = "Reset Game";

            //start count
            startCountdown();

            //generate quetion
            generateQA();

        }
    }

    for(i=1; i<5; i++){
        document.getElementById("box"+i).onclick = function(){
            if (playing == true){
                if(this .innerHTML == correctAnswer){

                    //increase score
                    score++;
                    document.getElementById("scorevalue").innerHTML = score;
                    hide("wrong");
                    show("correct");
                    setTimeout(function(){
                        hide("correct");
                    }, 1000);
                    generateQA();

                }else{
                    //wrong answer
                    hide("correct");
                    show("wrong");
                    setTimeout(function(){
                        hide("wrong");
                    }, 1000);
                    generateQA();
                }
            }
        }
    }

    //functions
    //start count
    function startCountdown(){
        action = setInterval(function(){
            timeremaining -= 1;


            document.getElementById("timeremainingvalue").innerHTML = timeremaining;
            if(timeremaining == 0){
                stopCountdown();
                show("gameOver");

//game over
                document.getElementById("gameOver").innerHTML = "<p>Game over!</p><p>Your score is " + score + ".</p>";
                speech.text =  "<p>Game over!</p><p>Your score is " + score + ".</p>";
                window.speechSynthesis.speak(speech);
                hide("timeremaining");
                hide("correct");
                hide("wrong");
                playing = false;

                document.getElementById("startreset").innerHTML = "Start Game";
                add(score, 'Math');
            }
        }, 1000);
    }

    //stop count
    function stopCountdown() {

        clearInterval(action);
    }

    //hide
    function hide(Id){
        document.getElementById(Id).style.display = "none";
    }

    //show
    function show(Id) {
        document.getElementById(Id).style.display = "block";
    }
    //guestion
    function generateQA(){
        window.speechSynthesis.cancel();
        var operations = ['+', '-', '*'];
        var operand = operations[parseInt(Math.floor(Math.random() * 3))]
        var x = 1+ Math.round(9*Math.random());
        var y = 1+ Math.round(9*Math.random());
        var question = String(x+(operand)+y);
        correctAnswer = eval(question);

        speech.text = 'what is '+(question.replaceAll('*', 'Multiply By').replaceAll('-', 'Minus').replaceAll('+', 'Plus'))+'';
        window.speechSynthesis.speak(speech);

        document.getElementById("question").innerHTML = question.replace('*', 'x');
        var correctPosition = 1+ Math.round(3*Math.random());

        document.getElementById("box"+correctPosition).innerHTML = correctAnswer;//correct answer

        //wrong answers
        var answers = [correctAnswer];

        for(i=1; i<5; i++){
            if (i != correctPosition) {
                var wrongAnswer;
                do{
                    wrongAnswer = eval(String((1+ Math.round(9*Math.random()))+operand+(1+ Math.round(9*Math.random()))));//wrong answer

                }while(answers.indexOf(wrongAnswer)>-1)

                document.getElementById("box"+i).innerHTML = wrongAnswer;
                answers.push(wrongAnswer);
            }
        }

        speech.text = 'please click the answer below';
        window.speechSynthesis.speak(speech);
        // Start Speaking

    }

    function add(score, game) {
        $.ajax({
            url:mainurl+'games/add',
            type: 'POST',
            method: 'POST',
            headers: {
                'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
            },
            data:{
                score: score,
                game:game,
            },
            dataType:'JSON'
        }).done(function (data, status, xhr) {
            swal('success', data.result, data.message);
        }).fail(function (xhr, status, error) {
            const response = JSON.parse(xhr.responseText);
            swal('info', response.result, response.message);
        });

    }

    function swal(icon, result, message) {
        Swal.fire({
            icon:icon,
            title:result,
            text:message,
            timer:5000,
            toast:true,
            timerProgressBar:true,
            position:'top-right',
        });
    }

</script>
