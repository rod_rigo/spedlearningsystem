<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Audio[]|\Cake\Collection\CollectionInterface $audios
 */
?>

<style>
    .card:hover{
        cursor: pointer;
    }
</style>

<div class="row h-100"  data-anijs="if: scroll ,on: window, do: tada animated, to: .ani">

    <div class="col-sm-12 col-md-8 col-lg-8 tm-block-col">
        <div class="row" >

            <?php foreach ($audios as $key => $audio):?>
                <div class="col-sm-12 col-md-6 col-lg-4 m-1 ani-js-<?=($key)?> ani" data-anijs="if: mouseenter, on: .ani-js-<?=($key)?>,  do: rubberBand animated, to: .ani-js-<?=($key)?>">
                    <div class="card">
                        <img src="<?=$this->Url->image('music.png')?>" class="card-img-top img-thumbnail" style="height: 10em !important;" alt="Video Thumbnail">
                        <div class="card-body">
                            <p class="card-text" style="font-size: 0.9em;"><?=$audio->title?></p>
                            <a href="<?=$audio->audio?>" data-id="<?=$audio->id?>" class="btn btn-small btn-block btn-blue audio-sources" >
                               Play <i class="fa fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>

        </div>
    </div>

    <div class="col-sm-12 col-md-4 col-lg-4 tm-block-col">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 m-4">
                <audio id="audio-player" class="video-js vjs-default-skin" poster="<?=$this->Url->image('music.png')?>" controls preload="auto" style="width: 100%;" height="400">
                    <!-- Add more audio sources for compatibility with different browsers -->
                </audio>
            </div>
        </div>

    </div>

</div>

<script>
    'use strict';
    $(window).on('load',function () {


        var player = videojs('audio-player', {
            controlBar: {
                playToggle: true,
                currentTimeDisplay: true,
                durationDisplay: true,
                volumePanel: {
                    inline: true,
                },
            },
            audioOnlyMode:false,
            responsive:true,
            audioPosterMode:false,
            enableDocumentPictureInPicture:true
        });

        $('a.audio-sources').click(function (e) {
           e.preventDefault();
           var href = $(this).attr('href');
           player.src(href);
           player.autoplay(true);
           player.muted(false);
           player.poster('<?=$this->Url->image('music.png')?>');
           player.play();
        });

    });
</script>


