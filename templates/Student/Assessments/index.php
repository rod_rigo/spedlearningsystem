<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assessment[]|\Cake\Collection\CollectionInterface $assessments
 */
?>

<div class="row h-100">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
        <div class="tm-bg-white tm-block">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Teacher</th>
                        <th>Subject</th>
                        <th>Title</th>
                        <th>Score</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {
        var baseurl = mainurl+'assessments/';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getData',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 1,
                    data: null,render: function(data,type,row){
                    return (row.quiz.teacher.first_name) +' '+ (row.quiz.teacher.last_name);
                }
                },
                {
                    targets: 5,
                    data: null,render: function(data,type,row){
                        return moment(row.quiz.created).format('Y-M-D hh:m A');
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'quiz.teacher.first_name'},
                { data: 'subject.subject'},
                { data: 'quiz.quiz'},
                { data: 'points'},
                { data: 'quiz.created'},
            ]
        });
    });
</script>