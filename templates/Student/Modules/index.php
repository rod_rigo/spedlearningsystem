<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Module[]|\Cake\Collection\CollectionInterface $modules
 */
?>
<style>
    li.modules:hover{
        padding: 0.5em 0 !important;
        background-color: #00A5E3;
        transition-delay: 0.1s;
    }
    li.modules:hover i.fa{
        margin-right: 1.5em;
        transition: all 0.5s;
        margin-left: 0.5em;
        transition-delay: 0.1s;
    }
</style>

<?php
    $i = 0;
    $f = 0;
    function generateRandomLightColor() {
        $minBrightness = 150; // Adjust this value to control the minimum brightness

        $red = mt_rand($minBrightness, 255);
        $green = mt_rand($minBrightness, 255);
        $blue = mt_rand($minBrightness, 255);

        $color = sprintf("#%02X%02X%02X", $red, $green, $blue);

        return $color;
    }

function generateRandomSolidColor() {
    $minBrightness = 0; // Adjust this value to control the minimum brightness

    $red = mt_rand($minBrightness, 255);
    $green = mt_rand($minBrightness, 255);
    $blue = mt_rand($minBrightness, 255);

    $color = sprintf("#%02X%02X%02X", $red, $green, $blue);

    return $color;
}
?>
<div class="row h-100" style=" background-image: url(<?=$this->Url->assetUrl('/product-admin/img/new-bg.jpg')?>);background-repeat: no-repeat ;background-size: 100% 100%;object-fit: cover">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
        <div class="p-2">
            <ul class="nav nav-tabs tabs" role="tablist">
                <?php foreach ($subjects as $subject):?>
                    <li class="nav-item">
                        <?php
                            $color = generateRandomLightColor();
                        ?>

                        <a class="nav-link <?=($i == 0)? 'active': null;?>" data-toggle="tab" href="#tab-<?=$subject->id?>" style="background-color: <?=($color)?> !important; border-color: <?=($color)?> !important;" role="tab">
                            <?=ucwords($subject->subject)?>
                        </a>
                    </li>
                    <?php $i++;?>
                <?php endforeach;?>
            </ul>
            <div class="tab-content tabs card-block">

                <?php foreach ($subjects as $subject):?>
                    <div class="tab-pane <?=($f == 0)? 'active': null;?>" id="tab-<?=$subject->id?>" role="tabpanel">
                        <div class="col-sm-12 col-md-8 col-lg-8 m-3">
                            <ul style="list-style-type: none !important;;">
                                <?php foreach ($subject->modules as $key => $module):?>
                                    <?php
                                        $color = generateRandomSolidColor();
                                        $filetype = ($module->type)? $module->module: explode('.',$module->module);
                                        $title = ($module->type) ? ucwords('Click to visit link ').($module->module).'.': ucwords('This is ').(strtoupper(@$filetype[1])).' File.';
                                    ?>
                                    <li class="h5 mt-2 modules p-1" style="cursor: pointer;">
                                        <i class="fa fa-chevron-right"></i>
                                        <a href="<?=($module->type)? $module->module: $this->Url->build(['prefix' => 'Student', 'controller' => 'Modules', 'action' => 'download', $module->id]);?>" class="text-dark" title="<?=$title?>">
                                            <?=ucwords($module->title)?>
                                            <?php if(boolval($module->type)):?>
                                                <a style="background: <?=($color)?>; color: #fff; font-size: 0.8em; padding: 0.3em;border-radius: 4em;" href="<?=$this->Url->build(['prefix' => 'Student', 'controller' => 'Modules', 'action' => 'download', $module->id]);?>" target="_blank">Visit Link</a>
                                            <?php else:?>
                                                <?php if(strtolower(@$filetype[1]) == strtolower('pdf')):?>
                                                    <a style="background: <?=($color)?>; color: #fff; font-size: 0.8em; padding: 0.3em;border-radius: 4em;" href="<?=$this->Url->build(['prefix' => 'Student', 'controller' => 'Modules', 'action' => 'download', $module->id]);?>" target="_blank">Download</a>
                                                    <a style="background: <?=($color)?>; color: #fff; font-size: 0.8em; padding: 0.3em;border-radius: 4em;" href="<?=$this->Url->assetUrl('/modules/'.($module->module));?>" target="_blank">Preview</a>
                                                <?php else:?>
                                                    <a style="background: <?=($color)?>; color: #fff; font-size: 0.8em; padding: 0.3em; border-radius: 4em;" href="<?=$this->Url->build(['prefix' => 'Student', 'controller' => 'Modules', 'action' => 'download', $module->id]);?>" target="_blank">Download</a>
                                                <?php endif;?>
                                            <?php endif;?>
                                        </a>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <?php $f++;?>
                <?php endforeach;?>

            </div>
        </div>
    </div>
</div>
