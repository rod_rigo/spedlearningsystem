<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quiz[]|\Cake\Collection\CollectionInterface $quizzes
 */
?>

<style>
    .card{
        cursor: pointer;
        min-height: 40vh !important;
        /*max-height: 30vh !important;*/
        background-image: url("<?=$this->Url->image('question.png')?>");
        background-repeat: no-repeat !important;
        background-size: 100% 100% !important;
        object-fit: contain !important;
        /*background-color: #ccd6dd !important;*/
    }
    #quiz-container{

        max-height: 600vh !important;
/*        background-image: url("*/<?//=$this->Url->image('paper.png')?>/*");*/
        background-repeat: no-repeat !important;
        background-size: 100% 100% !important;
        object-fit: cover !important;
        /*background-color: #ccd6dd !important;*/
    }
</style>

<div class="row h-100"  data-anijs="if: scroll ,on: window, do: tada animated, to: .ani">

    <div class="col-sm-12 col-md-12 col-lg-12 tm-block-col">

        <div class="row" id="quiz-container">

            <?php foreach ($quizzes as $quiz):?>
                <?php if(boolval($quiz->is_special)):?>
                    <div class="col-sm-12 col-md-3 col-lg-3 m-1">
                        <div class="card">
                            <div class="card-header">
                                <p class="card-text text-dark font-weight-bold"><?=$quiz->quiz?></p>
                            </div>
                            <div class="card-body">
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=$quiz->quiz_type?>
                                </p>
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=$quiz->subject->subject?>
                                </p>
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=(new DateTime($quiz->start_time))->format('h:m A')?>
                                </p>
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=(new DateTime($quiz->end_time))->format('h:m A')?>
                                </p>

                                <small class="font-weight-bold" id="small-<?=intval($quiz->id)?>">Time</small>
                            </div>
                            <div class="card-footer">
                                <?php
                                $now = (new DateTime());
                                $start_time = (new DateTime($quiz->date.' '.$quiz->start_time));
                                $end_time = (new DateTime($quiz->date.' '.$quiz->end_time));
                                ?>

                                <button type="button" data-id="<?= $quiz->id?>" id="button-<?=intval($quiz->id)?>" class="btn btn-small btn-block btn-blue quiz" >
                                    Take Special Quiz <i class="fa fa-pencil-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <script>
                        'use strict';
                        $(document).ready(function () {

                            $('.quiz').click(function () {
                                var dataId = $(this).attr('data-id');
                                window.location.replace(mainurl+'quizzes/view/'+dataId);
                            });

                        });
                    </script>
                <?php else:?>
                    <div class="col-sm-12 col-md-3 col-lg-3 m-1">
                        <div class="card">
                            <div class="card-header">
                                <p class="card-text text-dark font-weight-bold"><?=$quiz->quiz?></p>
                            </div>
                            <div class="card-body">
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=$quiz->quiz_type?>
                                </p>
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=$quiz->subject->subject?>
                                </p>
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=(new DateTime($quiz->start_time))->format('h:m A')?>
                                </p>
                                <p class="text-dark font-weight-bold">
                                    <i class="fa fa-chevron-right"></i> <?=(new DateTime($quiz->end_time))->format('h:m A')?>
                                </p>

                                <small class="font-weight-bold" id="small-<?=intval($quiz->id)?>">Time</small>
                            </div>
                            <div class="card-footer">
                                <?php
                                $now = (new DateTime());
                                $start_time = (new DateTime($quiz->date.' '.$quiz->start_time));
                                $end_time = (new DateTime($quiz->date.' '.$quiz->end_time));
                                ?>

                                <?php if($now > $start_time && $now < $end_time):?>
                                    <button type="button" data-id="<?= $quiz->id?>" id="button-<?=intval($quiz->id)?>" class="btn btn-small btn-block btn-blue quiz" >
                                        Take Quiz <i class="fa fa-pencil-alt"></i>
                                    </button>
                                <?php else:?>
                                    <button type="button" data-id="<?= $quiz->id?>" id="button-<?=intval($quiz->id)?>" disabled class="btn btn-small btn-block btn-blue quiz" >
                                        Take Quiz <i class="fa fa-pencil-alt"></i>
                                    </button>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <script>
                        'use strict';
                        $(document).ready(function () {
                            var timestart = moment('<?=$quiz->date.' '.$quiz->start_time?>');
                            var timeend = moment('<?=$quiz->date.' '.$quiz->end_time?>');
                            var duration;

                            $('.quiz').click(function () {
                                var dataId = $(this).attr('data-id');
                                window.location.replace(mainurl+'quizzes/view/'+dataId);
                            });

                            setInterval(function () {
                                var currenttime = moment();

                                if(currenttime >= timestart && currenttime <= timeend){
                                    duration = moment.duration(moment().diff(timeend));
                                    $('#button-<?=intval($quiz->id)?>').prop('disabled', false);
                                    $('#small-<?=intval($quiz->id)?>').text( (Math.abs(duration._data.days))+' Days '+(Math.abs(duration._data.hours))+'Hours '+(Math.abs(duration._data.minutes))+' Minutes '+(Math.abs(duration._data.seconds)+' Seconds Left.'));
                                    return true;
                                }

                                if(currenttime > timestart && timeend < currenttime){
                                    duration = moment.duration(moment().diff(timeend));
                                    $('#button-<?=intval($quiz->id)?>').prop('disabled', true);
                                    $('#small-<?=intval($quiz->id)?>').text( 'Ended '+(Math.abs(duration._data.days))+' Days '+(Math.abs(duration._data.hours))+'Hours '+(Math.abs(duration._data.minutes))+' Minutes '+(Math.abs(duration._data.seconds)+' Seconds')+' Ago.');
                                    return true;
                                }

                                if(currenttime < timestart && timeend > currenttime ){
                                    duration = moment.duration(moment().diff(timestart));
                                    $('#button-<?=intval($quiz->id)?>').prop('disabled', true);
                                    $('#small-<?=intval($quiz->id)?>').text( (Math.abs(duration._data.days))+' Days '+(Math.abs(duration._data.hours))+'Hours '+(Math.abs(duration._data.minutes))+' Minutes '+(Math.abs(duration._data.seconds)+' Seconds To Wait.'));
                                    return true;
                                }

                            },1000);

                        });
                    </script>
                <?php endif;?>
            <?php endforeach;?>

        </div>

    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {

        var cards = $('.card');
        var animations = [
            'animate__bounce',
            'animate__pulse',
            'animate__rubberBand',
            'animate__headShake',
            'animate__swing',
            'animate__tada',
            'animate__wobble',
            'animate__jello',
            'animate__heartBeat',
        ];

        $('p, label, h1, h2, h3, h4, h5, h5, li, small, button').hover( function () {
           window.speechSynthesis.cancel();
           var text = $(this).text();
           speech.text = text;
           // Start Speaking
           window.speechSynthesis.speak(speech);
       });

        cards.addClass('animate__animated animate__swing animate__slow');

        setTimeout(function () {
            cards.removeClass('animate__animated animate__swing animate__slow');
        }, 3000);

        setTimeout(function () {
            cards.each(function (e) {
                var random = Math.floor(Math.random() * (parseInt(animations.length) + 1));
                $(this).toggleClass('animate__animated '+(animations[(random)])+' animate__delay-5s animate__slow animate__infinite');
            });
        }, 5000);


    });
</script>
