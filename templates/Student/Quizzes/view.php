<?php
/**
 * @var \App\View\AppView $this
 */
?>

<style>
    p, label, h1, h2, h3, h4, h5, h5, li{
        cursor: pointer !important;
    }
</style>

<script>
    'use strict';
    $(document).ready(function () {
        var timestart = moment('<?=$quiz->date.' '.$quiz->start_time?>');
        var timeend = moment('<?=$quiz->date.' '.$quiz->end_time?>');
        var currenttime = moment().format('Y-MM-DD H:mm:ss');
        var duration;

        setInterval(function () {
            currenttime = moment().format('Y-MM-DD H:mm:ss');

            if(currenttime > timestart.format('Y-MM-DD H:mm:ss') && currenttime > timeend.format('Y-MM-DD H:mm:ss')){
                window.location.reload();
                return true;
            }

        },10000);

    });
</script>

<?php foreach ($quiz->questions as $key => $question):?>
    <div class="row h-50">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
            <div class="tm-bg-white tm-block">
               <div id="speech-<?=(intval($key))?>">
                   <?=htmlspecialchars_decode($question->question)?>
               </div>
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <?=$this->Form->label('answer-'.$question->id,'Answer')?>
                    <?=$this->Form->text('answer_'.$key,[
                        'class' => 'form-control form-control-sm bg-info answers',
                        'id' => 'answer-'.$question->id,
                        'assessment-id' => $assessment->id,
                        'question-id' => $question->id
                    ])?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#speech-<?=(intval($key))?>').mouseenter(function () {
            window.speechSynthesis.cancel();
            var html = $(this).html();
            speech.text = ((html).replaceAll('<p>&nbsp;</p>', ' '))+ ' and Please type your answer below';
            // Start Speaking
            window.speechSynthesis.speak(speech);
        });
    </script>
<?php endforeach;?>

<script>
    'use strict';
    $(document).ready(function (e) {

        <?php foreach ($assessment->answers as $key => $answer):?>
            $('#answer-<?=$answer->question_id?>').val('<?=$answer->answer?>');
        <?php endforeach;?>

    });
</script>


<script>
   'use strict';
   $(document).ready(function(e){

       $('input.answers').on('input', function () {
           var questionId = $(this).attr('question-id');
           var assessmentId = $(this).attr('assessment-id');
           var value = $(this).val();
           answers(questionId, assessmentId, value);
       });

//       $('p, label, h1, h2, h3, h4, h5, h5, li').hover( function () {
//           window.speechSynthesis.cancel();
//           var text = $(this).text();
//           speech.text = text;
//           // Start Speaking
//           window.speechSynthesis.speak(speech);
//       });

       function answers(questionId, assessmentId, value) {
           const baseurl = mainurl+'answers/check';
           $.ajax({
               url:baseurl,
               type: 'POST',
               method: 'POST',
               data: {
                   'question_id' : questionId,
                   'assessment_id' : assessmentId,
                   'answer': value,
                   'points' : 0
               },
               dataType: 'JSON',
               headers:{
                   'X-CSRF-TOKEN': $('meta[name="csrfToken"]').attr('content')
               },
               beforeSend: function (e) {

               },
           }).done(function (data, status, xhr) {
               swal(data.message, data.result, 'success');
           }).fail(function (data, status, xhr) {
               const validation = data.responseJSON;

               $.map(validation.fields, function (value, key) {
                   var input = key;
                   $.map(value, function (value, key) {
                       $('strong[data-target="'+(input)+'"]').text(value);
                       $('input[name="'+(input)+'"]').addClass('border-danger');
                   });
               });

           });
       }

       function swal(message, result, icon) {
           Swal.fire({
               text:message,
               title:result,
               icon:icon,
               timer:5000,
               timerProgressBar:true,
               toast:true,
               position:'bottom-left',
               showConfirmButton:false
           });
       }

   });
</script>
