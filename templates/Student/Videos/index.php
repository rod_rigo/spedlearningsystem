<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Video[]|\Cake\Collection\CollectionInterface $videos
 */
?>

<style>
    .card:hover{
        cursor: pointer;
    }
</style>

<div class="row h-100"  data-anijs="if: scroll ,on: window, do: tada animated, to: .ani">

    <div class="col-sm-12 col-md-12 col-lg-12 tm-block-col">

        <div class="row" >

            <?php foreach ($videos as $key => $video):?>
                <div class="col-sm-12 col-md-6 col-lg-4 m-1 ani-js-<?=($key)?> ani" data-anijs="if: mouseenter, on: .ani-js-<?=($key)?>,  do: rubberBand animated, to: .ani-js-<?=($key)?>">
                    <div class="card">

                        <?php if(preg_match_all('/^(https:\/\/www\.|http:\/\/www\.).{1,}$/',$video->video)):?>
                        <embed src="<?=$video->video?>" style="height: 10em !important;">
                        <?php else:?>
                            <img src="<?=$this->Url->image('preview.jpg')?>" class="card-img-top img-thumbnail" style="height: 10em !important;" alt="Video Thumbnail">
                        <?php endif;?>

                        <div class="card-body">
                            <p class="card-text" style="font-size: 0.9em;"><?=$video->title?></p>
                            <?php if(preg_match_all('/^(https:\/\/www\.|http:\/\/www\.).{1,}$/',$video->video)):?>
                                <a href="<?=($video->video)?>" class="btn btn-small btn-block btn-blue" target="_blank">
                                    Watch Now <i class="fa fa-share"></i>
                                </a>
                            <?php else:?>
                                <a href="#" data-id="<?=$video->id?>" class="btn btn-small btn-block btn-blue video-sources" >
                                    Watch Now <i class="fa fa-play"></i>
                                </a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>

        </div>

    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {

        var cards = $('.card');
        var animations = [
                            'animate__bounce',
                            'animate__pulse',
                            'animate__rubberBand',
                            'animate__headShake',
                            'animate__swing',
                            'animate__tada',
                            'animate__wobble',
                            'animate__jello',
                            'animate__heartBeat',
                        ];

        $('a.video-sources').click(function (e) {
           e.preventDefault();
           var dataId = $(this).attr('data-id');
           window.location.replace(mainurl+'videos/watch/'+(dataId));
        });

        cards.addClass('animate__animated animate__fadeInDown animate__fast');

        setTimeout(function () {
            cards.removeClass('animate__animated animate__fadeInDown animate__fast');
        }, 3000);

       setTimeout(function () {
           cards.each(function (e) {
               var random = Math.floor(Math.random() * (parseInt(animations.length) + 1));
               $(this).toggleClass('animate__animated '+(animations[(random)])+' animate__delay-5s animate__slow animate__infinite');

           });
       }, 5000);


    });
</script>
