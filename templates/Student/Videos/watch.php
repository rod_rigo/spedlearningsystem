<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Video[]|\Cake\Collection\CollectionInterface $videos
 */
?>

<div class="row h-100">
    <div class="col-sm-12 col-md-12 col-lg-12 tm-block-col">
        <div class="tm-bg-primary-blue tm-block" style="min-height: 40em !important;" id="player">

            <video id="video-player" class="video-js" controls preload="auto" poster="<?=$this->Url->image('preview.jpg')?>" data-setup='{}' autoplay="autoplay" height="560" style="width: 100%; height: 18vh%;">
                <source src="<?=$video->video?>" type="video/mp4"
                <source src="<?=$video->video?>" type="video/webm">
                <source src="<?=$video->video?>" type="video/ogg">
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a web browser that.
                    <a href="https://videojs.com/html5-video-support/" target="_blank">
                        supports HTML5 video
                    </a>
                </p>
            </video>

        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 tm-block-col" id="videos">

        <div class="row">

            <?php foreach ($videos as $video):?>
                <div class="video-classes col-sm-12 col-md-6 col-lg-4 m-1">
                    <div class="card">
                        <?php if(preg_match_all('/^(https:\/\/www\.|http:\/\/www\.).{1,}$/',$video->video)):?>
                            <embed src="<?=$video->video?>" style="height: 10em !important;">
                        <?php else:?>
                            <img src="<?=$this->Url->image('preview.jpg')?>" class="card-img-top img-thumbnail" style="height: 10em !important;" alt="Video Thumbnail">
                        <?php endif;?>
                        <div class="card-body">
                            <p class="card-text" style="font-size: 0.9em;"><?=$video->title?></p>
                            <?php if(preg_match_all('/^(https:\/\/www\.|http:\/\/www\.).{1,}$/',$video->video)):?>
                                <a href="<?=($video->video)?>" class="btn btn-small btn-block btn-blue" target="_blank">
                                    Watch Now <i class="fa fa-share"></i>
                                </a>
                            <?php else:?>
                                <a href="#" data-id="<?=$video->id?>" class="btn btn-small btn-block btn-blue video-sources" >
                                    Watch Now <i class="fa fa-play"></i>
                                </a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>

        </div>

    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {

        var player = document.querySelector('#player');

        $('a.video-sources').click(function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            window.location.replace(mainurl+'videos/watch/'+(dataId));
        });

        $('.card').addClass('animate__animated animate__fadeInDown animate__faster');

        var video = videojs('video-player', {
            controlBar: {
                progressControl: false,
                remainingTimeDisplay: false,
                durationDisplay: false,
                currentTimeDisplay: false,
                timeDivider: false,
            },
        },function onPlayerReady() {
            videojs.log('Your player is ready!');

            // In this context, `this` is the player that was created by Video.js.
            this.play();

            // How about an event listener?
            this.on('ended', function() {
                videojs.log('Awww...over so soon?!');
            });

            player.scrollIntoView(true);

        });

        video.autoplay(true);
        video.muted(false);
        video.fullscreen(true);
        video.play();

    });
</script>
