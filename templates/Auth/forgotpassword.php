<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="full_container" id="pjax-response-container">
    <div class="container">
        <div class="center verticle_center full_height">
            <div class="login_section">
                <div class="logo_login">
                    <div class="center">
                        <?=$this->Html->image('/plutoAssets/images/logo/logo.png',[
                            'width' => '210'
                        ])?>
                    </div>
                </div>
                <div class="login_form">
                    <?= $this->Flash->render() ?>
                    <?= $this->Form->create($entity,['type' => 'file', 'id' => 'form']) ?>
                    <fieldset style="border: 0;">
                        <div class="field" style="display: flex; justify-content: flex-end;">
                            <?=$this->Form->label('email',ucwords('email'),[
                                'class' => 'label_field'
                            ])?>
                            <?=$this->Form->control('email',[
                                'placeholder' => ucwords('email'),
                                'label' => false,
                                'required' => true,
                            ])?>
                        </div>
                        <div class="field">
                            <label class="label_field hidden">hidden label</label>
                            <a class="forgot" href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Auth', 'action' => 'login'])?>">Return to login</a>
                        </div>
                        <div class="field margin_0">
                            <label class="label_field hidden">hidden label</label>
                            <?= $this->Form->submit(__('Submit'), ['class' => 'main_bt']); ?>
                        </div>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
       'use strict';

       const url = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', data.result, data.message);
            }).fail(function (data, status, xhr) {
                const json = data.responseJSON;
                swal('info', json.result, json.message);
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
