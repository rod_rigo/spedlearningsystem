<?php
/**
 * @var \App\View\AppView $this
 */
?>
<style>
    a{
        color: black;
        text-align: center;
        cursor: pointer;
    }
    .graph_head{
        text-align: center;
        display: flex;
        justify-content: center;
    }
</style>
<div class="full_container" id="pjax-response-container">
    <div class="container">
        <div class="center verticle_center full_height">
            <div class="login_section">
                <div class="logo_login">

                </div>
                <div class="login_form" id="login-as">
                    <div class="full graph_head">
                        <div class="heading1 margin_0">
                            <h2>Login As <a href="#" id="current-selected-role"></a></h2>
                        </div>
                    </div>
                    <?= $this->Form->create() ?>
                    <fieldset style="border: 0px;">
                        <?= $this->Flash->render() ?>
                        <div class="field" style="display: flex; justify-content: flex-end;">
                            <?=$this->Form->label('username',ucwords('Username'),[
                                'class' => 'label_field'
                            ])?>
                            <?=$this->Form->control('username',[
                                'placeholder' => ucwords('Username or Email'),
                                'label' => false,
                                'required' => true,
                            ])?>
                        </div>
                        <div class="field" style="display: flex; justify-content: flex-end;">
                            <?=$this->Form->label('password',ucwords('password'),[
                                'class' => 'label_field'
                            ])?>
                            <?=$this->Form->control('password',[
                                'placeholder' => ucwords('password'),
                                'label' => false,
                                'required' => true,
                            ])?>
                        </div>
                        <div class="field">
                            <label class="label_field hidden">hidden label</label>
                            <label class="form-check-label">
                                <?=$this->Form->control('remember_me',[
                                    'class' => 'form-check-input',
                                    'label' => false,
                                    'type' => 'checkbox',
                                    'hiddenField' => false,
                                ])?>
                                Remember Me</label>
                            <a class="forgot" href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Auth', 'action' => 'forgotpassword'])?>">Forgotten Password?</a>
                        </div>
                        <div class="field margin_0" style="display: block; justify-content: around;">
                            <?= $this->Form->hidden('role', ['id' => 'role']) ?>
                            <?= $this->Form->submit(__('Sign In'), ['class' => 'main_bt btn-block col-12', 'style' => 'width: 100%;']); ?>
                            <button type="button" id="back-role" class="btn btn-block btn-warning">Change Role</button>
                        </div>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>


                <div class="col-md-12" id="select-role">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="heading1 margin_0">
                                <h2>Login As</h2>
                            </div>
                        </div>
                        <div class="full inner_elements">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab_style3">
                                        <div class="tabbar padding_infor_info">
                                            <div class="nav flex-column nav-pills col-12" id="v-pills-tab" role="tablist" aria-orientation="vertical">
<!--                                                <a class="nav-link roles" data-role="is_admin" data-value="admin" data-toggle="pill" role="tab" aria-controls="v-pills-home" aria-selected="false">Admin</a>-->
                                                <a class="nav-link roles" data-role="is_teacher" data-value="teacher" data-toggle="pill" role="tab" aria-controls="v-pills-profile" aria-selected="false">Teacher</a>
                                                <a class="nav-link roles" data-role="is_parent" data-value="parent" data-toggle="pill" role="tab" aria-controls="v-pills-messages" aria-selected="false">Parent</a>
                                                <a class="nav-link roles" data-role="is_student" data-value="student" data-toggle="pill" role="tab" aria-controls="v-pills-settings" aria-selected="false">Student</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        var role = localStorage.getItem("role"),
            currenRoleValue = $(`a[data-role="${role}"]`),
            selectRole = $('#select-role'),
            loginAs = $('#login-as'),
            currentSelectedRole = $('#current-selected-role'),
            roleInput = $('#role')

        checkRole()
        chooseRoleAsActive()
        currentSelectedRole.html(currenRoleValue.data('value'))
        roleInput.val(role)

        $(document).on('click', '.roles', function(e){
            e.preventDefault()
            setTimeout(() => {
                let currentValue = $(this).data('value')
                let currentRole = $(this).data('role')
                localStorage.setItem("role", currentRole);
                role = currentRole
                checkRole()
                chooseRoleAsActive()
                currentSelectedRole.html(currentValue)
                roleInput.val(role)
            }, 300)
        })

        $('#back-role').on('click', function(e){
            e.preventDefault()
            selectRole.removeClass('d-none')
            loginAs.addClass('d-none')
        })

        function checkRole(){
            if(role == null){
                selectRole.removeClass('d-none')
                loginAs.addClass('d-none')
            }else{
                selectRole.addClass('d-none')
                loginAs.removeClass('d-none')
            }
        }

        function chooseRoleAsActive(){
            $(`a[data-role="${role}"]`).addClass('active show')
        }

    })
</script>
