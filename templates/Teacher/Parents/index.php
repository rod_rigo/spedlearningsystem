<?php $genders = [0 => 'Male', 1 => 'Female'] ?>

<div class="modal fade" id="parent-modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'parent-form']); ?>
            <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Parent's Information</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row m-3">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('last_name', ucwords('Last Name')) ?></strong>
                        <?= $this->Form->control('last_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Last Name'),
                        ]) ?>

                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('first_name', ucwords('Firstname')) ?></strong>
                        <?= $this->Form->control('first_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('First name'),
                        ]) ?>
                        <strong data-target="first_name"></strong>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('middle_name', ucwords('Middle Name')) ?></strong>
                        <?= $this->Form->control('middle_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Middle name'),
                        ]) ?>
                    </div>

                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('gender', ucwords('Gender')) ?></strong>
                        <?= $this->Form->control('gender', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'options' => $genders,
                            'placeholder' => ucwords('Gender'),
                        ]) ?>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('birthdate', ucwords('Birthdate')) ?></strong>
                        <?= $this->Form->control('birthdate', [
                            'type' => 'date',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Birthdate'),
                        ]) ?>
                    </div>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <strong class="text-dark"><?= $this->Form->label('address', ucwords('address')) ?></strong>
                        <?= $this->Form->control('address', [
                            'type' => 'textarea',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('address'),
                        ]) ?>
                    </div>

                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal">Close</button>
                <?= $this->Form->button('Save', [
                    'type' => 'submit',
                    'class' => 'btn btn-success rounded-0'
                ]) ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="modal fade" id="student-modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'student-form']); ?>
            <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Student's Information</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row m-3">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('last_name', ucwords('Last Name')) ?></strong>
                        <?= $this->Form->control('last_name', [
                            'class' => 'form-control rounded-0',
                            'id' => 'student-last-name',
                            'label' => false,
                            'placeholder' => ucwords('Last Name'),
                        ]) ?>

                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('first_name', ucwords('Firstname')) ?></strong>
                        <?= $this->Form->control('first_name', [
                            'class' => 'form-control rounded-0',
                            'id' => 'student-first-name',
                            'label' => false,
                            'placeholder' => ucwords('First name'),
                        ]) ?>
                        <strong data-target="first_name"></strong>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('middle_name', ucwords('Middle Name')) ?></strong>
                        <?= $this->Form->control('middle_name', [
                            'class' => 'form-control rounded-0',
                            'id' => 'student-middle-name',
                            'label' => false,
                            'placeholder' => ucwords('Middle name'),
                        ]) ?>
                    </div>

                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('gender', ucwords('Gender')) ?></strong>
                        <?= $this->Form->control('gender', [
                            'class' => 'form-control rounded-0',
                            'id' => 'student-gender',
                            'label' => false,
                            'options' => $genders,
                            'placeholder' => ucwords('Gender'),
                        ]) ?>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('birthdate', ucwords('Birthdate')) ?></strong>
                        <?= $this->Form->control('birthdate', [
                            'type' => 'date',
                            'class' => 'form-control rounded-0',
                            'id' => 'student-birthdate',
                            'label' => false,
                            'placeholder' => ucwords('Birthdate'),
                        ]) ?>
                    </div>

                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal">Close</button>
                <?= $this->Form->button('Save', [
                    'type' => 'submit',
                    'class' => 'btn btn-success rounded-0'
                ]) ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Parents List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Parent</th>
                                <th>Student</th>
                                <th>Student BirthDate</th>
                                <th>Student Gender</th>
                                <th>Address</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {

        var baseurl = mainurl+'parents/';
        var image = $('#image').attr('src');
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getParents',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 6,
                    data: null,render: function(data,type,row){
                    return  '<a data-id="'+(row.parent.id)+'" class="btn btn-sm btn-info text-white edit-parent">Edit Parent <i class="fa fa-pencil-square" aria-hidden="true"></i></a> | '+
                        '<a data-id="'+(row.student.id)+'" class="btn btn-sm btn-info text-white edit-student">Edit Student <i class="fa fa-pencil-square" aria-hidden="true"></i></a> | '+
                        '<a data-id="'+(row.id)+'" class="btn btn-sm btn-danger text-white delete">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                }
            ],
            columns: [
                { data: 'id'},
                {
                    data: null,
                    render: (data, type, row) => {
                        return `${row.parent.last_name}, ${row.parent.first_name}, ${row.parent.middle_name}`;
                    }
                },
                {
                    data: null,
                    render: (data, type, row) => {
                        return `${row.student.last_name}, ${row.student.first_name}, ${row.student.middle_name}`;
                    }
                },
                { data: 'student.birthdate'},
                {
                    data: 'student.gender',
                    render: (data,type,row) => {
                        return (!data) ? 'Male':'Female';
                    }
                },
                { data: 'parent.address'},
                { data: 'id'}
            ]
        });

        $('#parent-modal, #student-modal').on('hidden.bs.modal', function (e) {
            $('#parent-form, #student-form')[0].reset();
            $('input').removeClass('border-danger');
            $('#image').attr('src', image);
            $('strong[data-target]').empty();
        });

        $('#parent-form, #student-form').submit(function (e) {
            e.preventDefault();
            console.log(e)
            const data = new FormData(this);
            $.ajax({
                url: baseurl+url,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
            }).done(function (data, status, xhr) {
                console.log(data)
                swal('success', data.result, data.message);
                $('#parent-modal, #student-modal').modal('hide');
                $('#parent-form, #student-form')[0].reset();
                table.ajax.reload(null, false);
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;
                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });
            });
        });

        datatable.on('click','.edit-parent',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit-parent/'+dataId;
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    url = 'edit-parent/'+dataId
                },
            }).done(function (data, status, xhr) {
                // console.log(data)
                $('#parent-modal').modal('toggle');
                $('#first-name').val(data.first_name);
                $('#middle-name').val(data.middle_name);
                $('#last-name').val(data.last_name);
                $('#gender').val(data.gender);
                $('#birthdate').val(data.birthdate);
                $('#address').val(data.address);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.edit-student',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit-student/'+dataId;
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    url = 'edit-student/'+dataId
                },
            }).done(function (data, status, xhr) {
                // console.log(data)
                $('#student-modal').modal('toggle');
                $('#teacher-id').val(data.teacher_id);
                $('#student-first-name').val(data.first_name);
                $('#student-middle-name').val(data.middle_name);
                $('#student-last-name').val(data.last_name);
                $('#student-gender').val(data.gender);
                $('#student-birthdate').val(data.birthdate);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+dataId;
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    url = 'edit/'+dataId
                },
            }).done(function (data, status, xhr) {
                // console.log(data)
                $('#modal').modal('toggle');
                $('#first-name').val(data.first_name);
                $('#middle-name').val(data.middle_name);
                $('#last-name').val(data.last_name);
                $('#gender').val(data.gender);
                $('#birthdate').val(data.birthdate);
                $('#address').val(data.address);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        table.ajax.reload(null, false);
                        swal('success', data.result, data.message);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        $('#file').change(function (e) {
            var blob = URL.createObjectURL(e.target.files[0]);
            $('#image').attr('src', blob);
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
