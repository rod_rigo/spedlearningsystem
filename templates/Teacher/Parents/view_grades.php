<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Parents List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <?php foreach ($subjs as $subj) : ?>
                                        <th><?= $subj['subject'] ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($groupedResults as $result) : ?>
                                    <tr>
                                        <td>
                                            <?php  
                                                $firstName = $result['first_name'];
                                                $middleName = $result['middle_name'];
                                                $lastName = $result['last_name'];
                                    
                                                $fullName = "$lastName, $firstName, $middleName";
                                                echo $fullName;
                                            ?>
                                        </td>
                                        <?php foreach ($subjs as $subj) : ?>
                                            <td>
                                                <?= (array_key_exists($subj['subject'], $result['grades'])) ? $result['grades'][$subj['subject']]['grade'] : '0' ?>
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- groupedResults -->