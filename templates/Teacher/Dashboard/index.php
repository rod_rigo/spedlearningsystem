<div class="container-fluid">
    <div class="row column_title">
        <div class="col-md-12">
            <div class="page_title">
                <h2>Dashboard</h2>
            </div>
        </div>
    </div>
    <div class="row column1">
        <div class="col-md-4 col-lg-4">
            <div class="full counter_section margin_bottom_30">
                <div class="couter_icon">
                    <div>
                        <i class="fa fa-user yellow_color"></i>
                    </div>
                </div>
                <div class="counter_no">
                    <div>
                        <p class="total_no"><?= $students ?></p>
                        <p class="head_couter">Students Advisory</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="full counter_section margin_bottom_30">
                <div class="couter_icon">
                    <div>
                        <i class="fa fa-ellipsis-h green_color"></i>
                    </div>
                </div>
                <div class="counter_no">
                    <div>
                        <p class="total_no"><?= $subjects ?></p>
                        <p class="head_couter">Managed Subjects</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="full counter_section margin_bottom_30">
                <div class="couter_icon">
                    <div>
                        <i class="fa fa-pencil-square red_color"></i>
                    </div>
                </div>
                <div class="counter_no">
                    <div>
                        <p class="total_no"><?= $upcomingQuizzes ?></p>
                        <p class="head_couter">Upcoming Quizzes</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
            <div class="white_shd full">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>
                            Student Standing Base On Scores / All Subjects
                            <i class="fa fa-user"></i>
                        </h2>
                    </div>
                </div>
                <div class="full graph_revenue">
                    <div class="row m-3">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Rank</th>
                                        <th>Student</th>
                                        <th>Score</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $rank = 1; ?>
                                    <?php foreach ($standings as $result): ?>
                                        <tr>
                                            <td><?= $rank++ ?></td>
                                            <td><?= $result->last_name.', '.$result->first_name.', '.$result->middle_name ?></td>
                                            <td><?= $result->score ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
