
<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'form']); ?>
            <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row m-3">


                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <!-- <a href="#" id="preview-link" target="_blank" style="display: none;">Open File</a>
                            <iframe id="file-preview" style="display: none;"></iframe> -->

                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('title', [
                                    'class' => 'form-control rounded-0',
                                    'required' => true,
                                ]); ?>
                                <strong data-target="title"></strong>
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('file', [
                                    'class' => 'form-control rounded-0',
                                    'required' => false,
                                    'type' => 'file',
                                    'id' => 'fileInput',
                                    'accept' => '.mp4, .webm, .ogv'
                                ]); ?>
                                <strong data-target="file"></strong>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-md-3 col-lg-3 d-flex">
                                <label for="">URL</label>
                                <?= $this->Form->control('type', [
                                    'type' => 'checkbox',
                                    'class' => 'form-control rounded-0',
                                    'style' => 'margin-top: 4px; margin-left: 8px;',
                                    'required' => false,
                                    'value' => true,
                                    'checked' => false,
                                    'label' => false,
                                ]); ?>
                                <strong data-target="type"></strong>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal">Close</button>
                <?= $this->Form->button('Save', [
                    'type' => 'submit',
                    'class' => 'btn btn-success rounded-0'
                ]) ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Audios List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Teacher</th>
                                <th>Title</th>
                                <th>Audio</th>
                                <th>Presound</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {

        var baseurl = mainurl+'audios/';
        var image = $('#image').attr('src');
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getAudios',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 5,
                    data: null,render: function(data,type,row){
                    return  '<a data-id="'+(row.id)+'" class="btn btn-sm btn-info text-white edit">Edit <i class="fa fa-pencil-square" aria-hidden="true"></i></a> | '+
                        '<a data-id="'+(row.id)+'" class="btn btn-sm btn-danger text-white delete">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                }
            ],
            columns: [
                { data: 'id'},
                {
                    data: 'teacher_id',
                    render: (data,type,row) => {
                        return data;
                    }
                },
                { data: 'title'},
                { data: 'audio'},
                {
                    data: 'audio',
                    render: (data, type, row) => {
                        return `<audio controls>
                                <source src="${data}" type="audio/mpeg">
                                <source src="${data}" type="audio/ogg">
                                <source src="${data}" type="audio/wav">
                                Your browser does not support the audio tag.
                            </audio>`;
                    }
                },
                { data: 'id'}
            ]
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            $('#form')[0].reset();
            $('input').removeClass('border-danger');
            $('#image').attr('src', image);
            $('strong[data-target]').empty();
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl+url,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
            }).done(function (data, status, xhr) {
                console.log(data)
                swal('success', data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload(null, false);
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;
                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+dataId;
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    url = 'edit/'+dataId
                },
            }).done(function (data, status, xhr) {
                let fileInput = $('#fileInput');
                // console.log(data)
                $('#modal').modal('toggle');
                if(data.type){
                    fileInput.attr('type', 'text').attr('name', 'video').val(data.video)
                    fileInput.prev().html('URL')
                }else{
                    fileInput.attr('type', 'file').attr('name', 'file')
                    fileInput.prev().html('File')
                }
                // $('#fileInput').val(data.module);
                // $('#subject-id').val(data.subject_id);
                $('#type').prop('checked', data.type);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        $('#type').on('change', () => {
            // alert($('#type').is(':checked'))
            let fileInput = $('#fileInput');
            if($('#type').is(':checked')){
                fileInput.attr('type', 'text').attr('name', 'video')
                fileInput.prev().html('URL')
            }else{
                fileInput.attr('type', 'file').attr('name', 'file')
                fileInput.prev().html('File')
            }
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        table.ajax.reload(null, false);
                        swal('success', data.result, data.message);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        $('#file').change(function (e) {
            var blob = URL.createObjectURL(e.target.files[0]);
            $('#image').attr('src', blob);
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
