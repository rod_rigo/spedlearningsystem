<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DomainEvaluation[]|\Cake\Collection\CollectionInterface $domainEvaluations
 */
?>
<div class="domainEvaluations index content">
    <?= $this->Html->link(__('New Domain Evaluation'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Domain Evaluations') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('student_id') ?></th>
                    <th><?= $this->Paginator->sort('evaluation_id') ?></th>
                    <th><?= $this->Paginator->sort('domain_id') ?></th>
                    <th><?= $this->Paginator->sort('is_checked') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($domainEvaluations as $domainEvaluation): ?>
                <tr>
                    <td><?= $this->Number->format($domainEvaluation->id) ?></td>
                    <td><?= $domainEvaluation->has('student') ? $this->Html->link($domainEvaluation->student->id, ['controller' => 'Students', 'action' => 'view', $domainEvaluation->student->id]) : '' ?></td>
                    <td><?= $domainEvaluation->has('evaluation') ? $this->Html->link($domainEvaluation->evaluation->id, ['controller' => 'Evaluations', 'action' => 'view', $domainEvaluation->evaluation->id]) : '' ?></td>
                    <td><?= $domainEvaluation->has('domain') ? $this->Html->link($domainEvaluation->domain->id, ['controller' => 'Domains', 'action' => 'view', $domainEvaluation->domain->id]) : '' ?></td>
                    <td><?= $this->Number->format($domainEvaluation->is_checked) ?></td>
                    <td><?= h($domainEvaluation->created) ?></td>
                    <td><?= h($domainEvaluation->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $domainEvaluation->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $domainEvaluation->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $domainEvaluation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $domainEvaluation->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
