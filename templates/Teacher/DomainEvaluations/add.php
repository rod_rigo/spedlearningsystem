<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DomainEvaluation $domainEvaluation
 * @var \Cake\Collection\CollectionInterface|string[] $students
 * @var \Cake\Collection\CollectionInterface|string[] $evaluations
 * @var \Cake\Collection\CollectionInterface|string[] $domains
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Domain Evaluations'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="domainEvaluations form content">
            <?= $this->Form->create($domainEvaluation) ?>
            <fieldset>
                <legend><?= __('Add Domain Evaluation') ?></legend>
                <?php
                    echo $this->Form->control('student_id', ['options' => $students, 'empty' => true]);
                    echo $this->Form->control('evaluation_id', ['options' => $evaluations]);
                    echo $this->Form->control('domain_id', ['options' => $domains]);
                    echo $this->Form->control('is_checked');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
