<?= $this->Form->create($lessonPlan, ['type' => 'file', 'id' => 'form', 'enctype' => 'multipart/form-data']) ?>
<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Register New Lesson Plan
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">

                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <!-- <a href="#" id="preview-link" target="_blank" style="display: none;">Open File</a>
                            <iframe id="file-preview" style="display: none;"></iframe> -->

                            <!-- Monday -->
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('monday', [
                                    'options' => $subjects,
                                    'empty' => 'Select Subject',
                                    'class' => 'form-control rounded-0',
                                    'required' => true,
                                ]); ?>
                                <strong data-target="monday"></strong>
                            </div>
                            <!-- Tuesday -->
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('tuesday', [
                                    'options' => $subjects,
                                    'empty' => 'Select Subject',
                                    'class' => 'form-control rounded-0',
                                    'required' => true,
                                ]); ?>
                                <strong data-target="tuesday"></strong>
                            </div>
                            <!-- Wednesday -->
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('wednesday', [
                                    'options' => $subjects,
                                    'empty' => 'Select Subject',
                                    'class' => 'form-control rounded-0',
                                    'required' => true,
                                ]); ?>
                                <strong data-target="wednesday"></strong>
                            </div>
                            <!-- Thursday -->
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('thursday', [
                                    'options' => $subjects,
                                    'empty' => 'Select Subject',
                                    'class' => 'form-control rounded-0',
                                    'required' => true,
                                ]); ?>
                                <strong data-target="thursday"></strong>
                            </div>
                            <!-- Friday -->
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('friday', [
                                    'options' => $subjects,
                                    'empty' => 'Select Subject',
                                    'class' => 'form-control rounded-0',
                                    'required' => true,
                                ]); ?>
                                <strong data-target="friday"></strong>
                            </div>

                        </div>
                    </div>


                    <div class="bottom_list">

                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                        <?= $this->Form->button('Save', [
                            'class' => 'btn btn-lg btn-success rounded-0',
                            'type' => 'submit',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->Form->end() ?>

<script>
    $(function () {
        'use strict';

        var url = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                swal(data.message, data.result, (data.result.charAt(0).toLowerCase() + data.result.slice(1)));
                $('#form')[0].reset();
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;

                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="' + (input) + '"]').text(value);
                        $('input[name="' + (input) + '"]').addClass('border-danger');
                    });
                });

            });
        });

        // var fileInput = document.getElementById('file');
        // var previewLink = document.getElementById('preview-link');
        // var filePreview = document.getElementById('file-preview');

        // fileInput.addEventListener('change', function () {
        //     var selectedFile = fileInput.files[0];

        //     if (selectedFile) {
        //         previewLink.style.display = 'inline-block';

        //         if (
        //             selectedFile.type === 'application/pdf' ||
        //             selectedFile.type === 'application/msword' ||
        //             selectedFile.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
        //             selectedFile.type === 'application/vnd.ms-powerpoint' ||
        //             selectedFile.type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        //         ) {
        //             // PDF, DOCX, or PPT file - Display in an iframe
        //             filePreview.src = URL.createObjectURL(selectedFile);
        //             filePreview.style.display = 'block';

        //             // Update the link to open the file
        //             previewLink.href = URL.createObjectURL(selectedFile);
        //             previewLink.textContent = 'Open Selected File';
        //         } else {
        //             // Unsupported file type
        //             alert('Unsupported file type');
        //             previewLink.style.display = 'none';
        //             filePreview.style.display = 'none';
        //         }
        //     } else {
        //         previewLink.style.display = 'none';
        //         filePreview.style.display = 'none';
        //     }
        // });

        function swal(message, result, icon) {
            Swal.fire({
                text: message,
                title: result,
                icon: icon,
                timer: 5000,
                timerProgressBar: true,
            });
        }

    });
</script>
