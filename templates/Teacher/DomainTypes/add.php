<?= $this->Form->create($domainTypes, ['type' => 'file', 'id' => 'form', 'enctype' => 'multipart/form-data']) ?>
<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Register New Domain Type
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">

                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->control('name', [
                                    'class' => 'form-control rounded-0',
                                    'required' => true,
                                ]); ?>
                                <strong data-target="name"></strong>
                            </div>
                        </div>
                    </div>


                    <div class="bottom_list">

                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                        <?= $this->Form->button('Save', [
                            'class' => 'btn btn-lg btn-success rounded-0',
                            'type' => 'submit',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->Form->end() ?>

<script>
    $(function () {
        'use strict';

        var url = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                swal(data.message, data.result, 'success');
                $('#form')[0].reset();
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;

                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="' + (input) + '"]').text(value);
                        $('input[name="' + (input) + '"]').addClass('border-danger');
                    });
                });

            });
        });

        $('input').on('input', function () {
            $(this).removeClass('border-danger')
        });

        function swal(message, result, icon) {
            Swal.fire({
                text: message,
                title: result,
                icon: icon,
                timer: 5000,
                timerProgressBar: true,
            });
        }

    });
</script>
