<?php $no = 1; ?>
<div class="row column2 graph margin_bottom_30">
    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
            <?= $this->Html->link(__('Back'), $this->request->referer(), ['class' => 'btn btn-warning float-right pull-right col-3']) ?>
                <div class="heading1 margin_0">
                    <h2>
                        Student <?= $data->student->last_name.', '.$data->student->first_name.', '.$data->student->middle_name ?> Assessment
                        <i class="fa fa-user"></i>

                    </h2>
                </div>
            </div>

            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Questions</th>
                                    <th>Answers</th>
                                    <th>Student Answers</th>
                                    <th>Points</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data->answers as $answers): ?>
                                    <tr>
                                        <td><?= $no ?></td>
                                        <td><?= htmlspecialchars_decode($answers->question->question) ?></td>
                                        <td><?= $answers->question->answer ?></td>
                                        <td><?= $answers->answer ?></td>
                                        <td><?= $answers->points.'/'.$answers->question->points ?></td>
                                    </tr>
                                    <?php $no++ ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
        });

        datatable.on('click','.mark',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var dataMark = $(this).attr('data-mark');
            var href = baseurl+'mark/'+dataId;
            Swal.fire({
                title: `Mark as ${dataMark}?`,
                text: `Are You Sure you want to Mark as ${dataMark}?`,
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'GET',
                        method: 'GET',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        table.ajax.reload(null, false);
                        swal('success', data.result, data.message);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('error', response.result, response.message);
                    });
                }
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
