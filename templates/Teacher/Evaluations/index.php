<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'form']); ?>
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Evaluation Form</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row m-3">
                    <div id="evaluation-form" class="col-12">

                    </div>
                    <!-- <div class="col-sm-12 col-lg-12 col-md-12">
                        <div class="row m-3 col-12">
                            <strong class="text-dark"><?= $this->Form->label('content', ucwords('content')) ?></strong>
                            <div id="CKEditorDiv"></div>
                        </div>
                        <textarea name="content" id="textarea-content" hidden></textarea>
                    </div>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <strong class="text-dark"><?= $this->Form->label('assessment', ucwords('Overall Assessment')) ?></strong>
                        <?= $this->Form->control('assessment', [
                            'type' => 'text',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Overall Assessment'),
                        ]) ?>

                    </div> -->
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal">Close</button>
                <?= $this->Form->button('Save', [
                    'type' => 'submit',
                    'class' => 'btn btn-success rounded-0'
                ]) ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Evaluations List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function() {

        var baseurl = mainurl + 'evaluations/';
        var image = $('#image').attr('src');
        var url = '';
        let getEditor;
        let globalMonths, globalApi;

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy: true,
            processing: true,
            responsive: true,
            serchDelay: 3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order: [
                [0, 'asc']
            ],
            ajax: {
                url: baseurl + 'getEvaluations',
                method: 'GET',
                dataType: 'JSON',
                complete: () => {
                    globalApi.cells('tr', 1).every(function() {
                        var data = this.data();
                        $(this.node()).html(`<a data-student="${data}" class="btn btn-sm btn-info text-white evaluate"> Evaluation</a>`);

                    });
                },
            },
            initComplete: function(settings, json) {
                globalMonths = json.months
                console.log(globalMonths)
                globalApi = this.api();

                // Add a button to each cell in the third column (index 1)
                globalApi.cells('tr', 1).every(function() {
                    var data = this.data();
                    $(this.node()).html(`<a data-student="${data}" class="btn btn-sm btn-info text-white evaluate"> Evaluation</a>`);

                });

            },
            columnDefs: [{
                targets: 0,
                render: function(data, type, full, meta) {
                    const row = meta.row;
                    return row + 1;
                }
            }, ],
            columns: [
                {
                    data: null,
                    render: (data, type, row) => {
                        return `${row.last_name}, ${row.first_name}, ${row.middle_name}`
                    }
                },
                {
                    data: 'id'
                }
            ]
        });

        $('#modal').on('hidden.bs.modal', function(e) {
            $('#form')[0].reset();
            $('input').removeClass('border-danger');
            $('strong[data-target]').empty();
        });

        $('#form').submit(function(e) {
            e.preventDefault();
            swal('info', "Loading..", "Please wait while saving evaluation..")
            const data = new FormData(this);
            $.ajax({
                url: baseurl + url,
                method: 'POST',
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
            }).done(function(data, status, xhr) {
                console.log(data)
                swal(data.result.toLowerCase(), data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload();
            }).fail(function(data, status, xhr) {
                const validation = data.responseJSON;
                $.map(validation.fields, function(value, key) {
                    var input = key;
                    $.map(value, function(value, key) {
                        $('strong[data-target="' + (input) + '"]').text(value);
                        $('input[name="' + (input) + '"]').addClass('border-danger');
                    });
                });
            });
        });

        datatable.on('click', '.evaluate', async function(e) {
            swal('info', "Loading..", "Please wait while detecting evaluation..")
            e.preventDefault();
            var dataStudentId = $(this).attr('data-student');
            var href = baseurl + 'createOrUpdateEvaluation/' + dataStudentId
            $.ajax({
                url: href,
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function(e) {
                    url = 'createOrUpdateEvaluation/' + dataStudentId
                },
            }).done(async function(data, status, xhr) {
                console.log(data)
                let containerToBeDisplayed = $('#evaluation-form')
                containerToBeDisplayed.html(await displayDomains(data.domains))
                containerToBeDisplayed.append(await displayPerDomain(data.eachDomainEval))
                containerToBeDisplayed.append(await displayEvaluationScores(data.evaluation))
                $('#modal').modal('toggle');
                swal('success', "Success..", "Evaluation is ready")
                // $('#teacher-id').val(data.teacher_id);
            }).fail(function(xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        async function displayPerDomain(data){ // eachDomainEval
            let str = '', first = 0;
            str += ``
            for (let key in data) {
                if(first == 0){
                    str += `<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="color: black; background-color: yellow;" rowspan="2">Domains Evaluation Summary </th>
                                <th colspan="3">${data[key][0].month}</th>
                                <th colspan="3">${data[key][1].month}</th>
                            </tr>
                            <tr>
                                <th>Raw Score</th>
                                <th>Scaled Score</th>
                                <th>Evaluations</th>
                                <th>Raw Score</th>
                                <th>Scaled Score</th>
                                <th>Evaluations</th>
                            </tr>
                        <thead>
                    <tbody>`
                }
                // console.log(key + ': ' + data[key], data[key][0].month);
                str += `
                    <tr>
                        <td style="color: black; background-color: yellow;">${key} </td>
                        <td>${data[key][0].raw_score}</td>
                        <td>${data[key][0].scaled_score}</td>
                        <td>${data[key][0].interpretation}</td>
                        <td>${data[key][1].raw_score}</td>
                        <td>${data[key][1].scaled_score}</td>
                        <td>${data[key][1].interpretation}</td>
                    </tr>
                `
                first++;
            }
            str += `<tbody></table>`
            return str;
        }

        // globalMonths variable
        async function displayDomains(data) {
            // console.log(data)
            let valueToAppend = '',
                inputCount = 0;

            Object.keys(data).forEach(function(key) {
                // console.log(key)
                count = 1
                valueToAppend += `<table class="table table-bordered"><thead>`
                valueToAppend += `
                    <tr>
                        <th></th>
                        <th style="color: black; background-color: yellow;">${key}</th>
                        <th>${globalMonths[0].month}</th>
                        <th>${globalMonths[1].month}</th>
                    </tr>
                `
                valueToAppend += `</thead><tbody>`
                data[key].forEach(async function(value, index) {
                    // console.log(value)
                    valueToAppend += `
                        <tr>
                            <td>${count}</td>
                            <td>${value.domain}</td>
                            <td>
                                <input type="checkbox" name="first_domain_evaluations[${inputCount}][is_checked]" ${
                                    (value.domain_evaluations?.[0]?.evaluation?.evaluation_month_id == globalMonths[0].id) ?
                                    (value.domain_evaluations?.[0]?.is_checked == 1) ? 
                                    'checked':'':(value.domain_evaluations?.[1]?.is_checked == 1) ? 'checked':''
                                } value="1" />${globalMonths[0].month}
                                <input type="hidden" name="first_domain_evaluations[${inputCount}][domain_id]" value="${value.id}" />
                                <input type="hidden" name="first_domain_evaluations[${inputCount}][domain_name]" value="${value.domain}" />
                                <input type="hidden" name="first_domain_evaluations[${inputCount}][month]" value="${globalMonths[0].id}" />
                            </td>
                            <td>
                                <input type="checkbox" name="second_domain_evaluations[${inputCount + 1}][is_checked]" ${
                                    (value.domain_evaluations?.[1]?.evaluation?.evaluation_month_id == globalMonths[1].id) ?
                                    (value.domain_evaluations?.[1]?.is_checked == 1) ? 
                                    'checked':'':(value.domain_evaluations?.[0]?.is_checked == 1) ? 'checked':''
                                } value="1" />${globalMonths[1].month}
                                <input type="hidden" name="second_domain_evaluations[${inputCount + 1}][domain_id]" value="${value.id}" />
                                <input type="hidden" name="second_domain_evaluations[${inputCount + 1}][domain_name]" value="${value.domain}" />
                                <input type="hidden" name="second_domain_evaluations[${inputCount + 1}][month]" value="${globalMonths[1].id}" />
                            </td>
                        </tr>
                    `
                    count++
                    inputCount += 2
                })
                valueToAppend += `</tbody></table>`
                // console.log(key, data[key]);
            });
            return valueToAppend
        }

        async function displayEvaluationScores(data) {
            let str = '';
            str += `<table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="color: black; background-color: yellow;">Months</th>
                        <th>Raw Score Total</th>
                        <th>Scaled Score Total</th>
                        <th>Standard Score</th>
                        <th>Interpretation</th>
                    </tr>
                <thead>
                <tbody>`
            data.forEach(async (value, index) => {
                str += `
                    <tr>
                        <td style="color: black; background-color: yellow;">${value.evaluation_month.month}</td>
                        <td>${value.raw_score_total}</td>
                        <td>${value.scaled_score_total}</td>
                        <td>${value.standard_score}</td>
                        <td>${value.interpretation}</td>
                    </tr>
                `
            })
            str += `<tbody></table>`
            return str;

        }

        function swal(icon, result, message) {
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000
            });
        }
    });
</script>