<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question $question
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Question'), ['action' => 'edit', $question->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Question'), ['action' => 'delete', $question->id], ['confirm' => __('Are you sure you want to delete # {0}?', $question->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Questions'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Question'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="questions view content">
            <h3><?= h($question->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Quiz') ?></th>
                    <td><?= $question->has('quiz') ? $this->Html->link($question->quiz->id, ['controller' => 'Quizzes', 'action' => 'view', $question->quiz->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Answers') ?></th>
                    <td><?= h($question->answers) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($question->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Points') ?></th>
                    <td><?= $this->Number->format($question->points) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($question->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($question->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Deleted') ?></th>
                    <td><?= h($question->deleted) ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Question') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($question->question)); ?>
                </blockquote>
            </div>
            <div class="related">
                <h4><?= __('Related Answers') ?></h4>
                <?php if (!empty($question->answers)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Assessment Id') ?></th>
                            <th><?= __('Question Id') ?></th>
                            <th><?= __('Answer') ?></th>
                            <th><?= __('Points') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Deleted') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($question->answers as $answers) : ?>
                        <tr>
                            <td><?= h($answers->id) ?></td>
                            <td><?= h($answers->assessment_id) ?></td>
                            <td><?= h($answers->question_id) ?></td>
                            <td><?= h($answers->answer) ?></td>
                            <td><?= h($answers->points) ?></td>
                            <td><?= h($answers->created) ?></td>
                            <td><?= h($answers->modified) ?></td>
                            <td><?= h($answers->deleted) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Answers', 'action' => 'view', $answers->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Answers', 'action' => 'edit', $answers->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Answers', 'action' => 'delete', $answers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $answers->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Choices') ?></h4>
                <?php if (!empty($question->choices)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Question Id') ?></th>
                            <th><?= __('Title') ?></th>
                            <th><?= __('Answer') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Deleted') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($question->choices as $choices) : ?>
                        <tr>
                            <td><?= h($choices->id) ?></td>
                            <td><?= h($choices->question_id) ?></td>
                            <td><?= h($choices->title) ?></td>
                            <td><?= h($choices->answer) ?></td>
                            <td><?= h($choices->created) ?></td>
                            <td><?= h($choices->modified) ?></td>
                            <td><?= h($choices->deleted) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Choices', 'action' => 'view', $choices->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Choices', 'action' => 'edit', $choices->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Choices', 'action' => 'delete', $choices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $choices->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
