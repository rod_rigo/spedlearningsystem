<?php
    define('CATEGORY', [
        'LETTERS ABC' => 'LETTERS ABC',
        'COLORS' => 'COLORS',
        'SHAPES' => 'SHAPES',
        'NUMBERS 123' => 'NUMBERS 123',
        'BODY PARTS' => 'BODY PARTS'
    ]);
?>


<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'form']); ?>
            <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Quiz Form</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row m-3">

                    <!-- <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('subject_id', ucwords('Subject')) ?></strong>
                        <?= $this->Form->control('subject_id', [
                            'options' => $subjects,
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Subject'),
                        ]) ?>
                    </div> -->

                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('quiz', ucwords('Quiz Title')) ?></strong>
                        <?= $this->Form->control('quiz', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Quiz Title'),
                        ]) ?>
                    </div>
                    <!-- Quiz Type -->
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <strong class="text-dark"><?= $this->Form->label('quiz_type', ucwords('Quiz Type')) ?></strong>
                        <select name="quiz_type" id="quiz_type" class="form-control rounded-0" required>
                            <option value="">-- Select Quiz Type --</option>
                            <option value="Assignment">Assignment</option>
                            <option value="Activity">Activity</option>
                        </select>
                    </div>

                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <strong class="text-dark"><?= $this->Form->label('date', ucwords('Date')) ?></strong>
                        <?= $this->Form->control('date', [
                            'type' => 'date',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Date'),
                        ]) ?>
                    </div>

                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('start_time', ucwords('Start Time')) ?></strong>
                        <?= $this->Form->control('start_time', [
                            'type' => 'time',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Start Time'),
                        ]) ?>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('end_time', ucwords('End Time')) ?></strong>
                        <?= $this->Form->control('end_time', [
                            'type' => 'time',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('End Time'),
                        ]) ?>
                    </div>

                    <div class="col-12">
                        <strong class="text-dark"><?= $this->Form->label('end_time', ucwords('Category')) ?></strong>
                        <?= $this->Form->control('category', [
                            'options' => CATEGORY,
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Category'),
                        ]) ?>
                    </div>

                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal">Close</button>
                <?= $this->Form->button('Save', [
                    'type' => 'submit',
                    'class' => 'btn btn-success rounded-0'
                ]) ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Quizzes List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
                <button class="btn btn-primary float-right col-4" id="add">New Quiz</button>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>Teacher</th>
                                <th>Subject</th>
                                <th>Quiz Type</th>
                                <th>Quiz</th>
                                <th>Date</th>
                                <th>Special Quiz?</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Category</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {

        var baseurl = mainurl+'quizzes/';
        var image = $('#image').attr('src');
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getQuizzes',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 8,
                    data: null,render: function(data,type,row){
                    return '<a data-id="'+(row.id)+'" href="'+(baseurl)+'questions/'+(row.id)+'" class="btn btn-sm btn-primary text-white edit-questions">Edit Questions <i class="fa fa-pencil-square" aria-hidden="true"></i></a> | '+
                        '<a data-id="'+(row.id)+'" class="btn btn-sm btn-info text-white edit">Edit <i class="fa fa-pencil-square" aria-hidden="true"></i></a> | '+
                        '<a data-id="'+(row.id)+'" class="btn btn-sm btn-danger text-white delete">Delete <i class="fa fa-trash" aria-hidden="true"></i></a> | '+
                        `<a data-id="${row.id}" class="btn btn-sm btn-info text-white special">${(row.is_special) ? "Make As Normal Quiz":"Make As Special Quiz"} <i class="fa fa-trash" aria-hidden="true"></i></a> | `
                }
                }
            ],
            columns: [
                { 
                    data: 'teacher_id',
                    render: (data, type, row) => {
                        return `${row.teacher.last_name}, ${row.teacher.first_name}, ${row.teacher.middle_name}`;
                    }
                },
                { 
                    data: 'subject_id',
                    render: (data, type, row) => {
                        return `${row.subject.subject}`;
                    }
                },
                { data: 'quiz_type'},
                { data: 'quiz'},
                { data: 'date'},
                { 
                    data: 'is_special',
                    render: (data, type, row) => {
                        return (data) ? "Special Quiz":"Normal Quiz";
                    }
                },
                { data: 'start_time'},
                { data: 'end_time'},
                { data: 'category'},
                { data: 'id'}
            ]
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            $('#form')[0].reset();
            $('input').removeClass('border-danger');
            $('#image').attr('src', image);
            $('strong[data-target]').empty();
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl+url,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
            }).done(function (data, status, xhr) {
                console.log(data)
                if(data.url){
                    swal('success', data.result, `${data.message}. Redirecting...`);
                    window.location = `${baseurl}${data.url}`
                }else{
                    swal('success', data.result, `${data.message}.`);
                }
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload(null, false);
            }).fail(function (data, status, xhr) {
                console.log(data)
                const validation = data.responseJSON;
                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });
            });
        });

        datatable.on('click','.special',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'special/'+dataId;
            Swal.fire({
                title: `Update Quiz?`,
                text: `Are You Sure you want to Update this quiz?`,
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'GET',
                        method: 'GET',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        table.ajax.reload(null, false);
                        swal('success', data.result, data.message);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('error', response.result, response.message);
                    });
                }
            });
        })

        $("#add").on('click', (e) => {
            e.preventDefault();
            url = 'add/'
            $('#modal').modal('toggle');
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+dataId;
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    url = 'edit/'+dataId
                },
            }).done(function (data, status, xhr) {
                // console.log(data)
                $('#modal').modal('toggle');
                $('#subject-id').val(data.subject_id);
                $('#date').val(data.date);
                $('#quiz').val(data.quiz);
                $('#quiz_type').val(data.quiz_type);
                $('#start-time').val(data.start_time);
                $('#end-time').val(data.end_time);
                $('#category').val(data.category);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        table.ajax.reload(null, false);
                        swal('success', data.result, data.message);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
