<style>
    .inner-shadow {
        width: 200px;
        height: 200px;
        background-color: #fff;
        /* Background color for the div */
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.5);
        /* Inset shadow effect */
    }

    .outer-shadow {
        /* background-color: #fff; */
        /* Background color for the div */
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.5);
        /* Outer shadow effect */
    }

    .custom-scrollbar {
        width: 300px;
        height: 200px;
        overflow-y: auto;
        /* Enable vertical scrollbar */
        scrollbar-width: thin;
        /* Width of the scrollbar (for Firefox) */
        scrollbar-color: darkgrey lightgrey;
        /* Color of the scrollbar thumb and track (for Firefox) */
    }

    /* Webkit browsers (like Chrome, Safari) */
    .custom-scrollbar::-webkit-scrollbar {
        width: 10px;
        /* Width of the scrollbar */
    }

    .custom-scrollbar::-webkit-scrollbar-track {
        background: lightgrey;
        /* Track color */
    }

    .custom-scrollbar::-webkit-scrollbar-thumb {
        background: darkgrey;
        /* Thumb color */
        border-radius: 5px;
        /* Rounded corners for the thumb */
    }
</style>

<div class="">
    <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'form']); ?>
    <div class="column4 graph col-12" id="questions">

    </div>
    <div class="container d-flex justify-content-center col-12">
        <div class="col-3">
            <button id="addQuestion" class="btn btn-primary rounded-1 btn-block outer-shadow">Add Question</button>
        </div>
        <div class="col-3">
            <button id="deleteQuestion" class="btn btn-danger rounded-1 btn-block outer-shadow">Delete Last Question</button>
        </div>
        <div class="col-6">
            <?= $this->Form->button('Save', [
                'type' => 'submit',
                'class' => 'btn btn-success rounded-1 btn-block outer-shadow'
            ]) ?>
        </div>
    </div>

    <?= $this->Form->end(); ?>
</div>


<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit question</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row m-3 col-12">
                    <label style="color: black;" for="">Question: </label><span style="color: red;">* Required field</span>
                    <div id="CKEditorDiv"></div>
                </div>

                <div class="row m-3 col-12">
                    <label style="color: black;" for="">Correct Answer: </label><span style="color: red;">* Required field</span>
                    <input type="text" id="answer-field" class="form-control form-control-md rounded-0 outer-shadow">
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal">Close</button>
                <?= $this->Form->button('Save', [
                    'type' => 'button',
                    'id' => 'save-question',
                    'class' => 'btn btn-success rounded-0'
                ]) ?>
            </div>
        </div>
    </div>


    <script>
        $(function() {
            var questionNumber = 0,
                questions = $('#questions'),
                addQuestion = $('#addQuestion'),
                deleteQuestion = $('#deleteQuestion'),
                getCKEditor,
                answerField = $('#answer-field'),
                saveQuestion = $('#save-question'),
                targetId

            saveQuestion.on('click', function(e) {
                e.preventDefault();
                $(`#question-preview-${targetId}`).html(getEditor.getData())
                $(`#question-${targetId}`).val(getEditor.getData())
                $(`#answer-${targetId}`).val(answerField.val())
                $('#modal').modal('hide')
                getEditor.setData()
                answerField.val('')
            })

            addQuestion.on('click', (e) => {
                e.preventDefault();
                console.log('appending')
                questionNumber++
                questions.append(questionTemplate(questionNumber))
            })

            deleteQuestion.on('click', (e) => {
                e.preventDefault();
                console.log('deleting')
                $(`[data-row="${questionNumber}"]`).remove()
                questionNumber--;
            })

            $(document).on('click', '.edit', (e) => {
                e.preventDefault();
                targetId = $(e.target).data('id')
                getEditor.setData($(`#question-${targetId}`).val())
                answerField.val($(`#answer-${targetId}`).val())
                $('#modal').modal('show')
            })

            // Get the questions of current quiz if there are any quizzes
            $.ajax({
                url: window.location,
                method: 'GET',
                type: 'GET',
                cache: false,
                contentType: false,
                processData: false,
            }).done(function(data, status, xhr) {
                console.log(data)
                var newData = $.map(data, function(item) {
                    questionNumber++;
                    return questionTemplate(questionNumber, item);
                });
                // console.log(newData)
                questions.append(newData.join(''))
                // console.log(joinedString)
            }).fail(function(data, status, xhr) {
                console.log(data)
            });

            function questionTemplate(withNumber, data = '') {
                // <h2>Question ${withNumber}</h2>
                return `
                <div class="col-md-12 col-12 mt-2" data-row="${withNumber}">
                    <div class="white_shd full margin_bottom_30 col-12">
                        <div class="full inner_elements col-12">
                            <div class="">
                            <div class="col-md-12 col-12">
                                <div class="tab_style1">
                                    <div class="tabbar padding_infor_info" style="padding-top: 0px !important; padding-bottom: 0px !important;">
                                        <div class="tab-content" id="nav-tabContent" style="padding-top: 0px !important;">
                                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                <label style="color: black;"><strong>Question: ${withNumber} preview</strong> </label> <span style="color: red;">* Required field</span>
                                                <div class="inner-shadow custom-scrollbar" style="height: 10rem; width: 100%; background-color: white; border: 1px solid gray; overflow-y: scroll;" id="question-preview-${withNumber}">
                                                    ${(!data) ? '<h5>"Question Preview" </h5>': data.question}
                                                </div>

                                                <div class="col-9">
                                                    <textarea name="quiz[${withNumber}][question]" id="question-${withNumber}" hidden value="" style="width: 100%; height: 100%; background-color: white; border: 1px solid gray;" readonly>${(!data) ? '<h5>"Question Preview" </h5>': data.question}</textarea>
                                                </div>
                                                
                                                <div class="row d-flex">
                                                    <div class="col-9">
                                                        <label style="color: black;"><strong>Answer:</strong> </label> <span style="color: red;">* Required field</span>
                                                        <input name="quiz[${withNumber}][answer]" type="text" class="form-control form-control-md rounded-1 outer-shadow" readonly id="answer-${withNumber}" value="${(!data) ? '': data.answer}" >
                                                    </div>
                                                    <div class="col-3">
                                                        <button class="btn btn-primary edit rounded-1 shadow-md mt-4 py-3 btn-block outer-shadow" data-id="${withNumber}">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
            }

            var baseurl = mainurl + 'quizzes/';
            var url = 'questions';

            $('#form').submit(function(e) {
                e.preventDefault();
                const data = new FormData(this);
                $.ajax({
                    url: window.location,
                    method: 'POST',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                }).done(function(data, status, xhr) {
                    console.log(data)
                    swal(data.result.toLowerCase(), data.result, `${data.message}.`);
                    // swal('success', data.result, `${data.message}. Redirecting...`);
                    // window.location = `${baseurl}${data.url}`
                    // $('#modal').modal('toggle');
                    // $('#form')[0].reset();
                    // table.ajax.reload(null, false);
                }).fail(function(data, status, xhr) {
                    console.log(data)
                    const validation = data.responseJSON;
                    $.map(validation.fields, function(value, key) {
                        var input = key;
                        $.map(value, function(value, key) {
                            $('strong[data-target="' + (input) + '"]').text(value);
                            $('input[name="' + (input) + '"]').addClass('border-danger');
                        });
                    });
                });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon: icon,
                    title: result,
                    text: message,
                    timer: 5000
                });
            }
            CKEDITOR.ClassicEditor.create(document.getElementById('CKEditorDiv'), {
                    // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
                    toolbar: {
                        items: [
                            'exportPDF', 'exportWord', '|',
                            'findAndReplace', 'selectAll', '|',
                            'heading', '|',
                            'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                            'bulletedList', 'numberedList', 'todoList', '|',
                            'outdent', 'indent', '|',
                            'undo', 'redo',
                            '-',
                            'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                            'alignment', '|',
                            'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', 'htmlEmbed', '|',
                            'specialCharacters', 'horizontalLine', 'pageBreak', '|',
                            'textPartLanguage', '|',
                            'sourceEditing'
                        ],
                        shouldNotGroupWhenFull: true
                    },
                    // Changing the language of the interface requires loading the language file using the <script> tag.
                    // language: 'es',
                    list: {
                        properties: {
                            styles: true,
                            startIndex: true,
                            reversed: true
                        }
                    },
                    // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
                    heading: {
                        options: [{
                                model: 'paragraph',
                                title: 'Paragraph',
                                class: 'ck-heading_paragraph'
                            },
                            {
                                model: 'heading1',
                                view: 'h1',
                                title: 'Heading 1',
                                class: 'ck-heading_heading1'
                            },
                            {
                                model: 'heading2',
                                view: 'h2',
                                title: 'Heading 2',
                                class: 'ck-heading_heading2'
                            },
                            {
                                model: 'heading3',
                                view: 'h3',
                                title: 'Heading 3',
                                class: 'ck-heading_heading3'
                            },
                            {
                                model: 'heading4',
                                view: 'h4',
                                title: 'Heading 4',
                                class: 'ck-heading_heading4'
                            },
                            {
                                model: 'heading5',
                                view: 'h5',
                                title: 'Heading 5',
                                class: 'ck-heading_heading5'
                            },
                            {
                                model: 'heading6',
                                view: 'h6',
                                title: 'Heading 6',
                                class: 'ck-heading_heading6'
                            }
                        ]
                    },
                    // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
                    placeholder: 'Welcome to CKEditor&nbsp;5!',
                    // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
                    fontFamily: {
                        options: [
                            'default',
                            'Arial, Helvetica, sans-serif',
                            'Courier New, Courier, monospace',
                            'Georgia, serif',
                            'Lucida Sans Unicode, Lucida Grande, sans-serif',
                            'Tahoma, Geneva, sans-serif',
                            'Times New Roman, Times, serif',
                            'Trebuchet MS, Helvetica, sans-serif',
                            'Verdana, Geneva, sans-serif'
                        ],
                        supportAllValues: true
                    },
                    // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
                    fontSize: {
                        options: [10, 12, 14, 'default', 18, 20, 22],
                        supportAllValues: true
                    },
                    // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
                    // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
                    htmlSupport: {
                        allow: [{
                            name: /.*/,
                            attributes: true,
                            classes: true,
                            styles: true
                        }]
                    },
                    // Be careful with enabling previews
                    // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
                    htmlEmbed: {
                        showPreviews: true
                    },
                    // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
                    link: {
                        decorators: {
                            addTargetToExternalLinks: true,
                            defaultProtocol: 'https://',
                            toggleDownloadable: {
                                mode: 'manual',
                                label: 'Downloadable',
                                attributes: {
                                    download: 'file'
                                }
                            }
                        }
                    },
                    // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
                    mention: {
                        feeds: [{
                            marker: '@',
                            feed: [
                                '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                                '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                                '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                                '@sugar', '@sweet', '@topping', '@wafer'
                            ],
                            minimumCharacters: 1
                        }]
                    },
                    // The "super-build" contains more premium features that require additional configuration, disable them below.
                    // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
                    removePlugins: [
                        // These two are commercial, but you can try them out without registering to a trial.
                        // 'ExportPdf',
                        // 'ExportWord',
                        'CKBox',
                        'CKFinder',
                        'EasyImage',
                        // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                        // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                        // Storing images as Base64 is usually a very bad idea.
                        // Replace it on production website with other solutions:
                        // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                        // 'Base64UploadAdapter',
                        'RealTimeCollaborativeComments',
                        'RealTimeCollaborativeTrackChanges',
                        'RealTimeCollaborativeRevisionHistory',
                        'PresenceList',
                        'Comments',
                        'TrackChanges',
                        'TrackChangesData',
                        'RevisionHistory',
                        'Pagination',
                        'WProofreader',
                        // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                        // from a local file system (file://) - load this site via HTTP server if you enable MathType.
                        'MathType',
                        // The following features are part of the Productivity Pack and require additional license.
                        'SlashCommand',
                        'Template',
                        'DocumentOutline',
                        'FormatPainter',
                        'TableOfContents',
                        'PasteFromOfficeEnhanced'
                    ]
                }).then(editor => {
                    getEditor = editor
                    if (getEditor) {
                        getEditor.setData('')
                    } else {
                        console.log('Error setting content')
                    }
                    console.log(editor);
                })
                .catch(error => {
                    console.error(error);
                });

        });
    </script>