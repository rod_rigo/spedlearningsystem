<?php $genders = [0 => 'Male', 1 => 'Female'] ?>

<?= $this->Form->create($subject, ['type' => 'file', 'id' => 'form']) ?>
    <div class="row column2 graph margin_bottom_30">

        <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
            <div class="white_shd full">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>
                            Register New Subject
                            <i class="fa fa-user"></i>
                        </h2>
                    </div>
                </div>
                <div class="full graph_revenue">
                    <div class="row m-3">
                        <div class="full graph_head">
                            <div class="heading1 margin_0">
                                <h2>
                                    Subject
                                    <i class="fa fa-user"></i>
                                </h2>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-12 col-md-12">
                            <strong class="text-dark"><?= $this->Form->label('subject', ucwords('Subject')) ?></strong>
                            <?= $this->Form->control('subject', [
                                'class' => 'form-control rounded-0',
                                'label' => false,
                                'placeholder' => ucwords('Subject'),
                            ]) ?>

                        </div>
                        <div class="bottom_list">

                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                            <?= $this->Form->button('Save', [
                                'class' => 'btn btn-lg btn-success rounded-0',
                                'type' => 'submit',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?= $this->Form->end() ?>

<script>
    $(function () {
        'use strict';

        var url = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url:url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                // console.log(data)
                swal(data.message, data.result, data.result.toLowerCase());
                $('#form')[0].reset();
            }).fail(function (data, status, xhr) {
                // console.log(data)
                const validation = data.responseJSON;

                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });

            });
        });

        $('#show-password').change(function (e) {
            var passwords = $('#password, #confirm-password');
            var type = $(this).prop('checked')? 'text': 'password';
            passwords.attr('type', type);
        });

        $('input').on('input', function () {
            $(this).removeClass('border-danger')
        });

        $('#file').change(function (e) {
            var blob = URL.createObjectURL(e.target.files[0]);
            $('#image').attr('src', blob);
        });

        function swal(message, result, icon) {
            Swal.fire({
                text:message,
                title:result,
                icon:icon,
                timer:5000,
                timerProgressBar:true,
            });
        }

    });
</script>
