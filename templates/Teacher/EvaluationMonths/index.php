<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Domain Types List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Months</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
$months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
]
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'form']); ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row m-3">


                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="row">

                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label for="">Months</label>
                                <select name="month" class="form-control rounded-0" id="month">
                                    <option value="">-- Select Month --</option>
                                    <?php foreach($months as $month): ?>
                                        <option value="<?= $month ?>"><?= $month ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <strong data-target="month"></strong>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center align-items-center">
                    <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal">Close</button>
                    <?= $this->Form->button('Save', [
                        'type' => 'submit',
                        'class' => 'btn btn-success rounded-0'
                    ]) ?>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            var baseurl = mainurl + 'evaluation-months/';
            var url = '';

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy: true,
                processing: true,
                responsive: true,
                serchDelay: 3500,
                deferRender: true,
                pagingType: 'full_numbers',
                order: [
                    [0, 'asc']
                ],
                ajax: {
                    url: baseurl + 'getEvaluationMonths',
                    method: 'GET',
                    dataType: 'JSON'
                },
                columnDefs: [{
                        targets: 0,
                        render: function(data, type, full, meta) {
                            const row = meta.row;
                            return row + 1;
                        }
                    },
                    {
                        targets: 1,
                        data: null,
                        render: function(data, type, row) {
                            return '<a data-id="' + (row.id) + '" class="btn btn-sm btn-info text-white edit">Edit <i class="fa fa-pencil-square" aria-hidden="true"></i></a>';
                        }
                    }
                ],
                columns: [
                    {
                        data: 'month',
                        render: (data, type, row) => {
                            return data;
                        }
                    },
                    {
                        data: 'id'
                    }
                ]
            });

            $('#modal').on('hidden.bs.modal', function(e) {
                $('#form')[0].reset();
                $('input').removeClass('border-danger');
                $('strong[data-target]').empty();
            });

            $('#form').submit(function(e) {
                e.preventDefault();
                const data = new FormData(this);
                $.ajax({
                    url: baseurl + url,
                    method: 'POST',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                }).done(function(data, status, xhr) {
                    console.log(data)
                    swal('success', data.result, data.message);
                    $('#modal').modal('toggle');
                    $('#form')[0].reset();
                    table.ajax.reload(null, false);
                }).fail(function(data, status, xhr) {
                    const validation = data.responseJSON;
                    $.map(validation.fields, function(value, key) {
                        var input = key;
                        $.map(value, function(value, key) {
                            $('strong[data-target="' + (input) + '"]').text(value);
                            $('input[name="' + (input) + '"]').addClass('border-danger');
                        });
                    });
                });
            });

            datatable.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl + 'edit/' + dataId;
                $.ajax({
                    url: href,
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function(e) {
                        url = 'edit/' + dataId
                    },
                }).done(function(data, status, xhr) {
                    $('#modal').modal('toggle');
                    $('#month').val(data.month);
                }).fail(function(xhr, status, error) {
                    const response = JSON.parse(xhr.responseText);
                    swal('info', response.result, response.message);
                });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon: icon,
                    title: result,
                    text: message,
                    timer: 5000
                });
            }

        });
    </script>
