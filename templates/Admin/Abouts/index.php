<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\About[]|\Cake\Collection\CollectionInterface $abouts
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'form']); ?>
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-5 col-lg-5">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                                <?=$this->Html->image('preview.jpg',[
                                    'class' => 'img-thumbnail',
                                    'width' => '200',
                                    'height' => '200',
                                    'id' => 'image',
                                ])?>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <?= $this->Form->control('file', [
                                    'class' => 'form-control rounded-0',
                                    'type' => 'file',
                                    'accept' => 'image/*'
                                ]); ?>
                                <strong data-target="file"></strong>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <div class="row">

                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <strong class="text-dark">
                                    <?=$this->Form->label('title', ucwords('title'))?>
                                </strong>
                                <?=$this->Form->control('title',[
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('title'),
                                ])?>
                                <strong data-target="title"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <strong class="text-dark">
                                    <?=$this->Form->label('description', ucwords('description'))?>
                                </strong>
                                <?=$this->Form->control('description',[
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('description'),
                                    'style' => 'resize:none;'
                                ])?>
                                <strong data-target="description"></strong>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                                <div class="icheck-primary d-inline">
                                    <input type="checkbox" id="re-size">
                                    <label for="re-size">
                                        <strong class="text-dark">
                                            Auto Resize
                                        </strong>
                                    </label>
                                </div>
                                <?=$this->Form->control('resize',[
                                    'type' => 'hidden',
                                    'label' => false,
                                    'value' => 0
                                ])?>
                            </div>

                        </div>
                    </div>

                    <div class="bottom_list">

                    </div>

                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <?=$this->Form->button('Reset',[
                    'type' => 'reset',
                    'class' => 'btn btn-danger rounded-0'
                ])?>
                <button type="button" class="btn btn-info rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button('Save',[
                    'type' => 'submit',
                    'class' => 'btn btn-success rounded-0'
                ])?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        <?= ucwords($controller); ?> Lists
                    </h2>
                </div>
                <button id="open-modal" class="btn btn-info rounded-0 pull-right">New About</button>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Descriptions</th>
                                <th>Created</th>
                                <th>Modified</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'abouts/';
        var image = $('#image').attr('src');
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getAbouts',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 5,
                    data: null,render: function(data,type,row){
                    return  '<a data-id="'+(row.id)+'" class="btn btn-sm btn-info text-white rounded-0 edit">Edit <i class="fa fa-pencil-square" aria-hidden="true"></i></a> | '+
                        '<a data-id="'+(row.id)+'" class="btn btn-sm btn-danger text-white rounded-0 delete">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'title'},
                { data: 'description'},
                { data: 'created'},
                { data: 'modified'},
                { data: 'id'}
            ]
        });

        $('#open-modal').click(function (e) {
            url = 'add';
            $('#resize').val(0);
            $('#form')[0].reset();
            $('button[type="reset"]').fadeIn(100);
            $('#image').attr('src', image);
            $('strong[data-target]').empty();
            $('#modal').modal('toggle');
            $('input').removeClass('border-danger');
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            url = 'add';
            $('#resize').val(0);
            $('#form')[0].reset();
            $('input').removeClass('border-danger');
            $('#image').attr('src', image);
            $('strong[data-target]').empty();
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl+url,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
            }).done(function (data, status, xhr) {
                swal('success', data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload(null, false);
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;
                $.map(validation.errors, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+dataId;
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    $('button[type="reset"]').fadeIn(100);
                    url = 'edit/'+dataId
                },
            }).done(function (data, status, xhr) {
                $('#modal').modal('toggle');
                $('#title').val(data.title);
                $('#description').val(data.description);
                $('#image').attr('src', '/SPEDLearningSystem/img/'+(data.background));
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        table.ajax.reload(null, false);
                        swal('success', data.result, data.message);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        $('#file').change(function (e) {
            var blob = URL.createObjectURL(e.target.files[0]);
            $('#image').attr('src', blob);
        });

        $('#re-size').change(function (e) {
            var prop = $(this).prop('checked');
            $('#resize').val(Number(prop));
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

