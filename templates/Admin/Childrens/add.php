<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Children $children
 * @var \Cake\Collection\CollectionInterface|string[] $students
 * @var \Cake\Collection\CollectionInterface|string[] $parentChildrens
 */
?>

<?= $this->Form->create($children, ['type' => 'file', 'id' => 'form']) ?>
<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Set Guardian
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3 d-flex justify-content-center align-items-center">

                    <div class="col-sm-12 col-lg-8 col-md-8">
                        <strong class="text-dark"><?= $this->Form->label('student', ucwords('student')) ?></strong>
                        <?= $this->Form->control('student', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('student'),
                            'required' => true,
                        ]) ?>
                        <?=$this->Form->control('student_id',[
                            'required' => true,
                            'type' => 'hidden',
                        ])?>
                        <strong data-target="student-id"></strong>
                        <strong data-target="student"></strong>
                    </div>

                    <div class="col-sm-12 col-lg-8 col-md-8">
                        <strong class="text-dark"><?= $this->Form->label('parent', ucwords('parent')) ?></strong>
                        <?= $this->Form->control('parent', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('parent'),
                            'required' => true,
                        ]) ?>
                        <?=$this->Form->control('parent_id',[
                            'required' => true,
                            'type' => 'hidden',
                        ])?>
                        <strong data-target="parent-id"></strong>
                        <strong data-target="parent"></strong>
                    </div>

                    <div class="col-sm-12 col-lg-8 col-md-8">
                        <strong class="text-dark"><?= $this->Form->label('relation', ucwords('relation')) ?></strong>
                        <?= $this->Form->control('relation', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('relation'),
                            'required' => true,
                        ]) ?>
                        <strong data-target="relation"></strong>
                    </div>

                    <div class="bottom_list">

                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                        <?= $this->Form->button('Save', [
                            'class' => 'btn btn-lg btn-success rounded-0',
                            'type' => 'submit',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->Form->end() ?>

<script>
    $(function () {
        'use strict';

        var url = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url:url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                swal(data.message, data.result, 'success');
                $('#form')[0].reset();
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;
                $.map(validation.errors, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });

            });
        });

        $('input').on('input', function () {
            $(this).removeClass('border-danger')
        });

        function students() {
            $.ajax({
                url:mainurl+'students/getStudentsLists',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function () {
                    Swal.fire({
                        icon: null,
                        title: null,
                        text: null,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                $('#student').autocomplete({
                    minLength: 2,
                    source: function (request, response) {
                        response($.map(data, function (value, key) {
                            var name = value.toUpperCase();
                            if (name.indexOf(request.term.toUpperCase()) !== -1) {
                                return {
                                    label: value, // Label for Display
                                    id: key // Value
                                }
                            } else {
                                return null;
                            }
                        }));
                    },
                    focus: function(event, ui) {
                        event.preventDefault();
                        $('#student').val(ui.item.label);
                        $('#student-id').val(ui.item.id);
                    },
                    select: function(event, ui) {
                        event.preventDefault();
                        $('#student').val(ui.item.label);
                        $('#student-id').val(ui.item.id);
                    }
                });
            }).fail(function (jqXhr, textStatus, errorMessage) {
                window.location.reload();
            }).always(function (jqXhr, textStatus, errorMessage) {
                Swal.close();
            });

            $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                var label = item.label.replace(new RegExp('(?![^&;]+;)(?!<[^<>]*)(' + $.ui.autocomplete.escapeRegex(this.term) + ')(?![^<>]*>)(?![^&;]+;)', 'gi'), '<strong style="font-weight: 900">$1</strong>');
                return $('<li></li>').data('item.autocomplete', item).append('<li>'+(label)+'</li>').appendTo(ul);
            };
        }

        function parents() {
            $.ajax({
                url:mainurl+'parents/getParentsLists',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function () {
                    Swal.fire({
                        icon: null,
                        title: null,
                        text: null,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                $('#parent').autocomplete({
                    minLength: 2,
                    source: function (request, response) {
                        response($.map(data, function (value, key) {
                            var name = value.toUpperCase();
                            if (name.indexOf(request.term.toUpperCase()) !== -1) {
                                return {
                                    label: value, // Label for Display
                                    id: key // Value
                                }
                            } else {
                                return null;
                            }
                        }));
                    },
                    focus: function(event, ui) {
                        event.preventDefault();
                        $('#parent').val(ui.item.label);
                        $('#parent-id').val(ui.item.id);
                    },
                    select: function(event, ui) {
                        event.preventDefault();
                        $('#parent').val(ui.item.label);
                        $('#parent-id').val(ui.item.id);
                    }
                });
            }).fail(function (jqXhr, textStatus, errorMessage) {
                window.location.reload();
            }).always(function (jqXhr, textStatus, errorMessage) {
                Swal.close();
            });

            $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                var label = item.label.replace(new RegExp('(?![^&;]+;)(?!<[^<>]*)(' + $.ui.autocomplete.escapeRegex(this.term) + ')(?![^<>]*>)(?![^&;]+;)', 'gi'), '<strong style="font-weight: 900">$1</strong>');
                return $('<li></li>').data('item.autocomplete', item).append('<li>'+(label)+'</li>').appendTo(ul);
            };
        }

        function swal(message, result, icon) {
            Swal.fire({
                text:message,
                title:result,
                icon:icon,
                timer:5000,
                timerProgressBar:true,
            });
        }

        students();
        parents();

    });
</script>

