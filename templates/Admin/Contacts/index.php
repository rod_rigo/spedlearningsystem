<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact[]|\Cake\Collection\CollectionInterface $contacts
 */
?>

<style>
    #map{
        width: 100%;
        height: 50vh;
        border: 2px solid black;
        margin: auto;
    }
</style>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?= $this->Form->create($entity, ['type' => 'file', 'id' => 'form']); ?>
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5 col-lg-5">
                        <div class="row">
                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <strong class="text-dark">
                                    <?=$this->Form->label('email', ucwords('email'))?>
                                </strong>
                                <?=$this->Form->control('email',[
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('email'),
                                ])?>
                                <strong data-target="email"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <strong class="text-dark">
                                    <?=$this->Form->label('contact_number', ucwords('contact number'))?>
                                </strong>
                                <?=$this->Form->control('contact_number',[
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('contact number'),
                                ])?>
                                <strong data-target="contact_number"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-6 col-md-6">
                                <strong class="text-dark">
                                    <?=$this->Form->label('latitude', ucwords('latitude'))?>
                                </strong>
                                <?=$this->Form->control('latitude',[
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('latitude'),
                                    'readonly' => true,
                                ])?>
                                <strong data-target="latitude"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-6 col-md-6">
                                <strong class="text-dark">
                                    <?=$this->Form->label('longtitude', ucwords('longtitude'))?>
                                </strong>
                                <?=$this->Form->control('longtitude',[
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('longtitude'),
                                    'readonly' => true,
                                ])?>
                                <strong data-target="longtitude"></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <?=$this->Form->button('Reset',[
                    'type' => 'reset',
                    'class' => 'btn btn-danger rounded-0'
                ])?>
                <button type="button" class="btn btn-info rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button('Save',[
                    'type' => 'submit',
                    'class' => 'btn btn-success rounded-0'
                ])?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        <?= ucwords($controller); ?> Lists
                    </h2>
                </div>
                <button id="open-modal" class="btn btn-info rounded-0 pull-right">New Contacts</button>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Email</th>
                                <th>Contact #</th>
                                <th>Address</th>
                                <th>Created</th>
                                <th>Modified</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'contacts/';
        var url = '';

        var marker;
        var p = 0;
        var latloc = 0;
        var longloc = 0;
        const cities = L.layerGroup();

        const google_streets = L.tileLayer('http://www.google.cn/maps/vt?lyrs=m&x={x}&y={y}&z={z}', {
            id: null,
            attribution: 'Google Streets',
            maxZoom: 19,
        });

        const google_hybrid = L.tileLayer('http://www.google.cn/maps/vt?lyrs=s,h&x={x}&y={y}&z={z}', {
            id: null,
            attribution: 'Google Hybrid',
            maxZoom: 19,
        });

        const google_satellite = L.tileLayer('http://www.google.cn/maps/vt?lyrs=s&x={x}&y={y}&z={z}', {
            id: null,
            attribution: 'Google Satellite',
            maxZoom: 19,
        });

        const google_terrain = L.tileLayer('http://www.google.cn/maps/vt?lyrs=p&x={x}&y={y}&z={z}', {
            id: null,
            attribution: 'Google Terrain',
            maxZoom: 19,
        });

        var map = new L.Map('map',{
            layers: [google_streets, cities]
        }).locate({
            setView: true,
            maxZoom: 16,
            watch:true
        });

        const layerControl = L.control.layers({
            'Google Streets': google_streets,
            'Google Hybrid': google_hybrid,
            'Google Satellite': google_satellite,
            'Google Terrain': google_terrain,
        }).addTo(map);

        function onLocationFound(e) {
            var latitude = e.latlng.lat;
            var longtitude = e.latlng.lng;
            var circle =  L.marker([latitude, longtitude], {
                draggable:true
            }).addTo(map);
            circle.bindPopup('You Are Here');
            $('#latitude').val(latitude);
            $('#longtitude').val(longtitude);
            latloc = latitude;
            longloc = longtitude;
        }
        map.on('locationfound', onLocationFound);

        var searchControl = new L.Control.Search({
            url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
            jsonpParam: 'json_callback',
            propertyName: 'display_name',
            propertyLoc: ['lat','lon'],
            marker: L.circleMarker([0,0],{
                radius:30
            }),
            autoCollapse: true,
            autoType: false,
            minLength: 2
        });
        map.addControl(searchControl);
        searchControl.on('search:locationfound', function(e) {
            var latitude = e.latlng.lat;
            var longtitude = e.latlng.lng;
            $('#latitude').val(latitude);
            $('#longtitude').val(longtitude);
        });

        map.on('click', function(e) {
            var latitude = e.latlng.lat;
            var longtitude = e.latlng.lng;
            $('#latitude').val(latitude);
            $('#longtitude').val(longtitude);
            if(p>0){
                map.removeLayer(marker);
            }
            marker = L.marker([latitude, longtitude], {
                draggable:true
            }).addTo(map);
            p++;
        });

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getContacts',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 6,
                    data: null,render: function(data,type,row){
                    return  '<a data-id="'+(row.id)+'" class="btn btn-sm btn-info text-white rounded-0 edit">Edit <i class="fa fa-pencil-square" aria-hidden="true"></i></a> | '+
                        '<a data-id="'+(row.id)+'" class="btn btn-sm btn-danger text-white rounded-0 delete">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'email'},
                { data: 'contact_number'},
                { data: 'address'},
                { data: 'created'},
                { data: 'modified'},
                { data: 'id'}
            ]
        });

        $('#open-modal').click(function (e) {
            url = 'add';
            $('#form')[0].reset();
            $('button[type="reset"]').fadeIn(100);
            $('strong[data-target]').empty();
            $('#modal').modal('toggle');
            $('input').removeClass('border-danger');
            p > 0 ? map.removeLayer(marker): true;
            p = 0;
            map.flyTo(new L.LatLng(latloc, longloc));
            $('#latitude').val(latloc);
            $('#longtitude').val(longloc);
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            url = 'add';
            $('#form')[0].reset();
            $('input').removeClass('border-danger');
            $('strong[data-target]').empty();
            p > 0 ? map.removeLayer(marker): true;
            p = 0;
            map.flyTo(new L.LatLng(latloc, longloc));
            $('#latitude').val(latloc);
            $('#longtitude').val(longloc);
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl+url,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
            }).done(function (data, status, xhr) {
                swal('success', data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload(null, false);
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;
                $.map(validation.errors, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'edit/'+dataId;
            $.ajax({
                url:href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend: function (e) {
                    $('button[type="reset"]').fadeIn(100);
                    url = 'edit/'+dataId
                },
            }).done(function (data, status, xhr) {
                $('#modal').modal('toggle');
                $('button[button="reset"]');
                $('#latitude').val(data.latitude);
                $('#longtitude').val(data.longtitude);
                $('#email').val(data.email);
                $('#contact-number').val(data.contact_number);
                map.flyTo(new L.LatLng(data.latitude, data.longtitude));
                p > 0 ? map.removeLayer(marker): true;
                marker =  L.marker([data.latitude, data.longtitude], {
                    draggable:true
                }).addTo(map);
                p = 1;
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        table.ajax.reload(null, false);
                        swal('success', data.result, data.message);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        setInterval(function () {
            map.invalidateSize();
        }, 1000);

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

