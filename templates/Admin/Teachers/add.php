<?php $genders = [ 0 => 'Male', 1 => 'Female' ] ?>

<?= $this->Form->create($user, ['type' => 'file', 'id' => 'form']) ?>
    <div class="row column2 graph margin_bottom_30">

        <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
            <div class="white_shd full">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>
                            Register New Teacher
                            <i class="fa fa-user"></i>
                        </h2>
                    </div>
                </div>
                <div class="full graph_revenue">
                    <div class="row m-3">

                        <div class="col-sm-12 col-md-5 col-lg-5">

                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                                    <?= $this->Html->image('preview.jpg', [
                                        'class' => 'rounded-circle',
                                        'width' => '240',
                                        'height' => '240',
                                        'id' => 'image',
                                    ]) ?>
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <?= $this->Form->control('file', [
                                        'class' => 'form-control rounded-0',
                                        'required' => true,
                                        'type' => 'file',
                                        'accept' => 'image/*'
                                    ]); ?>
                                    <strong data-target="file"></strong>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-12 col-md-7 col-lg-7">
                            <div class="row">

                                <div class="col-sm-12 col-lg-9 col-md-9">
                                    <strong class="text-dark"><?= $this->Form->label('username', ucwords('Username')) ?></strong>
                                    <?= $this->Form->control('username', [
                                        'class' => 'form-control rounded-0',
                                        'label' => false,
                                        'placeholder' => ucwords('username'),
                                    ]) ?>
                                    <strong data-target="username"></strong>
                                </div>

                                <div class="col-sm-12 col-lg-9 col-md-9">
                                    <strong class="text-dark"><?= $this->Form->label('email', ucwords('email')) ?></strong>
                                    <?= $this->Form->control('email', [
                                        'class' => 'form-control rounded-0',
                                        'label' => false,
                                        'placeholder' => ucwords('email'),
                                    ]) ?>
                                    <strong data-target="email"></strong>
                                </div>

                                <div class="col-sm-12 col-lg-9 col-md-9">
                                    <strong class="text-dark"><?= $this->Form->label('contact_number', ucwords('contact number')) ?></strong>
                                    <?= $this->Form->control('contact_number', [
                                        'class' => 'form-control rounded-0',
                                        'label' => false,
                                        'placeholder' => ucwords('contact number'),
                                        'required' => true,
                                    ]) ?>
                                    <strong data-target="contact_number"></strong>
                                </div>

                                <div class="bottom_list">

                                </div>

                                <div class="col-sm-12 col-lg-9 col-md-9">
                                    <strong class="text-dark"><?= $this->Form->label('password', ucwords('password')) ?></strong>
                                    <?= $this->Form->control('password', [
                                        'class' => 'form-control rounded-0',
                                        'label' => false,
                                        'placeholder' => ucwords('password'),
                                    ]) ?>
                                    <strong data-target="password"></strong>
                                </div>

                                <div class="col-sm-12 col-lg-9 col-md-9">
                                    <strong class="text-dark"><?= $this->Form->label('confirm_password', ucwords('confirm password')) ?></strong>
                                    <?= $this->Form->control('confirm_password', [
                                        'class' => 'form-control rounded-0',
                                        'label' => false,
                                        'placeholder' => ucwords('confirm password'),
                                        'type' => 'password',
                                        'required' => true
                                    ]) ?>
                                    <strong data-target="confirm_password"></strong>
                                </div>

                                <div class="col-sm-12 col-lg-9 col-md-9">
                                    <div class="icheck-primary icheck-inline">
                                        <input type="checkbox" id="show-password" />
                                        <label for="show-password">Show Password</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="full graph_head">
                            <div class="heading1 margin_0">
                                <h2>
                                    Teacher's Information
                                    <i class="fa fa-user"></i>
                                </h2>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4 col-md-4">
                            <strong class="text-dark"><?= $this->Form->label('teacher.last_name', ucwords('Last Name')) ?></strong>
                            <?= $this->Form->control('teacher.last_name', [
                                'class' => 'form-control rounded-0',
                                'label' => false,
                                'placeholder' => ucwords('Last Name'),
                            ]) ?>
                            
                        </div>
                        <div class="col-sm-4 col-lg-4 col-md-4">
                            <strong class="text-dark"><?= $this->Form->label('teacher.first_name', ucwords('Firstname')) ?></strong>
                            <?= $this->Form->control('teacher.first_name', [
                                'class' => 'form-control rounded-0',
                                'label' => false,
                                'placeholder' => ucwords('First name'),
                            ]) ?>
                            <strong data-target="teacher.first_name"></strong>
                        </div>
                        <div class="col-sm-4 col-lg-4 col-md-4">
                            <strong class="text-dark"><?= $this->Form->label('teacher.middle_name', ucwords('Middle Name')) ?></strong>
                            <?= $this->Form->control('teacher.middle_name', [
                                'class' => 'form-control rounded-0',
                                'label' => false,
                                'placeholder' => ucwords('Middle name'),
                            ]) ?>
                        </div>

                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong class="text-dark"><?= $this->Form->label('teacher.gender', ucwords('Gender')) ?></strong>
                            <?= $this->Form->control('teacher.gender', [
                                'class' => 'form-control rounded-0',
                                'label' => false,
                                'options' => $genders,
                                'placeholder' => ucwords('Gender'),
                            ]) ?>
                        </div>
                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong class="text-dark"><?= $this->Form->label('teacher.birthdate', ucwords('Birthdate')) ?></strong>
                            <?= $this->Form->control('teacher.birthdate', [
                                'type' => 'date',
                                'class' => 'form-control rounded-0',
                                'label' => false,
                                'placeholder' => ucwords('Birthdate'),
                            ]) ?>
                        </div>
                    <!-- echo $this->Form->control('user_id', ['options' => $users]);
                    echo $this->Form->control('first_name');
                    echo $this->Form->control('middle_name');
                    echo $this->Form->control('last_name');
                    echo $this->Form->control('gender');
                    echo $this->Form->control('birthdate');
                    echo $this->Form->control('deleted', ['empty' => true]); -->
                        <div class="bottom_list">

                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                            <?= $this->Form->button('Save', [
                                'class' => 'btn btn-lg btn-success rounded-0',
                                'type' => 'submit',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?= $this->Form->end() ?>

<script>
    $(function () {
        'use strict';

        var url = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url:url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                swal(data.message, data.result, 'success');
                $('#form')[0].reset();
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;

                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });

            });
        });

        $('#show-password').change(function (e) {
            var passwords = $('#password, #confirm-password');
            var type = $(this).prop('checked')? 'text': 'password';
            passwords.attr('type', type);
        });

        $('input').on('input', function () {
            $(this).removeClass('border-danger')
        });

        $('#file').change(function (e) {
            var blob = URL.createObjectURL(e.target.files[0]);
            $('#image').attr('src', blob);
        });

        function swal(message, result, icon) {
            Swal.fire({
                text:message,
                title:result,
                icon:icon,
                timer:5000,
                timerProgressBar:true,
            });
        }

    });
</script>
