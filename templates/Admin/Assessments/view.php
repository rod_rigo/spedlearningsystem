<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assessment $assessment
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Assessment'), ['action' => 'edit', $assessment->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Assessment'), ['action' => 'delete', $assessment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assessment->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Assessments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Assessment'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="assessments view content">
            <h3><?= h($assessment->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Quiz') ?></th>
                    <td><?= $assessment->has('quiz') ? $this->Html->link($assessment->quiz->id, ['controller' => 'Quizzes', 'action' => 'view', $assessment->quiz->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Subject') ?></th>
                    <td><?= $assessment->has('subject') ? $this->Html->link($assessment->subject->id, ['controller' => 'Subjects', 'action' => 'view', $assessment->subject->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Student') ?></th>
                    <td><?= $assessment->has('student') ? $this->Html->link($assessment->student->id, ['controller' => 'Students', 'action' => 'view', $assessment->student->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($assessment->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Points') ?></th>
                    <td><?= $this->Number->format($assessment->points) ?></td>
                </tr>
                <tr>
                    <th><?= __('Total') ?></th>
                    <td><?= $this->Number->format($assessment->total) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($assessment->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($assessment->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Deleted') ?></th>
                    <td><?= h($assessment->deleted) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Answers') ?></h4>
                <?php if (!empty($assessment->answers)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Assessment Id') ?></th>
                            <th><?= __('Question Id') ?></th>
                            <th><?= __('Answer') ?></th>
                            <th><?= __('Points') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Deleted') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($assessment->answers as $answers) : ?>
                        <tr>
                            <td><?= h($answers->id) ?></td>
                            <td><?= h($answers->assessment_id) ?></td>
                            <td><?= h($answers->question_id) ?></td>
                            <td><?= h($answers->answer) ?></td>
                            <td><?= h($answers->points) ?></td>
                            <td><?= h($answers->created) ?></td>
                            <td><?= h($answers->modified) ?></td>
                            <td><?= h($answers->deleted) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Answers', 'action' => 'view', $answers->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Answers', 'action' => 'edit', $answers->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Answers', 'action' => 'delete', $answers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $answers->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
