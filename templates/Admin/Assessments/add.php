<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assessment $assessment
 * @var \Cake\Collection\CollectionInterface|string[] $quizzes
 * @var \Cake\Collection\CollectionInterface|string[] $subjects
 * @var \Cake\Collection\CollectionInterface|string[] $students
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Assessments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="assessments form content">
            <?= $this->Form->create($assessment) ?>
            <fieldset>
                <legend><?= __('Add Assessment') ?></legend>
                <?php
                    echo $this->Form->control('quiz_id', ['options' => $quizzes]);
                    echo $this->Form->control('subject_id', ['options' => $subjects]);
                    echo $this->Form->control('student_id', ['options' => $students]);
                    echo $this->Form->control('points');
                    echo $this->Form->control('total');
                    echo $this->Form->control('deleted', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
