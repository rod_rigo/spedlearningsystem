<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assessment[]|\Cake\Collection\CollectionInterface $assessments
 */
?>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Assessments List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Student</th>
                                <th>Subject</th>
                                <th>Score</th>
                                <th>Mark</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {

        var baseurl = mainurl+'assessments/';
        var image = $('#image').attr('src');
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getAssessments',
                method: 'GET',
                dataType: 'JSON'
            },
            columns: [
                {
                    data: 'id',
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    // Student
                    data: null,
                    render: (data,type,row) => {
                    return `${row.student.last_name}, ${row.student.first_name}, ${row.student.middle_name}`;
    }
    },
        {
            // Subject
            data: null,
                render: (data,type,row) => {
            return row.subject.subject;

        }
        },
        {
            // Score
            data: null,
                render: (data, type, row) => {
            return `${row.points}/${row.total}`;
        }
        },
        {
            // Score
            data: null,
                render: (data, type, row) => {
            return (row.is_checked) ? 'Checked':'Unchecked';
        }
        },
        ]
    });


        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
