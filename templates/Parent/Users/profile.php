<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php $genders = [0 => 'Male', 1 => 'Female'] ?>
<script>
    const id = '<?=base64_encode($user->id)?>';
</script>

<?= $this->Form->create($user, ['type' => 'file', 'id' => 'form']) ?>
<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Student Account
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">

                    <div class="col-sm-12 col-md-5 col-lg-5">

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                                <?= $this->Html->image($user->user_image, [
                                    'class' => 'rounded-circle',
                                    'width' => '240',
                                    'height' => '240',
                                    'id' => 'image',
                                ]) ?>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <?= $this->Form->control('file', [
                                    'class' => 'form-control rounded-0',
                                    'required' => false,
                                    'type' => 'file',
                                    'accept' => 'image/*'
                                ]); ?>
                                <strong data-target="file"></strong>
                            </div>


                        </div>

                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <div class="row">

                            <div class="col-sm-12 col-lg-9 col-md-9">
                                <strong class="text-dark"><?= $this->Form->label('username', ucwords('Username')) ?></strong>
                                <?= $this->Form->control('username', [
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('username'),
                                ]) ?>
                                <strong data-target="username"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-9 col-md-9">
                                <strong class="text-dark"><?= $this->Form->label('email', ucwords('email')) ?></strong>
                                <?= $this->Form->control('email', [
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('email'),
                                ]) ?>
                                <strong data-target="email"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-9 col-md-9">
                                <strong class="text-dark"><?= $this->Form->label('contact_number', ucwords('contact number')) ?></strong>
                                <?= $this->Form->control('contact_number', [
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('contact number'),
                                    'required' => true,
                                ]) ?>
                                <strong data-target="contact_number"></strong>
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('student.last_name', ucwords('Last Name')) ?></strong>
                        <?= $this->Form->control('student.last_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Last Name'),
                        ]) ?>

                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('student.first_name', ucwords('Firstname')) ?></strong>
                        <?= $this->Form->control('student.first_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('First name'),
                        ]) ?>
                        <strong data-target="student.first_name"></strong>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('student.middle_name', ucwords('Middle Name')) ?></strong>
                        <?= $this->Form->control('student.middle_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Middle name'),
                        ]) ?>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('student.gender', ucwords('Gender')) ?></strong>
                        <?= $this->Form->control('student.gender', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'options' => $genders,
                            'placeholder' => ucwords('Gender'),
                        ]) ?>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('student.birthdate', ucwords('Birthdate')) ?></strong>
                        <?= $this->Form->control('student.birthdate', [
                            'type' => 'date',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Birthdate'),
                        ]) ?>
                    </div>

                    <div class="bottom_list">

                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                        <?= $this->Form->button('Submit', [
                            'class' => 'btn btn-lg btn-success rounded-0',
                            'type' => 'submit',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Parent Account
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('parent.last_name', ucwords('Last Name')) ?></strong>
                        <?= $this->Form->control('parent.last_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Last Name'),
                        ]) ?>

                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('parent.first_name', ucwords('Firstname')) ?></strong>
                        <?= $this->Form->control('parent.first_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('First name'),
                        ]) ?>
                        <strong data-target="parent.first_name"></strong>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <strong class="text-dark"><?= $this->Form->label('parent.middle_name', ucwords('Middle Name')) ?></strong>
                        <?= $this->Form->control('parent.middle_name', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Middle name'),
                        ]) ?>
                    </div>

                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('parent.gender', ucwords('Gender')) ?></strong>
                        <?= $this->Form->control('parent.gender', [
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'options' => $genders,
                            'placeholder' => ucwords('Gender'),
                        ]) ?>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <strong class="text-dark"><?= $this->Form->label('parent.birthdate', ucwords('Birthdate')) ?></strong>
                        <?= $this->Form->control('parent.birthdate', [
                            'type' => 'date',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('Birthdate'),
                        ]) ?>
                    </div>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <strong class="text-dark"><?= $this->Form->label('parent.address', ucwords('Address')) ?></strong>
                        <?= $this->Form->control('parent.address', [
                            'type' => 'textarea',
                            'class' => 'form-control rounded-0',
                            'label' => false,
                            'placeholder' => ucwords('address'),
                        ]) ?>
                    </div>

                    <div class="bottom_list">

                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                        <?= $this->Form->button('Submit', [
                            'class' => 'btn btn-lg btn-success rounded-0',
                            'type' => 'submit',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->Form->end() ?>

<?= $this->Form->create($user, ['type' => 'file', 'id' => 'password-form']) ?>
<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Change Password
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3 d-flex justify-content-center align-items-center">

                    <div class="col-sm-12 col-md-7 col-lg-7">

                        <div class="row">

                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <strong class="text-dark"><?= $this->Form->label('current_password', ucwords('current password')) ?></strong>
                                <?= $this->Form->control('current_password', [
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('current password'),
                                    'required' => true,
                                    'type' => 'password',
                                ]) ?>
                                <strong data-target="current_password"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <strong class="text-dark"><?= $this->Form->label('password', ucwords('new password')) ?></strong>
                                <?= $this->Form->control('password', [
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('new password'),
                                    'required' => true,
                                    'value' => ''
                                ]) ?>
                                <strong data-target="password"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <strong class="text-dark"><?= $this->Form->label('confirm_password', ucwords('confirm password')) ?></strong>
                                <?= $this->Form->control('confirm_password', [
                                    'class' => 'form-control rounded-0',
                                    'label' => false,
                                    'placeholder' => ucwords('confirm password'),
                                    'type' => 'password',
                                    'required' => true
                                ]) ?>
                                <strong data-target="confirm_password"></strong>
                            </div>

                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="icheck-primary icheck-inline">
                                    <input type="checkbox" id="show-password" />
                                    <label for="show-password">Show Password</label>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                        <?= $this->Form->button('Submit', [
                            'class' => 'btn btn-lg btn-success rounded-0',
                            'type' => 'submit',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->Form->end() ?>


<script>
    $(function () {
        'use strict';

        var url = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url:url,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                swal(data.message, data.result, 'success');
                window.location.reload();
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;

                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });

            });
        });

        $('#password-form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url:window.location.origin+'/SPEDLearningSystem/admin/users/changepassword/'+id,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                swal(data.message, data.result, 'success');
                window.location.reload();
            }).fail(function (data, status, xhr) {
                const validation = data.responseJSON;

                $.map(validation.fields, function (value, key) {
                    var input = key;
                    $.map(value, function (value, key) {
                        $('strong[data-target="'+(input)+'"]').text(value);
                        $('input[name="'+(input)+'"]').addClass('border-danger');
                    });
                });

            });
        });

        $('#show-password').change(function (e) {
            var passwords = $('#password, #confirm-password, #current-password');
            var type = $(this).prop('checked')? 'text': 'password';
            passwords.attr('type', type);
        });

        $('input').on('input', function () {
            $(this).removeClass('border-danger')
        });

        $('#file').change(function (e) {
            var blob = URL.createObjectURL(e.target.files[0]);
            $('#image').attr('src', blob);
        });

        function swal(message, result, icon) {
            Swal.fire({
                text:message,
                title:result,
                icon:icon,
                timer:5000,
                timerProgressBar:true,
            });
        }

    });
</script>