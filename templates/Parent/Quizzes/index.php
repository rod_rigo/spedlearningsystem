<?php
    define('CATEGORY', [
        'LETTERS ABC' => 'LETTERS ABC',
        'COLORS' => 'COLORS',
        'SHAPES' => 'SHAPES',
        'NUMBERS 123' => 'NUMBERS 123',
        'BODY PARTS' => 'BODY PARTS'
    ]);
?>

<div class="row column2 graph margin_bottom_30">

    <div class="col-sm-12 col-md-l2 col-lg-12 margin_bottom_30">
        <div class="white_shd full">
            <div class="full graph_head">
                <div class="heading1 margin_0">
                    <h2>
                        Quizzes List
                        <i class="fa fa-user"></i>
                    </h2>
                </div>
            </div>
            <div class="full graph_revenue">
                <div class="row m-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Teacher</th>
                                <th>Subject</th>
                                <th>Quiz</th>
                                <th>Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Category</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {

        var baseurl = mainurl+'quizzes/';
        var image = $('#image').attr('src');
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[0, 'asc']],
            ajax:{
                url:baseurl+'getQuizzes',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 1,
                    render: function ( data, type, row, meta ) {
                        return  row.teacher.first_name+' '+row.teacher.middle_name+' '+row.teacher.last_name;
                    }
                },
            ],
            columns: [
                { data: 'id'},
                { data: 'teacher_id'},
                { data: 'subject.subject'},
                { data: 'quiz'},
                { data: 'date'},
                { data: 'start_time'},
                { data: 'end_time'},
                { data: 'category'},

            ]
        });

    });
</script>
