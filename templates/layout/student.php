<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken')); ?>

    <?=$this->Html->css([
        '/product-admin/css/fontawesome.min',
        '/product-admin/css/bootstrap.min',
        '/product-admin/css/templatemo-style',
        '/jquery-ui/css/jquery-ui',
        '/jquery-ui/css/jquery-ui.min',
        '/icheck/css/icheck-bootstrap',
        '/icheck/css/icheck-bootstrap.min',
        '/datatables/css/dataTables.bootstrap4.min',
        '/datatables/css/responsive.bootstrap4.min',
        '/datatables/css/buttons/buttons.dataTables.min',
        '/datatables/css/buttons/buttons.bootstrap4.min',
        '/datatables/css/datetime/dataTables.dateTime.min',
        '/animate-css/css/animate',
        '/animate-css/css/animate.min',
        '/video-js/css/video-js',
        '/video-js/css/video-js.min',
        '/ani-js/css/anicollection',
        '/ani-js/css/anicollection.min'
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.7.0',
        '/jquery/js/jquery-3.7.0.min',
        '/chartjs/js/chart',
        '/chartjs/js/chart.min',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/jquery-ui/js/jquery-ui',
        '/jquery-ui/js/jquery-ui.min',
        '/moment/js/moment',
        '/moment/js/moment.min',
        '/sweet-alert/js/sweetalert2',
        '/sweet-alert/js/sweetalert2.min',
        '/sweet-alert/js/sweetalert2.all',
        '/sweet-alert/js/sweetalert2.all.min',
        '/datatables/js/jquery.dataTables.min',
        '/datatables/js/dataTables.bootstrap4.min',
        '/datatables/js/dataTables.responsive.min',
        '/datatables/js/responsive.bootstrap4.min',
        '/datatables/js/buttons/dataTables.buttons.min',
        '/datatables/js/buttons/buttons.colVis.min',
        '/datatables/js/buttons/buttons.print.min',
        '/datatables/js/buttons/html2pdf.bundle.min',
        '/datatables/js/buttons/jszip.min',
        '/datatables/js/buttons/pdfmake.min',
        '/datatables/js/buttons/vfs_fonts',
        '/datatables/js/buttons/buttons.html5.min',
        '/datatables/js/buttons/buttons.bootstrap4.min',
        '/datatables/js/typehead/bootstrap3-typeahead',
        '/datatables/js/typehead/bootstrap3-typeahead.min',
        '/datatables/js/datetime/dataTables.dateTime.min',
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        const mainurl = window.location.origin+'/KindyQuestLMS/student/';

        var speech = new SpeechSynthesisUtterance();
        var voices = [];
        speech.lang = 'en'; // Set Speech Language
        speech.rate = 1.0; // From 0.1 to 10
        speech.volume = 1; // From 0 to 1
        speech.pitch = 2; // From 0 to 2
        window.speechSynthesis.onvoiceschanged = function () {
            // Get List of Voices
            voices = window.speechSynthesis.getVoices();

            // Initially set the First Voice in the Array.
            speech.voice = voices[0];

            // Set the Voice Select List. (Set the Index as the value, which we'll use later when the user updates the Voice using the Select Menu.)
//            $.map(voices, function (value, key) {
//                $('#voices').append('<option value="'+(key)+'">'+(value.name)+'</option>')
//            });

        };
    </script>
</head>
<body id="reportsPage">

<div class="" id="home">
    <?=$this->element('student/navbar')?>
    <div class="container mt-5 mb-5" style="height: 100vh !important;">

        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
        <!-- row -->

    </div>
    <?=$this->element('student/footer')?>
</div>

<?=$this->Html->script([
    '/product-admin/js/bootstrap.min',
    '/product-admin/js/tooplate-scripts',
    '/video-js/js/video',
    '/video-js/js/video.min',
    '/ani-js/js/anijs',
    '/ani-js/js/anijs-min',
    '/ani-js/js/anijs-helper-dom',
    '/ani-js/js/anijs-helper-dom-min',
    '/ani-js/js/anijs-helper-scrollreveal',
    '/ani-js/js/anijs-helper-scrollreveal-min',
])?>

<script>
    $('a').hover( function () {
        window.speechSynthesis.cancel();
        var text = $(this).text();
        speech.text = text;
        // Start Speaking
        window.speechSynthesis.speak(speech);
    });
</script>
</body>
</html>
