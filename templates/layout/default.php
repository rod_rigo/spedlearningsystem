<?php
/**
* CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
* @link          https://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       https://opensource.org/licenses/mit-license.php MIT License
* @var \App\View\AppView $this
*/

$cakeDescription = 'Admin:';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset('utf-8') ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <?= $this->Html->meta('icon') ?>

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Inter:wght@600&family=Lobster+Two:wght@700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <?= $this->Html->css([
        'lib/animate/animate.min.css', 
        'lib/owlcarousel/assets/owl.carousel.min.css', 
        'css/bootstrap.min.css',
        'css/style.css',
        '/leaflet/css/leaflet',
        '/leaflet/css/leaflet.min',
        '/leaflet/css/leaflet-search',
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.7.0',
        '/leaflet/js/leaflet',
        '/leaflet/js/leaflet.min',
        '/leaflet/js/leaflet-search',
    ])?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <?= $this->element('website/spinner') ?>
        <!-- Spinner End -->


        <!-- Navbar Start -->
        <?= $this->element('website/navbar') ?>
        <!-- Navbar End -->

        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>

        <!-- Footer Start -->
        <?= $this->element('website/footer') ?>
        <!-- Footer End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top">
            <i class="bi bi-arrow-up"></i>
        </a>

    </div>
    <?= $this->Html->script([
        'lib/wow/wow.min.js',
        'lib/easing/easing.min.js',
        'lib/waypoints/waypoints.min.js',
        'lib/owlcarousel/owl.carousel.min.js',
        'js/main.js'
    ]) ?>
    <?=$this->Html->script('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js')?>

</body>
</html>
