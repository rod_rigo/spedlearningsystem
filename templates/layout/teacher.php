<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken')); ?>

    <?=$this->Html->css([
        '/admin/css/bootstrap.min',
        '/admin/style',
        '/admin/css/responsive',
        '/admin/css/bootstrap-select',
        '/admin/css/perfect-scrollbar',
        '/admin/css/custom',
        '/jquery-ui/css/jquery-ui',
        '/jquery-ui/css/jquery-ui.min',
        '/icheck/css/icheck-bootstrap',
        '/icheck/css/icheck-bootstrap.min',
        '/datatables/css/dataTables.bootstrap4.min',
        '/datatables/css/responsive.bootstrap4.min',
        '/datatables/css/buttons/buttons.dataTables.min',
        '/datatables/css/buttons/buttons.bootstrap4.min',
        '/datatables/css/datetime/dataTables.dateTime.min',
        '/leaflet/css/leaflet',
        '/leaflet/css/leaflet.min',
        '/leaflet/css/leaflet-search',
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.7.0',
        '/jquery/js/jquery-3.7.0.min',
        '/chartjs/js/chart',
        '/chartjs/js/chart.min',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/jquery-ui/js/jquery-ui',
        '/jquery-ui/js/jquery-ui.min',
        '/moment/js/moment',
        '/moment/js/moment.min',
        '/sweet-alert/js/sweetalert2',
        '/sweet-alert/js/sweetalert2.min',
        '/sweet-alert/js/sweetalert2.all',
        '/sweet-alert/js/sweetalert2.all.min',
        '/datatables/js/jquery.dataTables.min',
        '/datatables/js/dataTables.bootstrap4.min',
        '/datatables/js/dataTables.responsive.min',
        '/datatables/js/responsive.bootstrap4.min',
        '/datatables/js/buttons/dataTables.buttons.min',
        '/datatables/js/buttons/buttons.colVis.min',
        '/datatables/js/buttons/buttons.print.min',
        '/datatables/js/buttons/html2pdf.bundle.min',
        '/datatables/js/buttons/jszip.min',
        '/datatables/js/buttons/pdfmake.min',
        '/datatables/js/buttons/vfs_fonts',
        '/datatables/js/buttons/buttons.html5.min',
        '/datatables/js/buttons/buttons.bootstrap4.min',
        '/datatables/js/typehead/bootstrap3-typeahead',
        '/datatables/js/typehead/bootstrap3-typeahead.min',
        '/datatables/js/datetime/dataTables.dateTime.min',
        '/leaflet/js/leaflet',
        '/leaflet/js/leaflet.min',
        '/leaflet/js/leaflet-search',
    ])?>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/39.0.2/super-build/ckeditor.js"></script>
    <script>
        const mainurl = window.location.origin+'/KindyQuestLMS/teacher/';
    </script>
</head>
<body class="dashboard dashboard_1">
<div class="full_container">
    <div class="inner_container">
        <!-- Sidebar  -->
        <?=$this->element('teacher/sidebar')?>
        <!-- end sidebar -->
        <!-- right content -->
        <div id="content">
            <!-- topbar -->
            <?=$this->element('teacher/topbar')?>
            <!-- end topbar -->
            <!-- dashboard inner -->
            <div class="midde_cont">
                <div class="container-fluid">
                    <div class="row column_title">
                        <div class="col-md-12">
                            <div class="page_title">
                                <h2><?= $this->fetch('title') ?></h2>
                            </div>
                        </div>
                    </div>
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
                <!-- footer -->
                <div class="container-fluid">
                    <div class="footer">
                        <p>Copyright © 2018 Designed by html.design. All rights reserved.<br><br>
                            Distributed By: <a href="https://themewagon.com/">ThemeWagon</a>
                        </p>
                    </div>
                </div>
            </div>
            <!-- end dashboard inner -->
        </div>
    </div>
</div>



<?=$this->Html->script([
    '/admin/js/popper.min',
    '/admin/js/bootstrap.min',
    '/admin/js/animate',
    '/admin/js/bootstrap-select',
    '/admin/js/owl.carousel',
    '/admin/js/perfect-scrollbar.min',
    '/admin/js/custom',
])?>

</body>
</html>
