<?php
/**
 * @var \App\View\AppView $this
 */

$cakeDescription = 'SPED';
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?= $this->Html->charset() ?>
        <meta name = 'viewport' content = 'width=device-width, initial-scale=1'>
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>
        <?=
            $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken'));
        ?>
        

        <?=$this->Html->script([
            '/jquery/js/jquery-3.7.0',
            '/jquery/js/jquery-3.7.0.min',
            '/sweet-alert/js/sweetalert2',
            '/sweet-alert/js/sweetalert2.min',
            '/sweet-alert/js/sweetalert2.all',
            '/sweet-alert/js/sweetalert2.all.min',
        ]); ?>
        
        <?= $this->Html->css([
            '/plutoAssets/css/bootstrap.min',
            '/plutoAssets/style',
            '/plutoAssets/css/responsive',
            '/plutoAssets/css/colors',
            '/plutoAssets/css/bootstrap-select',
            '/plutoAssets/css/perfect-scrollbar',
            '/plutoAssets/css/custom',
            '/plutoAssets/css/semantic.min',
        ]) ?>

        <script>
            const BASE_URL = window.location.origin + '/KindyQuestLMS/';
        </script>
    </head>

    <body class="inner_page login">
        <?= $this->fetch('content') ?>
        <?= $this->Html->script([
            '/plutoAssets/js/popper.min',
            '/plutoAssets/js/bootstrap.min',
            '/plutoAssets/js/animate',
            '/plutoAssets/js/bootstrap-select',
            '/plutoAssets/js/perfect-scrollbar.min',
            '/plutoAssets/js/custom',
        ]) ?>
        <script>
            var ps = new PerfectScrollbar('#sidebar');
        </script>
    </body>

</html>
