<?php declare(strict_types=1);

namespace App\Controller;

use Cake\Validation\Validator;
use Cake\Mailer\Email;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class AuthController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Users = TableRegistry::getTableLocator()->get('Users');

    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        $this->viewBuilder()->setLayout('auth');
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login','forgotpassword', 'resetpassword']);
    }

    protected function checkRoles($passedRole): ?bool{
        $authRole = $this->Authentication->getIdentity('data')[$passedRole];
        $configRole = Configure::read('Roles')[$passedRole]['value'];
        return ($passedRole == 'is_parent') ? true:$authRole == $configRole;
    }

    public function login()
    {
        if ($this->request->is(['post'])) {
            // dd($this->getRequest()->getData('role') == 'is_parent');
            $roleData = $this->request->getData('role');
            if($roleData == 'is_parent'){
               $parent = $this->getTableLocator()->get('Parents')
                   ->find('all')
                   ->where([
                       'parents.login_id like' => '%'.($this->request->getData('username')).'%'
                   ])
                   ->first();

               $hash = @$parent->password;
               $userId = @$parent->user_id;

               if(password_verify($this->request->getData('password'), $hash)){
                   $user = $this->getTableLocator()->get('Users')->get(intval($userId),[
                       'contain' => [
                           'Parents' => [
                               'queryBuilder' => function($query){
                                   return $query->find('all',['withDeleted']);
                               }
                           ],
                           'Students' => [
                               'queryBuilder' => function($query){
                                   return $query->find('all',['withDeleted']);
                               }
                           ]
                       ]
                   ]);
                   $this->Authentication->setIdentity($user);
                   $this->Flash->success(__('Welcome Parent'));
                   return $this->redirect(Configure::read('Roles')[$roleData]['redirect']);
               }else{
                   $this->Flash->error(__('Invalid username or password'));
               }

            }else{
                $result = $this->Authentication->getResult();
            }

            // $result = $this->Authentication->getResult();
            if ($result && $result->isValid()) {
                // dd($result->getData()->user_id);
                // dd($this->Authentication->getIdentity('data'));

                if($roleData == 'is_parent'){
                    $user = $this->Users->find()
                        ->contain([
                            'Parents' => [
                                'queryBuilder' => function($query){
                                    return $query->find('all',['withDeleted']);
                                }
                            ],
                            'Students' => [
                                'queryBuilder' => function($query){
                                    return $query->find('all',['withDeleted']);
                                }
                            ]
                        ])->where(['users.id = ' => $result->getData()->user_id])->first();
                    $user->username = $result->getData()->login_id;
                    $this->Authentication->setIdentity($user);
                }
                if(!$this->checkRoles($roleData)){
                    $this->Flash->error(__('Username And Password Not Found'));
                    $this->logout();
                }else{
                    if($roleData == 'is_teacher'){
                        $this->Flash->success(__('Welcome Teacher'));
                    }
                    // $user = $this->Users->find()->where(['id = ' => 2])->first();
                    // dd($user);
                    // $this->Authentication->setIdentity($user);
                    return $this->redirect(Configure::read('Roles')[$roleData]['redirect']);
                }
            }
            // display error if user submitted and authentication failed
            if ($this->request->is('post') && !$result->isValid()) {
                // dd($result);
                $this->Flash->error(__('Invalid username or password'));
            }
        }
    }

    public function logout()
    {
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result && $result->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Auth', 'action' => 'login']);
        }
    }

    public function forgotpassword(){
        $entity = $this->getTableLocator()->get('Users')->newEmptyEntity();
        if($this->request->is('post')){

            $validator  = new Validator();

            $validator
                ->requirePresence('email',true)
                ->notEmptyString('email',ucwords('please enter your email'),false);

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value){
                $result  = ['message' => reset($value), 'result' => 'error'];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            $query = $this->getTableLocator()->get('Users')->find()
                        ->where([
                            'users.email like' => '%'.($this->request->getData('email')).'%'
                        ]);

            if($query->count() > 0){

                $user = $this->getTableLocator()->get('Users')->get($query->firstOrFail()->id);
                $user->token = rand(111111111111, 999999999999);

                try{
                    $mailer = new Mailer('default');
                    $mailer->setTransport('default');
                    //        Sender
                    $mailer->setFrom(['rz352235@gmail.com' => 'noreply-email'])
                        //        Sender
                        //                Receiver
                        ->setTo($user->email)
                        //                Receiver
                        ->setEmailFormat('html')
                        ->setSubject('Password')
                        ->viewBuilder()
                        ->setTemplate('default')
                        ->setLayout('default');
                    $url = $this->request->host().Router::url(['prefix'=>false,'controller' => 'Auth', 'action' => 'resetpassword', $user->token]);
                    $mailer->setViewVars(['url' => $url]);
                    $mailer->deliver();
                }catch (\Exception $exception){

                    $result = ['message' => ucwords('your requests has been failed!, please try again.'), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));

                }finally{

                    if($this->getTableLocator()->get('Users')->save($user)){
                        $result = ['message' => ucwords('your request has been sent!, please check your email.'), 'result' => ucwords('success')];
                        return $this->response->withStatus(200)->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }else{
                        $result = ['message' => ucwords('your requests has been failed!, please try again.'), 'result' => ucwords('error')];
                        return $this->response->withStatus(422)->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }

                }

            }else{
                $result  = ['message' => ucwords('the email requested is not registered!'), 'result' => ucwords('error')];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

        }
        $this->set(compact('entity'));
    }

    public function resetpassword($token = null)
    {
        $id = $this->getTableLocator()->get('Users')
            ->find()
            ->where([
                'users.token =' => $token
            ])
            ->firstOrFail();
        $user = $this->getTableLocator()->get('Users')->get($id->id, [
            'contain' => [],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->getTableLocator()->get('Users')->patchEntity($user, $this->request->getData());

            $validator = new Validator();

            $validator
                ->requirePresence('password', true)
                ->notEmptyString('password',ucwords('please enter your password'),false);

            $validator
                ->requirePresence('confirm_password', true)
                ->notEmptyString('confirm_password',ucwords('please confirm your password'),false)
                ->sameAs('password','confirm_password',ucwords('password do not match!'));

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value) {
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('Error'), 'fields' => $errors];
                return $this->response
                    ->withStatus(422)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
            $user->token = rand(11111111, 99999999);

            if ($this->getTableLocator()->get('Users')->save($user)) {
                $result = ['message' => ucwords('The user has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                foreach ($user->getErrors() as $key => $value) {
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('Error'), 'fields' => $user->getErrors()];
                    return $this->response
                        ->withStatus(422)
                        ->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $this->set(compact('user'));
    }



}
