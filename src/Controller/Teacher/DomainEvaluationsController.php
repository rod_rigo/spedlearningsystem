<?php
declare(strict_types=1);

namespace App\Controller\Teacher;

use App\Controller\AppController;

/**
 * DomainEvaluations Controller
 *
 * @property \App\Model\Table\DomainEvaluationsTable $DomainEvaluations
 * @method \App\Model\Entity\DomainEvaluation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DomainEvaluationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students', 'Evaluations', 'Domains'],
        ];
        $domainEvaluations = $this->paginate($this->DomainEvaluations);

        $this->set(compact('domainEvaluations'));
    }

    /**
     * View method
     *
     * @param string|null $id Domain Evaluation id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $domainEvaluation = $this->DomainEvaluations->get($id, [
            'contain' => ['Students', 'Evaluations', 'Domains'],
        ]);

        $this->set(compact('domainEvaluation'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $domainEvaluation = $this->DomainEvaluations->newEmptyEntity();
        if ($this->request->is('post')) {
            $domainEvaluation = $this->DomainEvaluations->patchEntity($domainEvaluation, $this->request->getData());
            if ($this->DomainEvaluations->save($domainEvaluation)) {
                $this->Flash->success(__('The domain evaluation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The domain evaluation could not be saved. Please, try again.'));
        }
        $students = $this->DomainEvaluations->Students->find('list', ['limit' => 200]);
        $evaluations = $this->DomainEvaluations->Evaluations->find('list', ['limit' => 200]);
        $domains = $this->DomainEvaluations->Domains->find('list', ['limit' => 200]);
        $this->set(compact('domainEvaluation', 'students', 'evaluations', 'domains'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Domain Evaluation id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $domainEvaluation = $this->DomainEvaluations->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $domainEvaluation = $this->DomainEvaluations->patchEntity($domainEvaluation, $this->request->getData());
            if ($this->DomainEvaluations->save($domainEvaluation)) {
                $this->Flash->success(__('The domain evaluation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The domain evaluation could not be saved. Please, try again.'));
        }
        $students = $this->DomainEvaluations->Students->find('list', ['limit' => 200]);
        $evaluations = $this->DomainEvaluations->Evaluations->find('list', ['limit' => 200]);
        $domains = $this->DomainEvaluations->Domains->find('list', ['limit' => 200]);
        $this->set(compact('domainEvaluation', 'students', 'evaluations', 'domains'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Domain Evaluation id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $domainEvaluation = $this->DomainEvaluations->get($id);
        if ($this->DomainEvaluations->delete($domainEvaluation)) {
            $this->Flash->success(__('The domain evaluation has been deleted.'));
        } else {
            $this->Flash->error(__('The domain evaluation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
