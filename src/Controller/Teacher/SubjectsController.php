<?php declare(strict_types=1);

namespace App\Controller\Teacher;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;

class SubjectsController extends AppController
{
    protected $Users;
    protected $Teachers;

    public function initialize(): void
    {
        parent::initialize();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
        $this->Teachers = TableRegistry::getTableLocator()->get('Teachers');
    }

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('teacher');
        parent::beforeFilter($event);  // TODO: Change the autogenerated stub
    }

    public function index()
    {
        $entity = $this->Subjects->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getSubjects()
    {
        $data = $this->Subjects->find();
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function add()
    {
        // dd($this->request->getAttribute('identity')->teacher->id);
        $subject = $this->Subjects->newEmptyEntity();
        $teachers = $this->Teachers->find('list', [
            'valueField' => function ($query) {
                return strval($query->first_name) . ', ' . strval($query->middle_name) . ', ' . strval($query->last_name);
            },
            'keyField' => function ($query) {
                return $query->id;
            },
        ]);
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['teacher_id'] = $this->request->getAttribute('identity')->teacher->id;
            $subject = $this->Subjects->patchEntity($subject, $data);

            try {
                $subject->teacher_id = $this->request->getAttribute('identity')->teacher->id;
                if ($this->Subjects->save($subject)) {
                    $result = ['message' => ucwords('The subject has been saved.'), 'result' => ucwords('Success')];
                    return $this->response
                        ->withStatus(200)
                        ->withType('application/json')
                        ->withStringBody(json_encode($result));
                } else {
                    foreach ($subject->getErrors() as $key => $value) {
                        $result = ['message' => ucwords(reset($value)), 'result' => ucwords('Error'), 'fields' => $subject->getErrors()];
                        return $this->response
                            ->withStatus(422)
                            ->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }
                }
            } catch (\Exception $e) {
                // Handle other exceptions as needed
                $error = ['message' => 'Database Error, teacher_id does not exist in teachers table', 'result' => 'Error'];
                return $this->response
                    ->withStatus(200)  // You can choose an appropriate HTTP status code
                    ->withType('application/json')
                    ->withStringBody(json_encode($error));
            }
        }
        $this->set(compact('subject', 'teachers'));
    }

    public function edit($id = null)
    {
        $subject = $this->Subjects->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subject = $this->Subjects->patchEntity($subject, $this->request->getData());
            if ($this->Subjects->save($subject)) {
                $result = ['message' => ucwords('The subject has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                $result = ['message' => ucwords('The subject could not be saved.'), 'result' => ucwords('Error')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($subject));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subject = $this->Subjects->get($id);
        if ($this->Subjects->delete($subject)) {
            $result = ['message' => ucwords('The subject has been deleted.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The subject could not be deleted. Please, try again.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
        }
    }
}
