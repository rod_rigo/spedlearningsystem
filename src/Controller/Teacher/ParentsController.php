<?php declare(strict_types=1);

namespace App\Controller\Teacher;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class ParentsController extends AppController
{
    protected $Users;

    public function initialize(): void
    {
        parent::initialize();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
        $this->Teachers = TableRegistry::getTableLocator()->get('Teachers');
        $this->Students = TableRegistry::getTableLocator()->get('Students');
    }

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('teacher');
        parent::beforeFilter($event);  // TODO: Change the autogenerated stub
    }

    public function index()
    {
        // dd($this->request->getAttribute('identity')->id);
        $entity = $this->Parents->newEmptyEntity();
        $teachers = $this->Teachers->find('list', [
            'valueField' => function ($query) {
                return strval($query->first_name) . ', ' . strval($query->middle_name) . ', ' . strval($query->last_name);
            },
            'keyField' => function ($query) {
                return $query->id;
            },
        ]);
        $this->set(compact('entity', 'teachers'));
    }

    public function getParents()
    {
        $data = $this->Users->find()->where([ 'OR' => [ 'is_parent' => 1 ], [ 'is_student' => 1 ] ])->contain(['Parents', 'Students']);
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
        // $data = $this->Parents->find();
        // return $this->response
        //     ->withType('application/json')
        //     ->withStringBody(json_encode(['data' => $data]));
    }

    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        $teachers = $this->Teachers->find('list', [
            'valueField' => function ($query) {
                return strval($query->first_name) . ', ' . strval($query->middle_name) . ', ' . strval($query->last_name);
            },
            'keyField' => function ($query) {
                return $query->id;
            },
        ])->where([
            'Teachers.user_id =' => intval($this->Authentication->getIdentity()->id)
        ]);
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            // dd($this->request->getData('teacher.first_name'));
            $validator = new Validator();

            $validator
                ->requirePresence('file', 'create')
                ->notEmptyFile('file', ucwords('please upload image!'), false)
                ->add('file', 'file', [
                    'rule' => function ($value) {
                        $extensions = ['image/png', 'image/jpeg', 'image/jpg'];
                        if (!in_array($value->getClientMediaType(), $extensions)) {
                            return ucwords('invalid image format!');
                        }
                        return true;
                    }
                ]);

            // $validator
            //     ->add('teacher', 'custom', [
            //     'rule' => function ($value, $context) {
            //         dd($value['first_name']);
            //         return mb_strlen($value) >= 10;
            //     },
            //     'message' => 'First names need to be at least 10 characters long',
            // ]);

            $validator
                ->requirePresence('confirm_password', 'create')
                ->notEmptyString('confirm_password', ucwords('please confirm your password!'), false)
                ->sameAs('confirm_password', 'password', ucwords('password confirmation not matched!'));

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value) {
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('Error'), 'fields' => $errors];
                return $this->response
                    ->withStatus(422)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            if ($user->hasErrors() == false && strlen($this->request->getData('file')->getClientFileName()) > 0) {
                $file = $this->request->getData('file');

                $filename = uniqid() . $file->getClientFileName();

                $folder = new Folder();

                $path = WWW_ROOT . 'img' . DS . 'user-img';
                if (!$folder->cd($path)) {
                    $folder->create($path);
                }

                $filepath = WWW_ROOT . 'img' . DS . 'user-img' . DS . $filename;

                try {
                    $file->moveTo($filepath);
                    $user->user_image = 'user-img/' . $filename;
                } catch (\Exception $exception) {
                    $user->user_image = 'image not moved';
                }
            }

            $user->is_parent = 1;
            $user->is_student = 1;
            $user->token = rand(11111111, 99999999);

            if ($this->Users->save($user)) {
                $result = ['message' => ucwords('The user has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                foreach ($user->getErrors() as $key => $value) {
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('Error'), 'fields' => $user->getErrors()];
                    return $this->response
                        ->withStatus(422)
                        ->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $this->set(compact('user', 'teachers'));
    }

    public function edit($id = null)
    {
        $parent = $this->Parents->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $parent = $this->Parents->patchEntity($parent, $this->request->getData());
            if ($this->Parents->save($parent)) {
                $result = ['message' => ucwords('The parent has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                $result = ['message' => ucwords('The parent could not be saved.'), 'result' => ucwords('Error')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($parent));
    }

    public function editParent($id = null)
    {
        $parent = $this->Parents->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $parent = $this->Parents->patchEntity($parent, $this->request->getData());
            if ($this->Parents->save($parent)) {
                $result = ['message' => ucwords('The parent has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                $result = ['message' => ucwords('The parent could not be saved.'), 'result' => ucwords('Error')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($parent));
    }

    public function editStudent($id = null)
    {
        $student = $this->Students->get($id, [
            'contain' => ['Teachers'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $student = $this->Students->patchEntity($student, $this->request->getData());
            if ($this->Students->save($student)) {
                $result = ['message' => ucwords('The student has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                $result = ['message' => ucwords('The student could not be saved.'), 'result' => ucwords('Error')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($student));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $result = ['message' => ucwords('The data has been deleted.'), 'result' => ucwords('Success')];
            return $this->response
                ->withStatus(200)
                ->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The data could not be deleted. Please, try again.'), 'result' => ucwords('Error')];
            return $this->response
                ->withStatus(422)
                ->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function viewGrades(){
        $connection = ConnectionManager::get('default');
        $results = $connection
            ->execute('SELECT subjects.subject, students.first_name, students.middle_name, students.last_name, SUM(assessments.points) as points, SUM(assessments.total) as total FROM assessments INNER JOIN students ON assessments.student_id = students.id INNER JOIN subjects ON assessments.subject_id = subjects.id GROUP BY subject_id')
            ->fetchAll('assoc');

        $subjs = $connection
            ->execute('SELECT subject FROM subjects')
            ->fetchAll('assoc');

        $groupedResults = [];
        foreach ($results as $result) {
            $firstName = $result['first_name'];
            $middleName = $result['middle_name'];
            $lastName = $result['last_name'];

            $fullName = "$lastName, $firstName, $middleName";

            if (!isset($groupedResults[$fullName])) {
                $groupedResults[$fullName] = [
                    'first_name' => $firstName,
                    'middle_name' => $middleName,
                    'last_name' => $lastName,
                    'grades' => [],
                ];
            }

            $groupedResults[$fullName]['grades'][$result['subject']] = [
                'subject' => $result['subject'],
                'points' => $result['points'],
                'total' => $result['total'],
                'grade' => round((($result['total'] / $result['points']) * 100), 0)
            ];
        }
        // dd($subjs);
        // dd($groupedResults);


        // dd($results);
        
        $this->set(compact('groupedResults', 'subjs'));
        // $this->set(compact('entity', 'teachers'));
    }
}
