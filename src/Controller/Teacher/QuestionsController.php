<?php
declare(strict_types=1);

namespace App\Controller\Teacher;

use App\Controller\AppController;

class QuestionsController extends AppController
{
    public function index()
    {
        $this->paginate = [
            'contain' => ['Quizzes'],
        ];
        $questions = $this->paginate($this->Questions);

        $this->set(compact('questions'));
    }

    public function view($id = null)
    {
        $question = $this->Questions->get($id, [
            'contain' => ['Quizzes', 'Answers', 'Choices'],
        ]);

        $this->set(compact('question'));
    }

    public function add()
    {
        $question = $this->Questions->newEmptyEntity();
        if ($this->request->is('post')) {
            $question = $this->Questions->patchEntity($question, $this->request->getData());
            if ($this->Questions->save($question)) {
                $this->Flash->success(__('The question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The question could not be saved. Please, try again.'));
        }
        $quizzes = $this->Questions->Quizzes->find('list', ['limit' => 200]);
        $this->set(compact('question', 'quizzes'));
    }

    public function edit($id = null)
    {
        $question = $this->Questions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $question = $this->Questions->patchEntity($question, $this->request->getData());
            if ($this->Questions->save($question)) {
                $this->Flash->success(__('The question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The question could not be saved. Please, try again.'));
        }
        $quizzes = $this->Questions->Quizzes->find('list', ['limit' => 200]);
        $this->set(compact('question', 'quizzes'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $question = $this->Questions->get($id);
        if ($this->Questions->delete($question)) {
            $this->Flash->success(__('The question has been deleted.'));
        } else {
            $this->Flash->error(__('The question could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
