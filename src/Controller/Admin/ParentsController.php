<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Parents Controller
 *
 * @property \App\Model\Table\ParentsTable $Parents
 * @method \App\Model\Entity\Parent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ParentsController extends AppController
{
    protected $Users;

    public function initialize(): void
    {
        parent::initialize();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
    }

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event);  // TODO: Change the autogenerated stub
    }

    public function index()
    {
        $entity = $this->Parents->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getParents()
    {
        $data = $this->Parents->find();
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function getParentsLists()
    {
        $data = $this->Parents->find('list',[
            'valueField' => function($query){
                return $query->first_name.' '.$query->middle_name.' '.$query->last_name;
            },
            'keyField' => function($query){
                return $query->id;
            }
        ]);
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            // dd($this->request->getData('teacher.first_name'));
            $validator = new Validator();

            $validator
                ->requirePresence('file', 'create')
                ->notEmptyFile('file', ucwords('please upload image!'), false)
                ->add('file', 'file', [
                    'rule' => function ($value) {
                        $extensions = ['image/png', 'image/jpeg', 'image/jpg'];
                        if (!in_array($value->getClientMediaType(), $extensions)) {
                            return ucwords('invalid image format!');
                        }
                        return true;
                    }
                ]);

            // $validator
            //     ->add('teacher', 'custom', [
            //     'rule' => function ($value, $context) {
            //         dd($value['first_name']);
            //         return mb_strlen($value) >= 10;
            //     },
            //     'message' => 'First names need to be at least 10 characters long',
            // ]);

            $validator
                ->requirePresence('confirm_password', 'create')
                ->notEmptyString('confirm_password', ucwords('please confirm your password!'), false)
                ->sameAs('confirm_password', 'password', ucwords('password confirmation not matched!'));

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value) {
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('Error'), 'fields' => $errors];
                return $this->response
                    ->withStatus(422)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            if ($user->hasErrors() == false && strlen($this->request->getData('file')->getClientFileName()) > 0) {
                $file = $this->request->getData('file');

                $filename = uniqid() . $file->getClientFileName();

                $folder = new Folder();

                $path = WWW_ROOT . 'img' . DS . 'user-img';
                if (!$folder->cd($path)) {
                    $folder->create($path);
                }

                $filepath = WWW_ROOT . 'img' . DS . 'user-img' . DS . $filename;

                try {
                    $file->moveTo($filepath);
                    $user->user_image = 'user-img/' . $filename;
                } catch (\Exception $exception) {
                    $user->user_image = 'image not moved';
                }
            }

            $user->is_parent = 1;
            $user->token = rand(11111111, 99999999);

            if ($this->Users->save($user)) {
                $result = ['message' => ucwords('The user has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                foreach ($user->getErrors() as $key => $value) {
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('Error'), 'fields' => $user->getErrors()];
                    return $this->response
                        ->withStatus(422)
                        ->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $this->set(compact('user'));
    }

    public function edit($id = null)
    {
        $parent = $this->Parents->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $parent = $this->Parents->patchEntity($parent, $this->request->getData());
            if ($this->Parents->save($parent)) {
                $result = ['message' => ucwords('The parent has been saved.'), 'result' => ucwords('Success')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                $result = ['message' => ucwords('The parent could not be saved.'), 'result' => ucwords('Error')];
                return $this->response
                    ->withStatus(200)
                    ->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($parent));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $parent = $this->Parents->get($id);
        if ($this->Parents->delete($parent)) {
            $result = ['message' => ucwords('The parent has been deleted.'), 'result' => ucwords('Success')];
            return $this->response
                ->withStatus(200)
                ->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The parent could not be deleted. Please, try again.'), 'result' => ucwords('Error')];
            return $this->response
                ->withStatus(422)
                ->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }
}
