<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Websites Controller
 *
 */
class WebsitesController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['index']);
    }

    public function index()
    {
        $carousels = $this->getTableLocator()->get('Carousels')->find();
        $abouts = $this->getTableLocator()->get('Abouts')->find();
        $contacts = $this->getTableLocator()->get('Contacts')->find()->first();
        $this->set(compact('carousels','abouts','contacts'));
    }

}
