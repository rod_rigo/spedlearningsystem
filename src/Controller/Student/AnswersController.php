<?php
declare(strict_types=1);

namespace App\Controller\Student;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use function Symfony\Component\Config\Definition\Builder\find;

/**
 * Answers Controller
 *
 * @property \App\Model\Table\AnswersTable $Answers
 * @method \App\Model\Entity\Answer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AnswersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

    }

    public function check(){
        $this->request->allowMethod(['post']);

        $connection = ConnectionManager::get('default');

        $connection->begin();

        if($this->request->is('post')){

          try{
              $answers = $this->Answers->find('all')
                  ->where([
                      'answers.question_id =' => $this->request->getData('question_id'),
                      'answers.assessment_id =' => $this->request->getData('assessment_id'),
                  ])
                  ->first();

              if(empty($answers)){

                  $answer = $this->Answers->newEmptyEntity();
                  $answer = $this->Answers->patchEntity($answer,$this->request->getData());

                  $question = $this->getTableLocator()->get('Questions')->get($this->request->getData('question_id'));

                  $answer->points = strtoupper($question->answer) == strtoupper($this->request->getData('answer'))? $question->points: 0;

                  if($this->Answers->save($answer)){
                      $result = ['message' => ucwords('the answer has been saved'), 'result' => ucwords('success')];
                      return $this->response->withStatus(200)->withType('application/json')
                          ->withStringBody(json_encode($result));
                  }else{
                      $result = ['message' => ucwords('the answer has not been saved'), 'result' => ucwords('error')];
                      return $this->response->withStatus(422)->withType('application/json')
                          ->withStringBody(json_encode($result));
                  }

              }else{

                  $answer = $this->Answers->get($answers->id);
                  $answer = $this->Answers->patchEntity($answer,$this->request->getData());

                  $question = $this->getTableLocator()->get('Questions')->get($this->request->getData('question_id'));

                  $answer->points = strtoupper($question->answer) == strtoupper($this->request->getData('answer'))? $question->points: 0;

                  if($this->Answers->save($answer)){
                      $result = ['message' => ucwords('the answer has been saved'), 'result' => ucwords('success')];
                      return $this->response->withStatus(200)->withType('application/json')
                          ->withStringBody(json_encode($result));
                  }else{
                      $result = ['message' => ucwords('the answer has not been saved'), 'result' => ucwords('error')];
                      return $this->response->withStatus(422)->withType('application/json')
                          ->withStringBody(json_encode($result));
                  }

              }

          }catch (\Exception $exception){
              $connection->rollback();
              dd($exception->getMessage());
          }finally{
              $connection->commit();
          }


        }
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $answer = $this->Answers->newEmptyEntity();
        if ($this->request->is('post')) {
            $answer = $this->Answers->patchEntity($answer, $this->request->getData());
            if ($this->Answers->save($answer)) {
                $this->Flash->success(__('The answer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The answer could not be saved. Please, try again.'));
        }
        $assessments = $this->Answers->Assessments->find('list', ['limit' => 200]);
        $questions = $this->Answers->Questions->find('list', ['limit' => 200]);
        $this->set(compact('answer', 'assessments', 'questions'));
    }




}
