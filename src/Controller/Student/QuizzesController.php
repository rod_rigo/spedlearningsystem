<?php
declare(strict_types=1);

namespace App\Controller\Student;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;
use Moment\Moment;

/**
 * Quizzes Controller
 *
 * @property \App\Model\Table\QuizzesTable $Quizzes
 * @method \App\Model\Entity\Quiz[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QuizzesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('student');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function index()
    {
        $sub = (new Moment(null,'Asia/Manila'))->subtractDays(7)->format('Y-m-d');
        $add = (new Moment(null,'Asia/Manila'))->addDays(14)->format('Y-m-d');
        $quizzes = $this->Quizzes->find()
            ->where([

                'OR' => [
                    [
                        'quizzes.teacher_id =' => $this->request->getAttribute('identity')->student->teacher_id,
                        'quizzes.is_special =' => intval(1),
                        'quizzes.is_special !=' => intval(0),
                    ],
                    [
                        'quizzes.date >=' => $sub,
                        'quizzes.date <=' => $add,
                    ]
                ]
            ])
            ->contain([
                'Subjects' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]);

        $this->set(compact('quizzes'));
    }

    public function view($id = null){
      try{
          $quiz = $this->Quizzes->get(intval($id),[
              'contain' => [
                  'Questions' => [
                      'queryBuilder' => function($query){
                          return $query->find('all');
                      }
                  ],
                  'QuestionPoints' => [
                      'queryBuilder' => function($query){
                          return $query->find('all')
                              ->select(['quiz_id'])
                              ->select([
                                  'total_points' => 'SUM(points)'
                              ]);
                      }
                  ]
              ]
          ]);

          if(boolval(!$quiz->is_special)){
              $now = new \DateTime();
              $start_time = new \DateTime($quiz->date.' '.$quiz->start_time);
              $end_time = new \DateTime($quiz->date.' '.$quiz->end_time);

              if($now > $start_time && $now > $end_time ){
                  $this->Flash->success(ucwords('the quiz is already ended'));
                  return $this->redirect(['prefix' => 'Student', 'controller' => 'Quizzes', 'action' => 'index']);
              }

              $assessment = $this->getTableLocator()->get('Assessments')
                  ->findOrCreate([
                      'assessments.quiz_id =' => $quiz->id,
                      'assessments.subject_id =' => $quiz->subject_id,
                      'assessments.student_id =' => $this->request->getAttribute('identity')->student->id,
                  ], function ($query) use($quiz){
                      $query->quiz_id = $quiz->id;
                      $query->subject_id = $quiz->subject_id;
                      $query->student_id = $this->request->getAttribute('identity')->student->id;
                      $query->points = 0;
                      $query->total = $quiz->question_points[0]->total_points;
                      $query->is_checked = 0;
                  });

              $assessment = $this->getTableLocator()->get('Assessments')
                  ->get($assessment->id,[
                      'contain' => [
                          'Answers' => [
                              'queryBuilder' => function($query){
                                  return $query->find('all');
                              }
                          ],
                          'Quizzes' => [
                              'queryBuilder' => function($query){
                                  return $query->find('all');
                              }
                          ]
                      ]
                  ]);

              if(boolval($assessment->is_checked)){
                  $this->Flash->error(ucwords('Your assesment is already checked'));
                  return $this->redirect($this->referer());
              }

              $this->set(compact('quiz','assessment'));
          }else{

              $assessments = TableRegistry::getTableLocator()->get('Assessments')->find()
                  ->where([
                      'Assessments.quiz_id =' => intval($quiz->id),
                      'Assessments.student_id =' => intval($this->request->getAttribute('identity')->student->id),
                  ])
                  ->first();

              if(!empty($assessments)){
                  $this->Flash->success(ucwords('You Aleady Take This Quiz'));
                  return $this->redirect(['prefix' => 'Student', 'controller' => 'Quizzes', 'action' => 'index']);
              }

              $assessment = $this->getTableLocator()->get('Assessments')
                  ->findOrCreate([
                      'assessments.quiz_id =' => $quiz->id,
                      'assessments.subject_id =' => $quiz->subject_id,
                      'assessments.student_id =' => $this->request->getAttribute('identity')->student->id,
                  ], function ($query) use($quiz){
                      $query->quiz_id = $quiz->id;
                      $query->subject_id = $quiz->subject_id;
                      $query->student_id = $this->request->getAttribute('identity')->student->id;
                      $query->points = 0;
                      $query->total = $quiz->question_points[0]->total_points;
                      $query->is_checked = 0;
                  });

              $assessment = $this->getTableLocator()->get('Assessments')
                  ->get($assessment->id,[
                      'contain' => [
                          'Answers' => [
                              'queryBuilder' => function($query){
                                  return $query->find('all');
                              }
                          ],
                          'Quizzes' => [
                              'queryBuilder' => function($query){
                                  return $query->find('all');
                              }
                          ]
                      ]
                  ]);

              if(boolval($assessment->is_checked)){
                  $this->Flash->error(ucwords('Your assesment is already checked'));
                  return $this->redirect($this->referer());
              }

              $this->set(compact('quiz','assessment'));

          }


      }catch (\Exception $exception){
          $this->Flash->error(ucwords('The quiz is not found'));
          return $this->redirect($this->referer());
      }
    }


}
