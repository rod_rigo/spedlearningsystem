<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Domain Entity
 *
 * @property int $id
 * @property int $teacher_id
 * @property int $domain_type_id
 * @property string $domain
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Teacher $teacher
 * @property \App\Model\Entity\DomainType $domain_type
 * @property \App\Model\Entity\DomainEvaluation[] $domain_evaluations
 */
class Domain extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'teacher_id' => true,
        'domain_type_id' => true,
        'domain' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'teacher' => true,
        'domain_type' => true,
        'domain_evaluations' => true,
    ];
}
