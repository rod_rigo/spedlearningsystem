<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Evaluation Entity
 *
 * @property int $id
 * @property int $student_id
 * @property int $teacher_id
 * @property int $evaluation_month_id
 * @property int $raw_score_total
 * @property int $scaled_score_total
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Student $student
 * @property \App\Model\Entity\Teacher $teacher
 * @property \App\Model\Entity\EvaluationMonth $evaluation_month
 * @property \App\Model\Entity\DomainEvaluation[] $domain_evaluations
 */
class Evaluation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'student_id' => true,
        'teacher_id' => true,
        'evaluation_month_id' => true,
        'raw_score_total' => true,
        'scaled_score_total' => true,
        'interpretation' => true,
        'standard_score' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'student' => true,
        'teacher' => true,
        'evaluation_month' => true,
        'domain_evaluations' => true,
    ];
}
