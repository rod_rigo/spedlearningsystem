<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Teacher Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property int $gender
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Audio[] $audios
 * @property \App\Model\Entity\Module[] $modules
 * @property \App\Model\Entity\Quiz[] $quizzes
 * @property \App\Model\Entity\Student[] $students
 * @property \App\Model\Entity\Subject[] $subjects
 * @property \App\Model\Entity\Video[] $videos
 */
class Teacher extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'gender' => true,
        'birthdate' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'audios' => true,
        'modules' => true,
        'quizzes' => true,
        'students' => true,
        'subjects' => true,
        'videos' => true,
    ];

    protected function _setFirstName($value){
        return ucwords($value);
    }

    protected function _setMiddleName($value){
        return ucwords($value);
    }

    protected function _setLastName($value){
        return ucwords($value);
    }

}
