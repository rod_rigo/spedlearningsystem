<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Parent Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property int $gender
 * @property string $address
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Children[] $childrens
 */
class Parents extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'login_id' => true,
        'password' => true,
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'birthdate' => true,
        'gender' => true,
        'address' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        // 'childrens' => true,
    ];

    protected $_hidden = [
        'password',
    ];

    protected function _setFirstName($value){
        return ucwords($value);
    }

    protected function _setMiddleName($value){
        return ucwords($value);
    }

    protected function _setLastName($value){
        return ucwords($value);
    }

    protected function _setAddress($value){
        return ucwords($value);
    }

    protected function _setPassword(string $value): ?string{
        return strlen($value) > 0 ? (new DefaultPasswordHasher())->hash($value): (new DefaultPasswordHasher())->hash('1234');
    }

}
