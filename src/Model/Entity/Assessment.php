<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Assessment Entity
 *
 * @property int $id
 * @property int $quiz_id
 * @property int $subject_id
 * @property int $student_id
 * @property float $points
 * @property float $total
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Quiz $quiz
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\Student $student
 * @property \App\Model\Entity\Answer[] $answers
 */
class Assessment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quiz_id' => true,
        'subject_id' => true,
        'student_id' => true,
        'points' => true,
        'total' => true,
        'is_checked' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'quiz' => true,
        'subject' => true,
        'student' => true,
        'answers' => true,
    ];
}
