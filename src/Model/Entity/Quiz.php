<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Quiz Entity
 *
 * @property int $id
 * @property int $teacher_id
 * @property int $subject_id
 * @property string $quiz
 * @property \Cake\I18n\FrozenDate $date
 * @property \Cake\I18n\Time $start_time
 * @property \Cake\I18n\Time $end_time
 * @property string $category
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Teacher $teacher
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\Assessment[] $assessments
 * @property \App\Model\Entity\Question[] $questions
 */
class Quiz extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'teacher_id' => true,
        'subject_id' => true,
        'quiz_type' => true,
        'quiz' => true,
        'date' => true,
        'is_special' => true,
        'start_time' => true,
        'end_time' => true,
        'category' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'teacher' => true,
        'subject' => true,
        'assessments' => true,
        'questions' => true,
    ];
}
