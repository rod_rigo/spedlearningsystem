<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Question Entity
 *
 * @property int $id
 * @property int $quiz_id
 * @property string $question
 * @property float $points
 * @property string $answer
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Quiz $quiz
 * @property \App\Model\Entity\Answer[] $answers
 * @property \App\Model\Entity\Choice[] $choices
 */
class Question extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quiz_id' => true,
        'question' => true,
        'points' => true,
        'answer' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'quiz' => true,
        'answers' => true,
        'choices' => true,
    ];
}
