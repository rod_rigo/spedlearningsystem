<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $contact_number
 * @property string $user_image
 * @property string $password
 * @property string $role
 * @property int $status
 * @property string|null $token
 * @property int $is_admin
 * @property int $is_student
 * @property int $is_parent
 * @property int $is_teacher
 * @property string|null $api_key
 * @property string|null $api_code
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Parent[] $parents
 * @property \App\Model\Entity\Student[] $students
 * @property \App\Model\Entity\Teacher[] $teachers
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'email' => true,
        'contact_number' => true,
        'user_image' => true,
        'password' => true,
        'role' => true,
        'status' => true,
        'token' => true,
        'is_admin' => true,
        'is_student' => true,
        'is_parent' => true,
        'is_teacher' => true,
        'api_key' => true,
        'api_code' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'parent' => true,
        'student' => true,
        'teacher' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setPassword(string $value): ?string{
        return strlen($value) > 0 ? (new DefaultPasswordHasher())->hash($value): (new DefaultPasswordHasher())->hash('1234');
    }

}
