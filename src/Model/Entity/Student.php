<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Student Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $teacher_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property int $gender
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property string $address
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Teacher $teacher
 * @property \App\Model\Entity\Assessment[] $assessments
 * @property \App\Model\Entity\Children[] $childrens
 */
class Student extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'teacher_id' => true,
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'gender' => true,
        'birthdate' => true,
        // 'address' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'teacher' => true,
        'assessments' => true,
        'evaluations' => true,
        // 'childrens' => true,
    ];

    protected function _setFirstName($value)
    {
        return ucwords($value);
    }

    protected function _setMiddleName($value)
    {
        return ucwords($value);
    }

    protected function _setLastName($value)
    {
        return ucwords($value);
    }

    protected function _setAddress($value)
    {
        return ucwords($value);
    }
}
