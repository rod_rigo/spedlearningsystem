<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Audio Entity
 *
 * @property int $id
 * @property int $teacher_id
 * @property string|null $audio
 * @property string $title
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Teacher $teacher
 */
class Audio extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'teacher_id' => true,
        'audio' => true,
        'title' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'teacher' => true,
    ];

    protected function _setTitle($value){
        return ucwords($value);
    }

}
