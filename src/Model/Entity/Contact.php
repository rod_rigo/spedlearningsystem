<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string|null $address
 * @property string $email
 * @property string $contact_number
 * @property float|null $latitude
 * @property float|null $longtitude
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 */
class Contact extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'address' => true,
        'email' => true,
        'contact_number' => true,
        'latitude' => true,
        'longtitude' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
    ];

    protected function _setAddress($value){
        return ucwords($value);
    }

}
