<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Event\EventInterface;
use Cake\Http\ServerRequest;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Psr\Http\Message\ServerRequestInterface;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Users Model
 *
 * @property \App\Model\Table\ParentsTable&\Cake\ORM\Association\HasMany $Parents
 * @property \App\Model\Table\StudentsTable&\Cake\ORM\Association\HasMany $Students
 * @property \App\Model\Table\TeachersTable&\Cake\ORM\Association\HasMany $Teachers
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    use SoftDeleteTrait;

    public function beforeSave(EventInterface $event)
    {
        $entity = $event->getData('entity');

        $key = uniqid() . uniqid() . uniqid();
        $result = Security::encrypt(strval($entity->id), $key);
        $entity->api_key = $key;
        $entity->api_code = base64_encode($result);
        return $entity;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasOne('Parents', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        $this->hasOne('Students', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        $this->hasOne('Teachers', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('contact_number')
            ->maxLength('contact_number', 255)
            ->requirePresence('contact_number', 'create')
            ->notEmptyString('contact_number','enter contact number ', false)
            ->add('contact_number','contact_number',[
                'rule' => function($value){
                    if(!preg_match_all('/^(09)([0-9]){9}$/',$value)){
                        return ucwords('contact number must 11 digits & start at 09');
                    }
                    return true;
                }
            ]);

        $validator
            ->scalar('user_image')
            ->maxLength('user_image', 255)
            ->allowEmptyString('user_image');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('role')
            ->maxLength('role', 255)
            ->allowEmptyString('role');

        $validator
            ->boolean('status')
            ->notEmptyString('status');

        $validator
            ->scalar('token')
            ->maxLength('token', 255)
            ->allowEmptyString('token');

        $validator
            ->boolean('is_admin')
            ->notEmptyString('is_admin');

        $validator
            ->boolean('is_student')
            ->notEmptyString('is_student');

        $validator
            ->boolean('is_parent')
            ->notEmptyString('is_parent');

        $validator
            ->boolean('is_teacher')
            ->notEmptyString('is_teacher');

        $validator
            ->scalar('api_key')
            ->maxLength('api_key', 4294967295)
            ->allowEmptyString('api_key');

        $validator
            ->scalar('api_code')
            ->maxLength('api_code', 4294967295)
            ->allowEmptyString('api_code');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $userId)
    {
        return $this->exists(['id' => $paramId, 'users.id' => $userId]);
    }

    public function findForAuthentication(\Cake\ORM\Query $query, array $options): \Cake\ORM\Query
    {
        return $query->contain([
            'Teachers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            'Parents' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            'Students' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username'],ucwords('this username already used!')), ['errorField' => 'username']);
        $rules->add(
            function ($entity, $options) {

                $query = $this->find('all',['withDeleted'])
                    ->where([
                        'users.username like' => $entity->username
                    ])
                    ->count();

                if(!$entity->isDirty('username')){
                    return true;
                }

                if ($query > 0) {
                    return ucwords('this username already used!');
                }

                return true;
            },
            [
                'errorField' => 'username',
            ]
        );

        $rules->add($rules->isUnique(['email'],ucwords('this email already used!')), ['errorField' => 'email']);
        $rules->add(
            function ($entity, $options) {

                $query = $this->find('all',['withDeleted'])
                    ->where([
                        'users.email like' => $entity->email
                    ])
                    ->count();

                if(!$entity->isDirty('email')){
                    return true;
                }

                if ($query > 0) {
                    return ucwords('this email already used!');
                }

                return true;
            },
            [
                'errorField' => 'email',
            ]
        );

        $rules->add($rules->isUnique(['contact_number'],ucwords('this contact number already used!')), ['errorField' => 'contact_number']);
        $rules->add(
            function ($entity, $options) {

                $query = $this->find('all',['withDeleted'])
                    ->where([
                        'users.contact_number like' => $entity->contact_number
                    ])
                    ->count();

                if(!$entity->isDirty('contact_number')){
                    return true;
                }

                if ($query > 0) {
                    return ucwords('this contact number already used!');
                }

                return true;
            },
            [
                'errorField' => 'contact_number',
            ]
        );

        return $rules;
    }
}
