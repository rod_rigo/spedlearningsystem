<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LessonPlans Model
 *
 * @property \App\Model\Table\TeachersTable&\Cake\ORM\Association\BelongsTo $Teachers
 *
 * @method \App\Model\Entity\LessonPlan newEmptyEntity()
 * @method \App\Model\Entity\LessonPlan newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LessonPlan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LessonPlan get($primaryKey, $options = [])
 * @method \App\Model\Entity\LessonPlan findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LessonPlan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LessonPlan[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LessonPlan|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LessonPlan saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LessonPlan[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LessonPlan[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LessonPlan[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LessonPlan[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LessonPlansTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('lesson_plans');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Teachers', [
            'foreignKey' => 'teacher_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('monday')
            ->maxLength('monday', 255)
            ->allowEmptyString('monday');

        $validator
            ->scalar('tuesday')
            ->maxLength('tuesday', 255)
            ->allowEmptyString('tuesday');

        $validator
            ->scalar('wednesday')
            ->maxLength('wednesday', 255)
            ->allowEmptyString('wednesday');

        $validator
            ->scalar('thursday')
            ->maxLength('thursday', 255)
            ->allowEmptyString('thursday');

        $validator
            ->scalar('friday')
            ->maxLength('friday', 255)
            ->allowEmptyString('friday');

        $validator
            ->integer('status')
            ->allowEmptyString('status', null, 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['teacher_id'], 'Teachers'), ['errorField' => 'teacher_id']);

        return $rules;
    }
}
