<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * DomainEvaluations Model
 *
 * @property \App\Model\Table\StudentsTable&\Cake\ORM\Association\BelongsTo $Students
 * @property \App\Model\Table\EvaluationsTable&\Cake\ORM\Association\BelongsTo $Evaluations
 * @property \App\Model\Table\DomainsTable&\Cake\ORM\Association\BelongsTo $Domains
 *
 * @method \App\Model\Entity\DomainEvaluation newEmptyEntity()
 * @method \App\Model\Entity\DomainEvaluation newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\DomainEvaluation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DomainEvaluation get($primaryKey, $options = [])
 * @method \App\Model\Entity\DomainEvaluation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\DomainEvaluation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DomainEvaluation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\DomainEvaluation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DomainEvaluation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DomainEvaluation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DomainEvaluation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\DomainEvaluation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DomainEvaluation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DomainEvaluationsTable extends Table
{
    use LocatorAwareTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('domain_evaluations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Students', [
            'foreignKey' => 'student_id',
        ]);
        $this->belongsTo('Evaluations', [
            'foreignKey' => 'evaluation_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Domains', [
            'foreignKey' => 'domain_id',
            'joinType' => 'INNER',
        ]);
    }

    // public function afterSave(EventInterface $event)
    // {
    //     $entity = $event->getData('entity');

    //     $answers = $this->getTableLocator()->get('Answers')->find()
    //         ->where([
    //             'answers.assessment_id =' => $entity->assessment_id
    //         ])
    //         ->select([
    //             'total_points' => 'SUM(answers.points)'
    //         ])
    //         ->first();

    //     $assesment = $this->getTableLocator()->get('Assessments')->get($entity->assessment_id);
    //     $assesment->points = $answers->total_points;
    //     $this->getTableLocator()->get('Assessments')->save($assesment);
    // }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('is_checked');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['student_id'], 'Students'), ['errorField' => 'student_id']);
        $rules->add($rules->existsIn(['evaluation_id'], 'Evaluations'), ['errorField' => 'evaluation_id']);
        $rules->add($rules->existsIn(['domain_id'], 'Domains'), ['errorField' => 'domain_id']);

        return $rules;
    }
}
