<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ModulesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ModulesTable Test Case
 */
class ModulesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ModulesTable
     */
    protected $Modules;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Modules',
        'app.Teachers',
        'app.Subjects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Modules') ? [] : ['className' => ModulesTable::class];
        $this->Modules = $this->getTableLocator()->get('Modules', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Modules);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test query method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::query()
     */
    public function testQuery(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test deleteAll method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::deleteAll()
     */
    public function testDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getSoftDeleteField method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::getSoftDeleteField()
     */
    public function testGetSoftDeleteField(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDelete method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::hardDelete()
     */
    public function testHardDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDeleteAll method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::hardDeleteAll()
     */
    public function testHardDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test restore method
     *
     * @return void
     * @uses \App\Model\Table\ModulesTable::restore()
     */
    public function testRestore(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
