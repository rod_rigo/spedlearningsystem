<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuizzesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuizzesTable Test Case
 */
class QuizzesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QuizzesTable
     */
    protected $Quizzes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Quizzes',
        'app.Teachers',
        'app.Subjects',
        'app.Assessments',
        'app.Questions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Quizzes') ? [] : ['className' => QuizzesTable::class];
        $this->Quizzes = $this->getTableLocator()->get('Quizzes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Quizzes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test query method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::query()
     */
    public function testQuery(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test deleteAll method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::deleteAll()
     */
    public function testDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getSoftDeleteField method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::getSoftDeleteField()
     */
    public function testGetSoftDeleteField(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDelete method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::hardDelete()
     */
    public function testHardDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDeleteAll method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::hardDeleteAll()
     */
    public function testHardDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test restore method
     *
     * @return void
     * @uses \App\Model\Table\QuizzesTable::restore()
     */
    public function testRestore(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
