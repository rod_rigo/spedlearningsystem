<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarouselsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CarouselsTable Test Case
 */
class CarouselsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CarouselsTable
     */
    protected $Carousels;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Carousels',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Carousels') ? [] : ['className' => CarouselsTable::class];
        $this->Carousels = $this->getTableLocator()->get('Carousels', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Carousels);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CarouselsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
