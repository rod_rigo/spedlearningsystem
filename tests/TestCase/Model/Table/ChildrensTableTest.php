<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChildrensTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChildrensTable Test Case
 */
class ChildrensTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChildrensTable
     */
    protected $Childrens;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Childrens',
        'app.Students',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Childrens') ? [] : ['className' => ChildrensTable::class];
        $this->Childrens = $this->getTableLocator()->get('Childrens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Childrens);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ChildrensTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ChildrensTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
