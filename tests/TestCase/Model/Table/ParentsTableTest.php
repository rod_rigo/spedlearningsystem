<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ParentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ParentsTable Test Case
 */
class ParentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ParentsTable
     */
    protected $Parents;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Parents',
        'app.Users',
        'app.Childrens',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Parents') ? [] : ['className' => ParentsTable::class];
        $this->Parents = $this->getTableLocator()->get('Parents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Parents);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ParentsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ParentsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
