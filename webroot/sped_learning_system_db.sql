/*
 Navicat Premium Data Transfer

 Source Server         : phpmyadmin
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : sped_learning_system_db

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 21/11/2023 20:34:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for abouts
-- ----------------------------
DROP TABLE IF EXISTS `abouts`;
CREATE TABLE `abouts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `background` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of abouts
-- ----------------------------

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `assessment_id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED NOT NULL,
  `answer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `points` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answers to assessments`(`assessment_id` ASC) USING BTREE,
  INDEX `answers to questions`(`question_id` ASC) USING BTREE,
  CONSTRAINT `answers to assessments` FOREIGN KEY (`assessment_id`) REFERENCES `assessments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of answers
-- ----------------------------

-- ----------------------------
-- Table structure for assessments
-- ----------------------------
DROP TABLE IF EXISTS `assessments`;
CREATE TABLE `assessments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `quiz_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `student_id` bigint UNSIGNED NOT NULL,
  `points` double NOT NULL,
  `total` double NOT NULL,
  `is_checked` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assessments to quizzes`(`quiz_id` ASC) USING BTREE,
  INDEX `assessments to subjects`(`subject_id` ASC) USING BTREE,
  INDEX `assessments to students`(`student_id` ASC) USING BTREE,
  CONSTRAINT `assessments to quizzes` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assessments to students` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assessments to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of assessments
-- ----------------------------

-- ----------------------------
-- Table structure for audios
-- ----------------------------
DROP TABLE IF EXISTS `audios`;
CREATE TABLE `audios`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `audio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `audios to teachers`(`teacher_id` ASC) USING BTREE,
  CONSTRAINT `audios to teachers` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of audios
-- ----------------------------

-- ----------------------------
-- Table structure for carousels
-- ----------------------------
DROP TABLE IF EXISTS `carousels`;
CREATE TABLE `carousels`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `background` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of carousels
-- ----------------------------

-- ----------------------------
-- Table structure for choices
-- ----------------------------
DROP TABLE IF EXISTS `choices`;
CREATE TABLE `choices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `question_id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `answer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `choices to questions`(`question_id` ASC) USING BTREE,
  CONSTRAINT `choices to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of choices
-- ----------------------------

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `contact_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `latitude` double NULL DEFAULT NULL,
  `longtitude` double NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of contacts
-- ----------------------------

-- ----------------------------
-- Table structure for domain_evaluations
-- ----------------------------
DROP TABLE IF EXISTS `domain_evaluations`;
CREATE TABLE `domain_evaluations`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `evaluation_id` bigint UNSIGNED NOT NULL,
  `domain_id` bigint UNSIGNED NOT NULL,
  `is_checked` tinyint NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 113 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of domain_evaluations
-- ----------------------------
INSERT INTO `domain_evaluations` VALUES (112, '1', 8, 15, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (111, '1', 8, 14, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (110, '1', 8, 13, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (109, '1', 8, 12, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (108, '1', 8, 11, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (107, '1', 8, 10, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (106, '1', 8, 9, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (105, '1', 8, 8, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (104, '1', 8, 7, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (103, '1', 8, 6, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (102, '1', 8, 5, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (101, '1', 8, 4, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (100, '1', 8, 3, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (99, '1', 8, 2, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (98, '1', 7, 15, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (97, '1', 7, 14, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (96, '1', 7, 13, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (95, '1', 7, 12, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (94, '1', 7, 11, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (93, '1', 7, 10, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (92, '1', 7, 9, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (91, '1', 7, 8, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (90, '1', 7, 7, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (89, '1', 7, 6, 1, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (88, '1', 7, 5, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (87, '1', 7, 4, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (86, '1', 7, 3, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');
INSERT INTO `domain_evaluations` VALUES (85, '1', 7, 2, 0, '2023-11-14 10:48:17', '2023-11-14 10:48:17');

-- ----------------------------
-- Table structure for domain_types
-- ----------------------------
DROP TABLE IF EXISTS `domain_types`;
CREATE TABLE `domain_types`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of domain_types
-- ----------------------------
INSERT INTO `domain_types` VALUES (4, 'Cognitive Domain', '2023-11-13 10:49:32', '2023-11-13 10:49:32');
INSERT INTO `domain_types` VALUES (5, 'Gross Motor Domain', '2023-11-13 10:49:51', '2023-11-13 10:49:51');
INSERT INTO `domain_types` VALUES (6, 'Selp-Help Domain', '2023-11-13 10:49:57', '2023-11-13 10:49:57');
INSERT INTO `domain_types` VALUES (7, 'Expressive Language Domain', '2023-11-13 10:50:10', '2023-11-13 10:50:10');
INSERT INTO `domain_types` VALUES (8, 'Fine Motor Domain', '2023-11-13 10:50:24', '2023-11-13 10:50:24');
INSERT INTO `domain_types` VALUES (9, 'Receptive Language Domain', '2023-11-13 10:50:30', '2023-11-13 10:50:30');
INSERT INTO `domain_types` VALUES (10, 'Social-Emotional Domain', '2023-11-13 10:50:37', '2023-11-13 10:50:37');

-- ----------------------------
-- Table structure for domains
-- ----------------------------
DROP TABLE IF EXISTS `domains`;
CREATE TABLE `domains`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `domain_type_id` bigint UNSIGNED NOT NULL,
  `domain` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `domains to teachers`(`teacher_id` ASC) USING BTREE,
  CONSTRAINT `domains to teachers` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of domains
-- ----------------------------
INSERT INTO `domains` VALUES (2, 1, 4, 'Cognitive 1', '2023-11-13 18:55:32', '2023-11-13 18:55:32', NULL);
INSERT INTO `domains` VALUES (3, 1, 4, 'Cognitive 2', '2023-11-13 18:55:39', '2023-11-13 18:55:39', NULL);
INSERT INTO `domains` VALUES (4, 1, 5, 'Gross Motor 1', '2023-11-13 18:55:51', '2023-11-13 18:55:51', NULL);
INSERT INTO `domains` VALUES (5, 1, 5, 'Gross Motor 2', '2023-11-13 18:55:54', '2023-11-13 18:55:54', NULL);
INSERT INTO `domains` VALUES (6, 1, 6, 'Self-Help Domain 1', '2023-11-13 18:56:09', '2023-11-13 18:56:09', NULL);
INSERT INTO `domains` VALUES (7, 1, 6, 'Self-Help Domain 2', '2023-11-13 18:56:14', '2023-11-13 18:56:14', NULL);
INSERT INTO `domains` VALUES (8, 1, 7, 'Expressive Language Domain 1', '2023-11-13 18:56:28', '2023-11-13 18:56:28', NULL);
INSERT INTO `domains` VALUES (9, 1, 7, 'Expressive Language Domain 2', '2023-11-13 18:56:34', '2023-11-13 18:56:34', NULL);
INSERT INTO `domains` VALUES (10, 1, 8, 'Fine Motor Domain 1', '2023-11-13 18:56:44', '2023-11-13 18:56:44', NULL);
INSERT INTO `domains` VALUES (11, 1, 8, 'Fine Motor Domain 2', '2023-11-13 18:56:49', '2023-11-13 18:56:49', NULL);
INSERT INTO `domains` VALUES (12, 1, 9, 'Receptive Language Domain 1', '2023-11-13 18:57:00', '2023-11-13 18:57:00', NULL);
INSERT INTO `domains` VALUES (13, 1, 9, 'Receptive Language Domain 2', '2023-11-13 18:57:04', '2023-11-13 18:57:04', NULL);
INSERT INTO `domains` VALUES (14, 1, 10, 'Social-Emotional Domain 1', '2023-11-13 18:57:20', '2023-11-13 18:57:20', NULL);
INSERT INTO `domains` VALUES (15, 1, 10, 'Social-Emotional Domain 2', '2023-11-13 18:57:23', '2023-11-13 18:57:23', NULL);

-- ----------------------------
-- Table structure for evaluation_months
-- ----------------------------
DROP TABLE IF EXISTS `evaluation_months`;
CREATE TABLE `evaluation_months`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of evaluation_months
-- ----------------------------
INSERT INTO `evaluation_months` VALUES (1, 'August', '2023-11-13 09:29:02', '2023-11-13 09:29:09');
INSERT INTO `evaluation_months` VALUES (2, 'March', '2023-11-13 09:29:18', '2023-11-13 10:37:19');

-- ----------------------------
-- Table structure for evaluations
-- ----------------------------
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE `evaluations`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `student_id` bigint UNSIGNED NOT NULL,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `evaluation_month_id` bigint UNSIGNED NOT NULL,
  `raw_score_total` int NULL DEFAULT 0,
  `scaled_score_total` int NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `evaluations to teachers`(`teacher_id` ASC) USING BTREE,
  INDEX `evaluations to students`(`student_id` ASC) USING BTREE,
  CONSTRAINT `evaluations to students` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `evaluations to teachers` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of evaluations
-- ----------------------------
INSERT INTO `evaluations` VALUES (7, 1, 1, 1, 6, 6, '2023-11-14 18:48:17', '2023-11-14 18:48:17', NULL);
INSERT INTO `evaluations` VALUES (8, 1, 1, 2, 10, 10, '2023-11-14 18:48:17', '2023-11-14 18:48:17', NULL);

-- ----------------------------
-- Table structure for games
-- ----------------------------
DROP TABLE IF EXISTS `games`;
CREATE TABLE `games`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `student_id` bigint UNSIGNED NOT NULL,
  `game` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `score` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `games to students`(`student_id` ASC) USING BTREE,
  CONSTRAINT `games to students` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of games
-- ----------------------------

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` tinyint NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teachers to modules`(`teacher_id` ASC) USING BTREE,
  INDEX `teachers to subjects`(`subject_id` ASC) USING BTREE,
  CONSTRAINT `teachers to modules` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of modules
-- ----------------------------

-- ----------------------------
-- Table structure for parents
-- ----------------------------
DROP TABLE IF EXISTS `parents`;
CREATE TABLE `parents`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `login_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `birthdate` date NOT NULL,
  `gender` tinyint NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parents to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `parents to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of parents
-- ----------------------------
INSERT INTO `parents` VALUES (1, 3, 'parent', '$2y$10$efZ.iqEFS6T3zDlqn8IkC.qseGCEkFakCoO9UpJR/0JHlPOQZwyza', 'Parent And Student Address\r\n', 'Parent', 'Parent', 'Parent', '2023-10-13', 0, '2023-10-13 18:09:19', '2023-10-13 19:20:32', NULL);

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `quiz_id` bigint UNSIGNED NOT NULL,
  `question` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `points` double NOT NULL DEFAULT 1,
  `answer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `questions to quizzes`(`quiz_id` ASC) USING BTREE,
  CONSTRAINT `questions to quizzes` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of questions
-- ----------------------------

-- ----------------------------
-- Table structure for quizzes
-- ----------------------------
DROP TABLE IF EXISTS `quizzes`;
CREATE TABLE `quizzes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `quiz_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `quiz` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `quizzes to teachers`(`teacher_id` ASC) USING BTREE,
  INDEX `quizzes to subjects`(`subject_id` ASC) USING BTREE,
  CONSTRAINT `quizzes to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `quizzes to teachers` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of quizzes
-- ----------------------------

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (1, '2023-10-16 21:20:23', '2023-10-16 21:20:23', 'Users', '1', '3e1752185e380b688d5a2d4dc7a3c9b4c917a0a0', 'ecc0c75ae0d16189eacefd23cb9b90e8324d8d6f', '2023-11-15 21:20:23');
INSERT INTO `remember_me_tokens` VALUES (2, '2023-10-17 16:53:26', '2023-10-17 16:53:26', 'Users', '1', '7e7d397c19f7ae7e611ca015388729d223452289', '91252b8da4633f2549f86d05e5c0cdb04ec053ac', '2023-11-16 16:53:26');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gender` tinyint NOT NULL,
  `birthdate` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `students to users`(`user_id` ASC) USING BTREE,
  INDEX `students to teachers`(`teacher_id` ASC) USING BTREE,
  CONSTRAINT `students to teachers` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `students to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (1, 3, 1, 'Student', 'Student', 'Student', 0, '2023-10-13', '2023-10-13 18:09:19', '2023-10-13 19:20:12', NULL);

-- ----------------------------
-- Table structure for subjects
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `subjects to teachers`(`teacher_id` ASC) USING BTREE,
  CONSTRAINT `subjects to teachers` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of subjects
-- ----------------------------
INSERT INTO `subjects` VALUES (1, 1, 'Filipino', '2023-10-16 21:20:55', '2023-10-16 21:20:55', NULL);

-- ----------------------------
-- Table structure for teachers
-- ----------------------------
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gender` tinyint NOT NULL,
  `birthdate` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teachers to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `teachers to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teachers
-- ----------------------------
INSERT INTO `teachers` VALUES (1, 1, 'TeacherName', 'Teacher', 'Teacher', 0, '2023-10-10', '2023-10-10 17:39:56', '2023-10-10 17:39:56', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `contact_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT 1,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_admin` tinyint NOT NULL DEFAULT 0,
  `is_student` tinyint NOT NULL DEFAULT 0,
  `is_parent` tinyint NOT NULL DEFAULT 0,
  `is_teacher` tinyint NOT NULL DEFAULT 0,
  `api_key` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `api_code` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'teacher', 'teacher@gmail.com', '09999999999', 'user-img/6524ab6c54cf4preview.jpg', '$2y$10$EAEsaG8SVAZO27n7VVfdJ.W831GtxM2oQybFF68F91cEC4yWsYwT6', '', 1, '47962038', 0, 0, 0, 1, '6524ab6c8b2286524ab6c8b2296524ab6c8b22a', 'NTA2NjJmOWVhZDUyNDNjNWRhNGY5MThiMmY3NDBjYTM0ZDdhZjcwMzIzZmE1YmIwZGVkODNiNDkwZWRjNGUxNo7IxnzRdW8N5w23gY5BhrWG7wGCJke7nmOwtJXxibYj', '2023-10-10 17:39:56', '2023-10-10 17:39:56', NULL);
INSERT INTO `users` VALUES (2, 'admin', 'admin@gmail.com', '09776544455', 'user-img/652607f46a0db64f3ee1f7f040320.png', '$2y$10$tfVTLWiQvBqTLC9b7x4h3eoWLUCQ/sWxPrH5982HXinQjcnsaaNwG', '', 1, '18473884', 1, 0, 0, 0, '652607f495020652607f495022652607f495023', 'ODY3Yzg2NTQxNGM4MWJmNDYyZmJiM2E4MDdhZjY2ZjM0YTg1MGFkMjY3YjdiYTQyOTdiNTJmYmUxMmYwM2YxNOKanf5O4c9nWkPINJcGYow3Klqe912bqoawpstOu4Fm', '2023-10-11 18:27:00', '2023-10-11 18:27:00', NULL);
INSERT INTO `users` VALUES (3, 'student', 'student@gmail.com', '09646464643', 'user-img/6528a6ceb4add64f3ecdd97382320.png', '$2y$10$RM47GKtKhKYdL71xCF3ocO/lx1ze59Hb4CrDxBXxoQHR7v2OEBY8e', '', 1, '32171789', 0, 1, 1, 0, '6528a6ceed9f06528a6ceed9f16528a6ceed9f2', 'MTg4ZTZlYmZlYjAwZTYyYTY1NTVkYWQwMTRlNmVlZWI0YzkxMzUwOTljMTNhMjg2MWMwYTc0NTc1OTcyZjYxNimO8w8O3qd/37xFQiuDMNP+nyQnPeVxi7SGqyRYHPZ2', '2023-10-13 18:09:18', '2023-10-13 18:09:18', NULL);

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `video` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `videos to teachers`(`teacher_id` ASC) USING BTREE,
  CONSTRAINT `videos to teachers` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of videos
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
